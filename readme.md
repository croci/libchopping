Libchopping - low-precision emulator in C++ and Python
==========

About
-----

`Libchopping` is strongly inspired by and partly directly translated from Higham and Pranesh's chop library.

`Libchopping` runs in C++ using both vectorization and OpenMP for parallelism. However, its intended use is
from Python, where the C++ library is called thanks to a pybind11 wrapper. The library can also directly
be used in C++.

`Libchopping` can be used both in a "manual" way by calling rounding functions after every operation,
and in an "automatic" way by wrapping a (C or numpy) array into a `low-precision variable` (`LPV`) object
and letting operator overloading do the rounding for you whenever needed.

`Libchopping` also supports some linear algebra operations such as:

* matrix multiplication,
* transposition,
* matrix-vector products in tridiagonal, banded, symmetric toeplitz and CSR format,
* Thomas algorithm for solving tridiagonal linear systems,
* Cholesky factorization (including using it to solve spd linear systems),
* Geometric multigrid solver for -lambda * laplace(u) + u with Dirichlet BCs,
* slicing (partially supported).

The arithmetic formats supported are 

*  'b', 'bfloat16'           - bfloat16,
*  'h', 'half', 'fp16'       - IEEE half precision (the default),
*  's', 'single', 'fp32'     - IEEE single precision,
*  'd', 'double', 'fp64'     - IEEE double precision,
*  'c', 'custom'            - custom format.

Subnormal numbers can be supported or not,
and in the latter case they are flushed to zero.

Several rounding modes are supported:

1. Round to nearest using round to even last bit to break ties (the default).
2. Round towards plus infinity (round up).
3. Round towards minus infinity (round down).
4. Round towards zero.
5. Stochastic rounding - round to the next larger or next smaller floating-point number with probability proportional to the distance to those floating-point numbers.
6. Stochastic rounding - round to the next larger or next smaller floating-point number with equal probability.

Optionally, each element of the rounded result has, with a specified
probability defaulting to 0.5, a randomly chosen bit in its significand
flipped.  This option is useful for simulating soft errors

A further option causes the exponent limit for the specified arithmetic to
be ignored, so overflow, underflow, or subnormal numbers will be produced
only if necessary for the data type of the input.  This option is useful
for exploring low precisions indepdent of range limitations.

Demonstration functions (in `src/tests/`):

* `demo-harmonic` computes the harmonic series in several arithmetic
   formats using all the supported rounding modes.
* `demo_slicing` shows some examples on slicing of LPV variables

Test functions (in `src/tests/`):

* `test_chop` is a test function for `chop`.
* `test_roundit` is a test function for `roundit`.
* `test_bindings` is a test function for the pybind11 bindings.
* `test_linalg` is a test function for some of the linear algebra routines.
* `test_mg` and `test_mg_lpv` are test functions for the geometric multigrid routines.

Each test function should print "All tests successful!".

How to use (from Python)
------------------------

The easiest way to learn is to look at the test functions and demos.

The low-precision emulation works in double precision so input to `Libchopping` functions should be in
double precision. The code is easy to modify to use single precision instead.

First, run `from chopping import *` to import the library. Then the first step is to define an `Option` object containing information about which format needs to be emulated.
This is done via `op = Option(); op.set_format('h'); op.round = 4` for e.g. half precision with round towards zero (the formats and rounding modes follow the same numbering as in this readme).
`op.set_format(fmt)` must always be called when the option is defined or an error will be thrown. 
Then you can call the chop function via e.g. `a = 5.4; chop(a, op)`, or define a low-precision variable LPV array via e.g. `import numpy as np; a = np.random.randn(5); a_lpv = LPV(a, op)`.

When using LPV array, the Option object will be linked to the LPV object and an error will be thrown for operations between LPV objects linked to different rounding modes and/or formats.
All operations with LPV arrays will automatically call `chop` under the hood so that emulation is performed automatically. 

How to use (from C++)
------------------------

Most functions and the LPV class work similarly as in Python.
The header `LPV.hpp` must be included to use the library.
The geometric multigrid headers `mg.hpp` and `mglpv.hpp` must be included separately.

Requirements
---------

Python3, numpy, pybind11, Intel MKL library, OpenMP, Intel C++ compiler icpx (recommended, but optional).

Generic Installation
---------

Make sure the MKLROOT and PYBIND11\_DIR environment variables are set
correctly and modify the Makefile if needed.

Run make, then set your PYTHONPATH accordingly to work from Python.

Possibly also set the OpenMP environment variable OMP_NUM_THREADS to specify the number of OpenMP threads.

Spack Installation on a Linux server
---------

To install the prerequisites and the library using spack, run in bash:

```bash
spack env create libchopping
spack env activate libchopping
spack add py-numpy
spack add py-scipy
spack add py-pybind11
spack add py-ipython
spack add intel-oneapi-compilers
spack add intel-oneapi-mkl
spack install
despacktivate
```

Then clone the `libchopping` repo and install it with

```bash
git clone git@bitbucket.org:croci/libchopping.git
spack env activate libchopping
cd libchopping/src/
make
export PYTHONPATH="${PWD}:${PYTHONPATH}"
```

If you encounter issues, make sure the MKLROOT variable
in the Makefile is set correctly. Modify the Makefile if needed.

IMPORTANT: You will have to set the `PYTHONPATH` again every time you log out.

Possibly also set the OpenMP environment variable `OMP_NUM_THREADS` to specify the number of OpenMP threads.

Spack Installation on a Linux laptop
---------

To install the prerequisites and the library using spack, run in bash:

```bash
python3 -m pip install --upgrade --user pybind11 scipy numpy 
spack install intel-oneapi-compilers
spack install intel-oneapi-mkl
spack load intel-oneapi-compilers intel-oneapi-mkl
```

Then clone the `libchopping` repo and install it with

```bash
git clone git@bitbucket.org:croci/libchopping.git
spack env activate libchopping
cd libchopping/src/
make
export PYTHONPATH="${PWD}:${PYTHONPATH}"
```

If you encounter issues, make sure the MKLROOT variable
in the Makefile is set correctly and that pybind11 can be found.
Modify the Makefile if needed.

IMPORTANT: You will have to set the `PYTHONPATH` again every time you log out.

Possibly also set the OpenMP environment variable `OMP_NUM_THREADS` to specify the number of OpenMP threads.

Test Installation
----------

Test that the library has been installed correctly by doing

```bash
cd libchopping/src/tests/
source ./test_all.sh
```

If any of the tests fails, open `test_all.sh`, figure out which test has failed, and try running the relevant code
from the terminal for debugging.

References
----------

Matteo Croci and Michael B. Giles, [Effects of round-to-nearest and stochastic rounding in the numerical solution of the heat equation in low precision
](https://arxiv.org/abs/2010.16225), ArXiv preprint, Mathematical Institute, University of Oxford, UK, Oct 2020.

Nicholas J. Higham and Srikara Pranesh, [Simulating Low Precision
Floating-Point Arithmetic](https://epubs.siam.org/doi/abs/10.1137/19M1251308), SIAM Journal on Scientific Computing 41.5 (2019): C585-C602.

License
-------

See `license.txt` for licensing information.
