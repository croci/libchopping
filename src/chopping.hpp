#include <iostream>
#include <string.h>
#include <sstream>
#include <regex>
#include <iomanip>
#include <math.h>
#include <stdexcept>
#include <omp.h>
#include <mkl.h>

#define ALIGN 32

const int nthreads = omp_get_max_threads();

template<typename real>
void myfill(real * v, const int N, const double val){
    #ifdef __INTEL_COMPILER
        __assume_aligned(v, ALIGN);
    #endif
    #pragma omp parallel for simd default(none) firstprivate(N,val) shared(v) aligned(v: ALIGN)
    for(int i=0;i<N;i++)
        v[i] = val;
}

void check_allocation_successful(void *allocated_mem){
    if (!allocated_mem){
        printf("Error: Allocation unsuccessful\n");
        exit(1);
    }
}

void* allocate(const unsigned int size){
    static_assert(ALIGN && ((ALIGN & (ALIGN - 1)) == 0));
    const unsigned int corrected_size = (size + ALIGN - 1) & -ALIGN;

    void * out __attribute__((aligned(ALIGN)));
    
    //out = aligned_alloc(ALIGN, corrected_size);
    
    if(size >= ALIGN)
        out = mkl_malloc(corrected_size, ALIGN);
    else
        out = mkl_malloc(size, ALIGN);

    check_allocation_successful(out);
    return out;
}
