py::array_t<double> fmgv_pybind(const int dim, py::array_t<double,py::array::c_style> b, const double lambda, const std::string& smoother, const int nsmooth, py::array_t<double,py::array::c_style> x){
    if(b.size() != b.shape(0) || x.size() != x.shape(0) || x.size() != b.size())
        throw std::invalid_argument("fmgv: input arrays must be flattened and have the same dimensions");
    const int N = b.size();
    const int J = (int) std::round(std::pow(N, 1.0/dim));

    double * out __attribute__((aligned(ALIGN)));
    out = (double*) allocate(N*sizeof(double));
    double * xptr = (double*) x.data(0);
    for(int i=0; i<N; i++)
        out[i] = xptr[i];

    fmgv(dim, (double*) b.data(0), lambda, smoother, nsmooth, J, out);

    return py::array_t<double>(N, out);
}

py::array_t<double> mgv_pybind(const int dim, py::array_t<double,py::array::c_style> b, const double lambda, const std::string& smoother, const int nsmooth, py::array_t<double,py::array::c_style> x){
    if(b.size() != b.shape(0) || x.size() != x.shape(0) || x.size() != b.size())
        throw std::invalid_argument("mgv: input arrays must be flattened and have the same dimensions");
    const int N = b.size();
    const int J = (int) std::round(std::pow(N, 1.0/dim));

    double * out __attribute__((aligned(ALIGN)));
    out = (double*) allocate(N*sizeof(double));
    double * xptr = (double*) x.data(0);
    for(int i=0; i<N; i++)
        out[i] = xptr[i];

    mgv(dim, (double*) b.data(0), lambda, smoother, nsmooth, J, out);

    return py::array_t<double>(N, out);
}

py::array_t<double> jacobi_pybind(const int dim, const double w, const double lambda, const int n_it, py::array_t<double, py::array::c_style> b, py::array_t<double, py::array::c_style> x){
    if(b.size() != b.shape(0) || x.size() != x.shape(0) || x.size() != b.size())
        throw std::invalid_argument("jacobi: input arrays must be flattened and have the same dimensions");
    const int N = b.size();
    const int J = (int) std::round(std::pow(N, 1.0/dim));

    double * out __attribute__((aligned(ALIGN)));
    out = (double*) allocate(N*sizeof(double));
    double * xptr = (double*) x.data(0);
    for(int i=0; i<N; i++)
        out[i] = xptr[i];

    jacobi(dim, w, lambda, n_it, J, (double*) b.data(0), out);

    return py::array_t<double>(N, out);
}

py::array_t<double> ssor_pybind(const int dim, const double w, const double lambda, const int n_it, py::array_t<double, py::array::c_style> b, py::array_t<double, py::array::c_style> x){
    if(b.size() != b.shape(0) || x.size() != x.shape(0) || x.size() != b.size())
        throw std::invalid_argument("ssor: input arrays must be flattened and have the same dimensions");
    const int N = b.size();
    const int J = (int) std::round(std::pow(N, 1.0/dim));

    double * out __attribute__((aligned(ALIGN)));
    out = (double*) allocate(N*sizeof(double));
    double * xptr = (double*) x.data(0);
    for(int i=0; i<N; i++)
        out[i] = xptr[i];

    ssor(dim, w, lambda, n_it, J, (double*) b.data(0), out);

    return py::array_t<double>(N, out);
}

py::array_t<double> residual_pybind(const int dim, py::array_t<double, py::array::c_style> b, const double lambda,  const py::array_t<double, py::array::c_style> x){
    if(b.size() != b.shape(0) || x.size() != x.shape(0) || x.size() != b.size())
        throw std::invalid_argument("residual: input arrays must be flattened and have the same dimensions");
    const int N = b.size();
    const int J = (int) std::round(std::pow(N, 1.0/dim));

    double * out __attribute__((aligned(ALIGN)));
    out = (double*) allocate(N*sizeof(double));
    std::fill(out, out + N, 0.0);

    residual(dim, (double*) b.data(0), lambda, J, (double*) x.data(0), out);

    return py::array_t<double>(N, out);
}

py::array_t<double> remove_zero_boundary_pybind(const int dim, py::array_t<double, py::array::c_style> x){
    if(x.size() != x.shape(0))
        throw std::invalid_argument("remove_zero_boundary: input arrays must be flattened and have the same dimensions");
    const int Nb = x.size();
    const int Jb = (int) std::round(std::pow(Nb, 1.0/dim));
    const int J = Jb-2;
    const int N = std::pow(J, dim);

    double * out __attribute__((aligned(ALIGN)));
    out = remove_zero_boundary(dim, Jb, (double*) x.data(0));

    return py::array_t<double, py::array::c_style>(N, out);
}

py::array_t<double> add_zero_boundary_pybind(const int dim, py::array_t<double, py::array::c_style> x){
    if(x.size() != x.shape(0))
        throw std::invalid_argument("add_zero_boundary: input arrays must be flattened and have the same dimensions");
    const int N = x.size();
    const int J = (int) std::round(std::pow(N, 1.0/dim));
    const int Jb = J+2;
    const int Nb = std::pow(Jb, dim);

    double * out __attribute__((aligned(ALIGN)));
    out = add_zero_boundary(dim, J, (double*) x.data(0));

    return py::array_t<double, py::array::c_style>(Nb, out);
}
