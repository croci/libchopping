#include "option.hpp"

template <typename real>
void roundit(real *x, const unsigned int N, const Option *opts){
    int s = 1; 
    real frac, intgr;
    float * U __attribute__((aligned(ALIGN))); U = NULL;
    float * P __attribute__((aligned(ALIGN))); P = NULL;

    const bool needU = (opts->round == 5 || opts->round == 6 || opts->flip);
    const bool needP = opts->flip;

    const int tid = omp_get_thread_num();

    if (needU){
        U = (float *)allocate(N*sizeof(float));
    }
    if(needP){
        P = (float *)allocate(N*sizeof(float));
    }

    switch (opts->round) {
        case 1 :
            #ifdef __INTEL_COMPILER
                __assume_aligned(x, ALIGN);
            #endif
            #pragma omp simd aligned(x : ALIGN)
            for(unsigned int i=0; i<N; i++){
                s = (real) (0. < x[i]) - (real) (x[i] < 0.);
                x[i] = fabs(x[i]);
                x[i] = round(x[i] - (remainder(x[i],2.) == 0.5));
                if (x[i] == -1.)
                    x[i] = 0.;
                x[i] *= s;
            }

            break;
        case 2 :
            #ifdef __INTEL_COMPILER
                __assume_aligned(x, ALIGN);
            #endif
            #pragma omp simd aligned(x : ALIGN)
            for(unsigned int i=0; i<N; i++){
                x[i] = ceil(x[i]);
            }

            break;

        case 3 :
            #ifdef __INTEL_COMPILER
                __assume_aligned(x, ALIGN);
            #endif
            #pragma omp simd aligned(x : ALIGN)
            for(unsigned int i=0; i<N; i++){
                x[i] = floor(x[i]);
            }

            break;

        case 4 :
            #ifdef __INTEL_COMPILER
                __assume_aligned(x, ALIGN);
            #endif
            #pragma omp simd aligned(x : ALIGN)
            for(unsigned int i=0; i<N; i++){
                x[i] = trunc(x[i]);
            }

            break;

        case 5 : 
            vsRngUniform(VSL_RNG_METHOD_UNIFORM_STD, opts->streamptr[tid], N, U, 0, 1);

            #ifdef __INTEL_COMPILER
                __assume_aligned(x, ALIGN);
                __assume_aligned(U, ALIGN);
            #endif
            #pragma omp simd aligned(x, U : ALIGN)
            for(unsigned int i=0; i<N; i++){
                s = (int) (0. <= x[i]) - (int) (x[i] < 0.);
                x[i] = fabs(x[i]);
                frac = modf(x[i], &intgr);
                if(frac != 0){
                    if (U[i] <= frac)
                        x[i] = ceil(x[i]);
                    else
                        x[i] = floor(x[i]);
                }
                x[i] *= s;
            }
            break;

        case 6 :
            vsRngUniform(VSL_RNG_METHOD_UNIFORM_STD, opts->streamptr[tid], N, U, 0, 1);

            #ifdef __INTEL_COMPILER
                __assume_aligned(x, ALIGN);
                __assume_aligned(U, ALIGN);
            #endif
            #pragma omp simd aligned(x, U : ALIGN)
            for(unsigned int i=0; i<N; i++){
                s = (int) (0. <= x[i]) - (int) (x[i] < 0.);
                x[i] = fabs(x[i]);
                frac = modf(x[i], &intgr);
                if(frac != 0){
                    if (U[i] <= 0.5)
                        x[i] = ceil(x[i]);
                    else
                        x[i] = floor(x[i]);
                }
                x[i] *= s;
            }
            break;
    }

    if(needP){
        vsRngUniform(VSL_RNG_METHOD_UNIFORM_STD, opts->streamptr[tid], N, U, 0, 1);
        vsRngUniform(VSL_RNG_METHOD_UNIFORM_STD, opts->streamptr[tid], N, P, 0, opts->t-1);
        #ifdef __INTEL_COMPILER
            __assume_aligned(x, ALIGN);
            __assume_aligned(U, ALIGN);
            __assume_aligned(P, ALIGN);
        #endif
        #pragma omp simd aligned(x, U, P : ALIGN)
        for(unsigned int i=0; i<N; i++){
            if (U[i] <= opts->p){
                s = (int) (0. <= x[i]) - (int) (x[i] < 0.);
                x[i] = fabs(x[i]);
                x[i] = (real) (((unsigned int) x[i]) ^ ((unsigned int) exp2(ceil(P[i])-1)));
                x[i] *= s;

            }
        }
    }

    if (needU){
        //std::free(U);
        mkl_free(U);
    }
    if(needP){
        //std::free(P);
        mkl_free(P);
    }
}
