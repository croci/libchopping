#include "LPV.hpp"
#include "mg.hpp"
#include "mglpv.hpp"

// need to undefine previously defined symbols as they clash with the pybind headers
#ifdef th
#undef th
#endif

#ifdef tb
#undef tb
#endif

#ifdef ts
#undef ts
#endif

#ifdef td
#undef td
#endif

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
//#include <pybind11/pytypes.h>
//#include <pybind11/cast.h>
#include <pybind11/operators.h>

namespace py = pybind11;

#include "mg_bindings.hpp"

py::dict floating_point_parameters_pybind(const char fmt){
    const float_params p = floating_point_parameters(fmt);
    py::dict d;
    d["t"] = p.t;
    d["u"] = p.u;
    d["emax"]  = p.emax;
    d["emin"]  = p.emin;
    d["emins"] = p.emins;
    d["xmax"]  = p.xmax;
    d["xmin"]  = p.xmin;
    d["xmins"] = p.xmins;
    return d;
}

py::dict get_floating_point_parameters_pybind(Option & op){
    const float_params p = op.get_floating_point_parameters();
    py::dict d;
    d["t"] = p.t;
    d["u"] = p.u;
    d["emax"]  = p.emax;
    d["emin"]  = p.emin;
    d["emins"] = p.emins;
    d["xmax"]  = p.xmax;
    d["xmin"]  = p.xmin;
    d["xmins"] = p.xmins;
    return d;
}

template <typename real>
real roundit_pybind_scalar(real x, const Option *opts){
    static_assert(std::is_floating_point<real>::value, "Can only round floating point numbers.");
    roundit<real>(&x, 1, opts);
    return x;
}

template <typename real>
void roundit_pybind_vector(py::array_t<real, py::array::c_style> x, const Option *opts){
    static_assert(std::is_floating_point<real>::value, "Can only round floating point numbers.");
    roundit<real>((real*) x.data(0), x.size(), opts);
}

double chop_pybind_scalar(double x, const Option *opts){
    chop(x, opts);
    return x;
}

void chop_pybind_vector(py::array_t<double, py::array::c_style> x, const Option *opts){
    chop((double*) x.data(0), x.size(), opts);
}

void choptest_scalar(py::array_t<double, py::array::c_style> x, const Option *opts){
    const double * xx = x.data(0);
    const int N = x.size();
    double xchopped;
    for(int i=0; i<N; i++)
        chop(&xx[i], &xchopped, 1, opts);
}

LPV<double> LPV_init_pybind(py::array_t<double,py::array::c_style> x, const Option *opts, const bool inplace=false, const bool needchopping=true){
        const unsigned int mm = x.shape(0);
        const unsigned int nn = x.size()/mm;
        LPV<double> out((double*) x.data(0), mm, nn, opts, inplace, needchopping);
        return out;
}

LPV<float> LPV_init_pybind_float(py::array_t<float,py::array::c_style> x, const Option *opts, const bool inplace=false, const bool needchopping=true){
        const unsigned int mm = x.shape(0);
        const unsigned int nn = x.size()/mm;
        LPV<float> out((float*) x.data(0), mm, nn, opts, inplace, needchopping);
        return out;
}

template <typename real>
LPV<real> banded_cholesky_pybind(py::array_t<real,py::array::c_style> a, const int nbands, const Option * opts){
    if(a.shape(0) != a.shape(1)  || nbands < 0 || nbands >= a.shape(0))
        throw std::invalid_argument("banded_cholesky: dimension mismatch.");

    return banded_cholesky<real>(a.data(0), nbands, a.shape(0), opts);
}

template <typename real>
LPV<real> solve_symmetric_pybind(py::array_t<real,py::array::c_style> a, const int nbands, const LPV<real> &b){
    if(a.shape(0) != a.shape(1) || a.shape(0) != b.N || b.m != b.N || nbands < 0 || nbands >= a.shape(0))
        throw std::invalid_argument("solve_symmetric: dimension mismatch.");

    return solve_symmetric<real>(a.data(0), nbands, b);
}

template <typename real>
LPV<real> solve_tridiag_numpy_pybind(py::array_t<real,py::array::c_style> a, py::array_t<real,py::array::c_style> b, py::array_t<real,py::array::c_style> c, LPV<real> &d){
        const unsigned int N = d.N;
        const unsigned int aN = a.size(), bN = b.size(), cN = c.size();
        const unsigned int am = a.shape(0), bm = b.shape(0), cm = c.shape(0);
        if(N < 2 || bN != (aN+1) || aN != cN || bm != bN || am != aN || cm != cN || N != bN)
            throw std::invalid_argument("solve_tridiag: dimension mismatch or scalar input.");

        LPV<real> aa((real*) a.data(0), am, 1, d.opts);
        LPV<real> bb((real*) b.data(0), bm, 1, d.opts);
        LPV<real> cc((real*) c.data(0), cm, 1, d.opts);
        return solve_tridiag<real>(aa,bb,cc,d);
}

template <typename real>
LPV<real> matvec_symm_toeplitz_numpy_pybind(py::array_t<real,py::array::c_style> row, py::array_t<int,py::array::c_style> idx, LPV<real> &b){
        const unsigned int N = b.N;
        const unsigned int rowN = row.size(), idxN = idx.size();
        const unsigned int rowm = row.shape(0), idxm = idx.shape(0);
        if(N < 3 || rowN != idxN+1 || rowN != rowm || idxN != idxm || rowN > N)
            throw std::invalid_argument("matvec_symm_toeplitz: dimension mismatch or scalar input. Not implemented for 2-by-2 matrices.");

        return matvec_symm_toeplitz<real>((real*) row.data(0), (int*) idx.data(0), (int) idxN, b);
}

template <typename real>
LPV<real> FEMdiff_numpy_pybind(py::array_t<real,py::array::c_style> K, const LPV<real> &b, const int n_cells, const int edim){
        if(b.N < 2 || b.N != n_cells*edim || K.size() != n_cells*edim*edim)
            throw std::invalid_argument("FEMdiff: scalar input or dimension mismatch.");
        return FEMdiff<real>((real*) K.data(0), b, n_cells, edim);
}

template <typename real>
LPV<real> matvec_csr_numpy_pybind(py::array_t<real,py::array::c_style> data, py::array_t<int,py::array::c_style> indices, py::array_t<int,py::array::c_style> indptr, LPV<real> &b){
        return matvec_csr<real>((real*) data.data(0), (int*) indices.data(0), (int*) indptr.data(0), b);
}

template <typename real>
LPV<real> FEMdiff_csr_numpy_pybind(py::array_t<real,py::array::c_style> data, py::array_t<int,py::array::c_style> indices, py::array_t<int,py::array::c_style> indptr, LPV<real> &b){
        return FEMdiff_csr<real>((real*) data.data(0), (int*) indices.data(0), (int*) indptr.data(0), b);
}

template <typename real>
LPV<real> matvec_symm_toeplitz_pybind(LPV<real> &row, py::array_t<int,py::array::c_style> idx, LPV<real> &b){
        const unsigned int N = b.N;
        const unsigned int rowN = row.N, idxN = idx.size();
        const unsigned int rowm = row.m, idxm = idx.shape(0);
        if(N < 3 || rowN != idxN+1 || rowN != rowm || idxN != idxm || rowN > N)
            throw std::invalid_argument("matvec_symm_toeplitz: dimension mismatch or scalar input. Not implemented for 2-by-2 matrices.");

        return matvec_symm_toeplitz<real>(row, (int*) idx.data(0), (int) idxN, b);
}


template <typename real>
py::array_t<real> array(const LPV<real> &a) {
    // No pointer is passed, so NumPy will allocate the buffer
    if(a.n == 1)
        return py::array_t<real>(a.N, a.x);
    else
        return py::array_t<real>(std::vector<ptrdiff_t>{a.m, a.n}, a.x);
}

template <typename real>
std::string tostring(const LPV<real> &a){
    // FIXME: a horrible workaround
    const std::string out = py::str(array(a).attr("__repr__")().attr("replace")("\n ", "\n     "));
    return "LPV_" + out;
}

PYBIND11_MODULE(chopping, m) {
    m.doc() = "pybind11 wrapper for the chopping library";

    py::class_<Option>(m, "Option")
    .def(py::init<const bool&>(), py::arg("fast")=false, py::return_value_policy::take_ownership)
    .def_readwrite("subnormal", &Option::subnormal)
    .def_readwrite("flip", &Option::flip)
    .def_readwrite("p", &Option::p)
    .def_readwrite("explim", &Option::explim)
    .def("get_floating_point_parameters", &get_floating_point_parameters_pybind)
    .def("set_custom_format", &Option::set_custom_format)
    .def("set_format", &Option::set_format)
    .def("get_format", &Option::get_format)
    .def("set_seed", &Option::set_seed)
    .def("get_seed", &Option::get_seed)
    .def("get_precision", &Option::get_precision)
    .def("get_max_exponent", &Option::get_max_exponent)
    .def("set_round", &Option::set_round)
    .def("get_round", &Option::get_round)
    .def("set_fast", &Option::set_fast)
    .def("is_fast", &Option::is_fast)
    .def("_setUp", &Option::setUp);

    m.def("floating_point_parameters", &floating_point_parameters_pybind);
    m.def("chop", &chop_pybind_vector, py::return_value_policy::take_ownership);
    m.def("chop", &chop_pybind_scalar);
    m.def("chop", [](int a, const Option * opts){return chop_pybind_scalar((double) a, opts);});
    m.def("choptest_scalar", &choptest_scalar);

    m.def("roundit", &roundit_pybind_vector<double>, py::return_value_policy::take_ownership);
    m.def("roundit", &roundit_pybind_scalar<double>);

    m.def("matvec_tridiag", &matvec_tridiag<double>);
    m.def("diffND", [](const LPV<double> &u, const int dim){return diffND<double>(u, dim);});
    m.def("diffND", [](const LPV<double> &u, const int dim, const double bc){return diffND<double>(u, dim, bc);});
    m.def("FEMdiff", &FEMdiff_numpy_pybind<double>);
    m.def("solve_tridiag", &solve_tridiag<double>);
    m.def("solve_tridiag", &solve_tridiag_numpy_pybind<double>);
    m.def("matvec_symm_toeplitz", &matvec_symm_toeplitz_pybind<double>);
    m.def("matvec_symm_toeplitz", &matvec_symm_toeplitz_numpy_pybind<double>);
    m.def("matvec_csr", &matvec_csr_numpy_pybind<double>);
    m.def("FEMdiff_csr", &FEMdiff_csr_numpy_pybind<double>);

    m.def("banded_cholesky", &banded_choleskyLPV<double>);
    m.def("banded_cholesky", &banded_cholesky_pybind<double>);
    m.def("solve_symmetric", &solve_symmetricLPV<double>);
    m.def("solve_symmetric", &solve_symmetric_pybind<double>);

    m.def("fmgv", &fmgv_pybind, py::return_value_policy::take_ownership);
    m.def("mgv", &mgv_pybind, py::return_value_policy::take_ownership);
    m.def("jacobi", &jacobi_pybind, py::return_value_policy::take_ownership);
    m.def("ssor", &ssor_pybind, py::return_value_policy::take_ownership);
    m.def("residual", &residual_pybind, py::return_value_policy::take_ownership);
    m.def("add_zero_boundary", &add_zero_boundary_pybind, py::return_value_policy::take_ownership);
    m.def("remove_zero_boundary", &remove_zero_boundary_pybind, py::return_value_policy::take_ownership);

    m.def("fmgv", &fmgv<double>);
    m.def("mgv", &mgv<double>);
    m.def("jacobi", &jacobi<double>);
    m.def("ssor", &ssor<double>);
    m.def("residual", &residual<double>);
    m.def("add_zero_boundary", &add_zero_boundary<double>);
    m.def("remove_zero_boundary", &remove_zero_boundary<double>);

    //m.def("test", &test);

    py::class_<LPV<double>>(m, "LPV")
    .def(py::init())
    .def(py::init<const LPV<double>&>(), py::return_value_policy::take_ownership)
    .def(py::init(&LPV_init_pybind), py::arg("x"), py::arg("opts"), py::arg("inplace")=false, py::arg("needchopping")=true, py::return_value_policy::take_ownership)

        // for next line and definition of the "array" function see here: https://stackoverflow.com/questions/49179356/pybind11-create-and-return-numpy-array-from-c-side
        // NOTE: not sure if it should be changed to copy since it might happen that Python deletes the array while it is
        // still being used by C++ ? Not sure, but worst case a copy is not that bad.
        .def("array", &array<double>, py::return_value_policy::move)

        .def_readwrite("option", &LPV<double>::opts)
        //.def("getOption", [](LPV<double> &a){return *a.opts;}) //FIXME: since it does not copy the stream, but it does copy the pointer to it the stream destructor gets called twice causing a sigfault
        .def("shape", [](LPV<double> &a){return std::make_tuple(a.m,a.n);})

        .def("__repr__", &tostring<double>)
        .def("__getitem__", [](LPV<double> &a, const unsigned int i){return a[i];}, py::is_operator(), py::return_value_policy::take_ownership)
        .def("__setitem__", [](LPV<double> &a, const unsigned int i, const double b){a[i] = b;}, py::is_operator())
        .def("__setitem__", [](LPV<double> &a, const unsigned int i, LPV<double> &b){
                                if(b.N > 1)
                                    throw std::invalid_argument("Cannot assign an LPV vector to a scalar entry of an LPV variable.");
                                a[i] = b;
                            }, py::is_operator())
        .def("T", [](LPV<double> &a){return transpose(a);}, py::return_value_policy::take_ownership)
        .def(py::self + py::self, py::return_value_policy::take_ownership)
        .def(py::self * py::self, py::return_value_policy::take_ownership)
        .def(py::self - py::self, py::return_value_policy::take_ownership)
        .def(py::self / py::self, py::return_value_policy::take_ownership)
        .def(float() * py::self, py::return_value_policy::take_ownership)
        .def(py::self * float(), py::return_value_policy::take_ownership)
        .def(float() + py::self, py::return_value_policy::take_ownership)
        .def(py::self + float(), py::return_value_policy::take_ownership)
        .def(float() - py::self, py::return_value_policy::take_ownership)
        .def(py::self - float(), py::return_value_policy::take_ownership)
        .def(float() / py::self, py::return_value_policy::take_ownership)
        .def(py::self / float(), py::return_value_policy::take_ownership)
        .def("__neg__", [](LPV<double> &a) {return -a;}, py::is_operator(), py::return_value_policy::take_ownership)
        .def("__matmul__", [](LPV<double> &a, LPV<double> &b) {return mtimes(a,b);}, py::is_operator(), py::return_value_policy::take_ownership)
        .def("dot", [](LPV<double> &a, LPV<double> &b) {return mtimes(a,b);}, py::return_value_policy::take_ownership)
        ;

    m.def("matvec_tridiag", &matvec_tridiag<float>);
    m.def("diffND", [](const LPV<float> &u, const int dim){return diffND<float>(u, dim);});
    m.def("diffND", [](const LPV<float> &u, const int dim, const double bc){return diffND<float>(u, dim, bc);});
    m.def("FEMdiff", &FEMdiff_numpy_pybind<float>);
    m.def("solve_tridiag", &solve_tridiag<float>);
    m.def("solve_tridiag", &solve_tridiag_numpy_pybind<float>);
    m.def("matvec_symm_toeplitz", &matvec_symm_toeplitz_pybind<float>);
    m.def("matvec_symm_toeplitz", &matvec_symm_toeplitz_numpy_pybind<float>);
    m.def("matvec_csr", &matvec_csr_numpy_pybind<float>);
    m.def("FEMdiff_csr", &FEMdiff_csr_numpy_pybind<float>);

    m.def("banded_cholesky", &banded_choleskyLPV<float>);
    m.def("banded_cholesky", &banded_cholesky_pybind<float>);
    m.def("solve_symmetric", &solve_symmetricLPV<float>);
    m.def("solve_symmetric", &solve_symmetric_pybind<float>);

}
