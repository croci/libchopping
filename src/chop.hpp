#include "roundit.hpp"

template <typename real>
void chop(const real *x, real *r, const unsigned int N, const Option *opts){

    static_assert(std::is_floating_point<real>::value, "Can only chop floating point numbers.");

    if(N>1){
        memcpy(r,x,N*sizeof(real));
        chop(r, N, opts);
    }else{
        real r_val = *x;
        chop(r_val, opts);
        *r = r_val;
    }
}

template <typename real>
void chop(real &x, const Option *op){
    static_assert(std::is_floating_point<real>::value, "Can only chop floating point numbers.");
    basechop(x, op);
}

template <typename real>
void chop(real * x, const unsigned int N, const Option *op){
    static_assert(std::is_floating_point<real>::value, "Can only chop floating point numbers.");
    basechopvec(x, N, op);
}

void chop(const double * x, double * r, const unsigned int N, const Option *op){
    if(N>1){
        memcpy(r,x,N*sizeof(double));
        op->chopvecfun(r, N, op);
    }else{
        double r_val = *x;
        op->chopfun(r_val, op);
        *r = r_val;
    }
}

void chop(double &x, const Option *op){
    op->chopfun(x, op);
}

void chop(double * x, const unsigned int N, const Option *op){
    op->chopvecfun(x, N, op);
}

// real can be single or double, or a flattened array of singles or of doubles, not a std::vector!
template <typename real>
void basechopvec(real *x, const unsigned int N, const Option *opts){

    static_assert(std::is_floating_point<real>::value, "Can only chop floating point numbers.");

    if(!(opts->issetup)){
        throw std::invalid_argument("Option variables must be setup with a call to SetUpOption before using.\n");
    }

    real e = 0.;
    int t1;
    bool kbool;
    real * exp2emt __attribute__((aligned(ALIGN)));
    real * xexp2tme __attribute__((aligned(ALIGN)));

    const bool isdouble = std::is_same<real,double>::value;
    const int t = opts->t;
    const int emax = opts->emax;

    const int emin = 1-emax;
    //const double xmin = exp2(emin);
    const int emins = emin + 1 - t;
    const double xmins = exp2(emins);
    //const double xmax = exp2(emax) * (2 - exp2(1-t));

    // Use the representation:
    // x = 2^e * d_1.d_2...d_{t-1} * s, s = 1 or -1.
    
    const double myeps = xmins/4;
    
    exp2emt = (real *)allocate(N*sizeof(real));
    xexp2tme = (real *)allocate(N*sizeof(real));

    #ifdef __INTEL_COMPILER
        __assume_aligned(x, ALIGN);
        __assume_aligned(exp2emt, ALIGN);
        __assume_aligned(xexp2tme, ALIGN);
    #endif
    #pragma omp simd aligned(x, exp2emt, xexp2tme : ALIGN)
    for(unsigned int i=0; i < N; i++){
        e = floor(log2(fabs(x[i])));
        kbool = opts->explim && e < emin && e >= emins;
        if(!kbool && std::isfinite(e)){
            xexp2tme[i] = x[i]*exp2(t-1-e);
            exp2emt[i]  = exp2(e-(t-1));
        }else if(kbool && opts->subnormal){
            t1 = t - std::max((double) emin-e,0.0);
            xexp2tme[i] = x[i]*exp2(t1-1-e);
            exp2emt[i]  = exp2(e-(t1-1));
        }else{
            xexp2tme[i] = 0;
            exp2emt[i]  = 0;
        }

    }
    //
    // NOTE: implement roundit so that it does things in place
    roundit(xexp2tme, N, opts);

    if(opts->explim){
        double xboundary = exp2(emax)*(2-exp2(-t));
        #ifdef __INTEL_COMPILER
            __assume_aligned(x, ALIGN);
            __assume_aligned(xexp2tme, ALIGN);
            __assume_aligned(exp2emt, ALIGN);
        #endif
        #pragma omp simd aligned(x, xexp2tme, exp2emt : ALIGN)
        for(unsigned int i=0; i < N; i++){
            if (x[i] >= xboundary){
                xexp2tme[i] = INFINITY;
                exp2emt[i]  = 1;
            }else if (x[i] <= -xboundary){
                xexp2tme[i] = -INFINITY;
                exp2emt[i]  = 1;
            }else if (fabs(x[i]) < xmins){
                xexp2tme[i] = 0;
                exp2emt[i]  = 0;
            }
        }
    }


    #ifdef __INTEL_COMPILER
        __assume_aligned(x, ALIGN);
        __assume_aligned(exp2emt, ALIGN);
        __assume_aligned(xexp2tme, ALIGN);
    #endif
    #pragma omp simd aligned(x, exp2emt, xexp2tme : ALIGN)
    for(unsigned int i=0; i < N; i++){
        // initialised at 1 unless we flushed subnormals to zero. In this case r[i] is zero
        x[i] = xexp2tme[i]*exp2emt[i];
    }


    mkl_free(xexp2tme);
    mkl_free(exp2emt);
    //std::free(xexp2tme);
    //std::free(exp2emt);

}

// real can be single or double
template <typename real>
void basechop(real &x, const Option *opts){

    static_assert(std::is_floating_point<real>::value, "Can only chop floating point numbers.");

    if(!opts->issetup)
        throw std::invalid_argument("Option variables must be setup with a call to SetUpOption before using.\n");

    real e = 0.;
    int t1;
    bool kbool;
    real exp2emt;
    real xexp2tme;

    const bool isdouble = std::is_same<real,double>::value;

    const int t = opts->t;
    const int emax = opts->emax;

    const int emin = 1-emax;
    //const double xmin = exp2(emin);
    const int emins = emin + 1 - t;
    const double xmins = exp2(emins);
    //const double xmax = exp2(emax) * (2 - exp2(1-t));

    // Use the representation:
    // x = 2^e * d_1.d_2...d_{t-1} * s, s = 1 or -1.
    
    const double myeps = xmins/4;
    
    e = floor(log2(fabs(x)));
    kbool = opts->explim && e < emin && e >= emins;
    if(!kbool && std::isfinite(e)){
        xexp2tme = x*exp2(t-1-e);
        exp2emt  = exp2(e-(t-1));
    }else if(kbool && opts->subnormal){
        t1 = t - std::max((double) emin-e,0.0);
        xexp2tme = x*exp2(t1-1-e);
        exp2emt  = exp2(e-(t1-1));
    }else{
        xexp2tme = 0;
        exp2emt  = 0;
    }

    // NOTE: implement roundit so that it does things in place
    roundit(&xexp2tme, 1, opts);

    if(opts->explim){
        double xboundary = exp2(emax)*(2-exp2(-t));
        if (x >= xboundary){
            xexp2tme = INFINITY;
            exp2emt  = 1;
        }else if (x <= -xboundary){
            xexp2tme = -INFINITY;
            exp2emt  = 1;
        }else if (fabs(x) < xmins){
            xexp2tme = 0;
            exp2emt  = 0;
        }
    }

    // initialised at 1 unless we flushed subnormals to zero. In this case r[i] is zero
    x = xexp2tme*exp2emt;
}
