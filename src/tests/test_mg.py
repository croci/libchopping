from chopping import *
import numpy as np
import scipy.sparse as sp
from scipy.sparse.linalg import spsolve
import sys;

#rng = np.random.RandomState(np.random.randint(1,100000))
rng = np.random.RandomState(3)

def assemble_matrix(K, dim):
    e = np.ones((K,))
    L = sp.spdiags([-e,2*e,-e],[-1,0,1], K, K, format="csr")
    I = sp.eye(L.shape[0])
    if   dim == 1: A = L
    elif dim == 2: A = sp.kron(I,L) + sp.kron(L,I)
    elif dim == 3: A = sp.kron(sp.kron(I,I),L) + sp.kron(L,sp.kron(I,I)) + sp.kron(sp.kron(I,L),I)

    return -A

d = 2

K = 2**5-1
l = 10
max_mg_it = 100
smoother = ["jacobi", "ssor"][1]
if smoother == "jacobi":
    nsmooth = 4
else:
    nsmooth = 4

ex_sol = rng.randn(K**d)
ex_sol /= np.linalg.norm(ex_sol, np.inf)

N = K**d
A = assemble_matrix(K, d)
B = sp.eye(N) - l*A
b = B@ex_sol
bb = add_zero_boundary(d, b)

if d < 3:
    ex = spsolve(B,b)

x = np.zeros(((K+2)**d,))
#x = add_zero_boundary(d, ex_sol)
err2 = np.inf
k = 0

while(err2>1.0e-10):
    x = mgv(d, bb, l, smoother, nsmooth, x)
    #x = fmgv(d, bb, l, smoother, nsmooth, x)
    out = remove_zero_boundary(d,x)
    temp = np.linalg.norm(ex_sol-out, np.inf)
    #temp = np.linalg.norm(b-B@out, np.inf)
    print(temp/err2, temp)
    err2 = temp
    k += 1
    if k >= max_mg_it:
        break

print("n MG its: ", k)

if d < 3:
    err1 = np.linalg.norm(ex-ex_sol, np.inf)
else:
    err1 = None

x = np.zeros(((K+2)**d,))
ksmooth = 0
err3 = np.inf
while(err3 > 1.0e-10):
    if smoother == "jacobi":
        wopt = (2*d*l+1)/(4*d*l+1)
        wopt = 1
        x = jacobi(d, wopt, l, 1,  bb, x)
    else:
        wopt = 2-(2*d*l+1)/(4*d*l+1)
        wopt = 1
        x = ssor(d, wopt, l, 1,  bb, x)
    out3 = remove_zero_boundary(d, x)
    err3 = np.linalg.norm(ex_sol-out3, np.inf)
    ksmooth += 1

print("n smoother its: ", ksmooth)

print("Direct: ", err1, "  MG: ", err2, "  Pure smoother: ", err3)

print("All test successful!")

#############################################################################

#ex_sol = np.zeros((K,))
#s = int((K+1)/8)
#ex_sol[s-1:3*s] = np.arange(0,s*2+1)+1
#ex_sol[5*s-1:7*s] = -2*np.arange(2*s + 1,0,-1)
#ex_sol = ex_sol/np.linalg.norm(ex_sol,np.inf)
