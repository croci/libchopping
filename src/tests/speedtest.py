from chopping import *
import numpy as np
from time import time
import sys

N = 10**7

fmts = ["h", "b", "s"]
for fmt in fmts:
    print("\n\nFormat: " + fmt + "\n")
    rnd = 5
    op = Option(True)
    op.set_round(rnd)
    op.set_format(fmt)
    pms = op.get_floating_point_parameters()
    xmins = pms["xmins"]
    xmin = pms["xmin"]

    ops = Option(False)
    ops.set_round(rnd)
    ops.set_format(fmt)

    a = np.random.randn(N)
    b = a.copy()
    tic = time()
    chop(a,op)
    toc = time()
    timefast = toc-tic
    tic = time()
    chop(b,ops)
    toc = time()
    timeslow = toc-tic
    print("ETA vector:\nfast = ", timefast, "\nslow = ", timeslow, "\ngain = ", timeslow/timefast)

    a = np.random.randn(N)
    tic = time()
    choptest_scalar(a,op)
    toc = time()
    timefast = toc-tic
    tic = time()
    choptest_scalar(a,ops)
    toc = time()
    timeslow = toc-tic
    print("ETA scalar:\nfast = ", timefast, "\nslow = ", timeslow, "\ngain = ", timeslow/timefast)

    a = np.random.rand(N)*(xmin-xmins) + xmins
    b = a.copy()
    tic = time()
    chop(a,op)
    toc = time()
    timefast = toc-tic
    tic = time()
    chop(b,ops)
    toc = time()
    timeslow = toc-tic
    print("Subnormal ETA vector:\nfast = ", timefast, "\nslow = ", timeslow, "\ngain = ", timeslow/timefast)

    a = np.random.rand(N)*(xmin-xmins) + xmins
    tic = time()
    choptest_scalar(a,op)
    toc = time()
    timefast = toc-tic
    tic = time()
    choptest_scalar(a,ops)
    toc = time()
    timeslow = toc-tic
    print("Subnormal ETA scalar:\nfast = ", timefast, "\nslow = ", timeslow, "\ngain = ", timeslow/timefast)

    a = np.random.rand(N)*xmins
    b = a.copy()
    tic = time()
    chop(a,op)
    toc = time()
    timefast = toc-tic
    tic = time()
    chop(b,ops)
    assert(abs(b).max() == 0)
    toc = time()
    timeslow = toc-tic
    print("Below range ETA vector:\nfast = ", timefast, "\nslow = ", timeslow, "\ngain = ", timeslow/timefast)

    a = np.random.rand(N)*xmins
    tic = time()
    choptest_scalar(a,op)
    toc = time()
    timefast = toc-tic
    tic = time()
    choptest_scalar(a,ops)
    toc = time()
    timeslow = toc-tic
    print("Below range ETA scalar:\nfast = ", timefast, "\nslow = ", timeslow, "\ngain = ", timeslow/timefast)
