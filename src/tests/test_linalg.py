from chopping import *
import numpy as np
import scipy.sparse as sp

rng = np.random.RandomState(123)

opt = Option(False)
opt.set_format("d")
opt.set_round(5)

N = 100
#idx = np.unique(np.random.randint(N,size=(int(N/2),), dtype=np.int32))
#row = np.random.randn(len(idx)+1)
row = np.array([1.8,-0.2, 0.5, -0.3])
idx = np.array([1, 3, 5], dtype=np.int32)
b = rng.randn(N)

A = np.diag(row[0]*np.ones(N)) + sum([np.diag(row[i+1]*np.ones(N-idx[i]), -idx[i]) + np.diag(row[i+1]*np.ones(N-idx[i]), idx[i]) for i in range(len(idx))])
AA = A.copy()

out1 = A.dot(b)
#out2 = np.linalg.solve(A,b)

bb = LPV(b, opt)
outa = matvec_symm_toeplitz(row, idx, bb).array()
outb = matvec_symm_toeplitz(LPV(row,opt), idx, bb).array()
ld = row[1]*np.ones(N-1); ud = ld.copy(); d = row[0]*np.ones(N)
outc = matvec_tridiag(LPV(ld,opt), LPV(d,opt), LPV(ud, opt), bb).array()

err1 = abs(outa - out1)/abs(out1)
err2 = abs(outb - out1)/abs(out1)
print("\n", err1, "\n\n", err2)

A = rng.randn(N,N); A = A.dot(A.T) + np.eye(N); idx = [A.shape[0]-1]
AA = A.copy()

A_c = LPV(A, opt)
A = A_c.array()
L = banded_cholesky(A_c, max(idx))
L = L.array().reshape(A.shape)
H = np.linalg.cholesky(A)
print(np.linalg.norm(H-L), np.linalg.norm(L.dot(L.T) - A))

if opt.get_round() == 5:
    H = np.linalg.cholesky(AA)
    L = banded_cholesky(AA, max(idx), opt)
    L1 = banded_cholesky(AA, max(idx), opt).array().reshape(AA.shape)
    L = L.array().reshape(AA.shape)
    print("check: ", np.linalg.norm(L-L1))
    err = L.dot(L.T) - AA
else:
    L = banded_cholesky(A, max(idx), opt)
    L = L.array().reshape(A.shape)
    err = L.dot(L.T) - A

print(abs(H-L).max(), abs(err).max())

if opt.get_round() == 5:
    x_ex = np.linalg.solve(AA,bb.array())
    x1 = solve_symmetric(banded_cholesky(A_c, max(idx)), max(idx), bb)
    x1 = x1.array()
    x2 = solve_symmetric(AA, max(idx), bb)
    x2 = x2.array()
else:
    x_ex = np.linalg.solve(A,bb.array())
    x1 = solve_symmetric(banded_cholesky(A_c, max(idx)), max(idx), bb)
    x1 = x1.array()
    x2 = solve_symmetric(A, max(idx), bb)
    x2 = x2.array()
print(np.linalg.norm(x1-x_ex, np.inf), np.linalg.norm(x2-x_ex, np.inf))

############################## CSR matvec test ##########################

A_csr = sp.csr_matrix(A)

ex = A@b
out = matvec_csr(A_csr.data, A_csr.indices, A_csr.indptr, bb)
err = np.linalg.norm(ex-out.array())
print("CSR Matvec test, error: ", err)

############################### diffND test #############################
def assemble_matrix(K, dim):
    e = np.ones((K,))
    L = sp.spdiags([-e,2*e,-e],[-1,0,1], K, K, format="csr")
    I = sp.eye(L.shape[0])
    if   dim == 1: A = L
    elif dim == 2: A = sp.kron(I,L) + sp.kron(L,I)
    elif dim == 3: A = sp.kron(sp.kron(I,I),L) + sp.kron(L,sp.kron(I,I)) + sp.kron(sp.kron(I,L),I)

    return -A

K = 8

for d in range(1,4):
    N = K**d
    A = assemble_matrix(K, d)
    b = np.random.randn(N)

    ex = A@b
    out = diffND(LPV(b, opt), d)
    err = np.linalg.norm(ex-out.array())/np.linalg.norm(ex)
    print("diff%dD test, error: " % d, err)

############################### NOW WITH FAST #############################

opt = Option(True)
opt.set_format("d")
opt.set_round(5)

N = 100
#idx = np.unique(np.random.randint(N,size=(int(N/2),), dtype=np.int32))
#row = np.random.randn(len(idx)+1)
row = np.array([1.8,-0.2, 0.5, -0.3])
idx = np.array([1, 3, 5], dtype=np.int32)
b = rng.randn(N)

A = np.diag(row[0]*np.ones(N)) + sum([np.diag(row[i+1]*np.ones(N-idx[i]), -idx[i]) + np.diag(row[i+1]*np.ones(N-idx[i]), idx[i]) for i in range(len(idx))])
AA = A.copy()

out1 = A.dot(b)
#out2 = np.linalg.solve(A,b)

bb = LPV(b, opt)
outa = matvec_symm_toeplitz(row, idx, bb).array()
outb = matvec_symm_toeplitz(LPV(row,opt), idx, bb).array()
ld = row[1]*np.ones(N-1); ud = ld.copy(); d = row[0]*np.ones(N)
outc = matvec_tridiag(LPV(ld,opt), LPV(d,opt), LPV(ud, opt), bb).array()

err1 = abs(outa - out1)/abs(out1)
err2 = abs(outb - out1)/abs(out1)
print("\n", err1, "\n\n", err2)

A = rng.randn(N,N); A = A.dot(A.T) + np.eye(N); idx = [A.shape[0]-1]
AA = A.copy()

A_c = LPV(A, opt)
A = A_c.array()
L = banded_cholesky(A_c, max(idx))
L = L.array().reshape(A.shape)
H = np.linalg.cholesky(A)
print(np.linalg.norm(H-L), np.linalg.norm(L.dot(L.T) - A))

if opt.get_round() == 5:
    H = np.linalg.cholesky(AA)
    L = banded_cholesky(AA, max(idx), opt)
    L1 = banded_cholesky(AA, max(idx), opt).array().reshape(AA.shape)
    L = L.array().reshape(AA.shape)
    print("check: ", np.linalg.norm(L-L1))
    err = L.dot(L.T) - AA
else:
    L = banded_cholesky(A, max(idx), opt)
    L = L.array().reshape(A.shape)
    err = L.dot(L.T) - A

print(abs(H-L).max(), abs(err).max())

if opt.get_round() == 5:
    x_ex = np.linalg.solve(AA,bb.array())
    x1 = solve_symmetric(banded_cholesky(A_c, max(idx)), max(idx), bb)
    x1 = x1.array()
    x2 = solve_symmetric(AA, max(idx), bb)
    x2 = x2.array()
else:
    x_ex = np.linalg.solve(A,bb.array())
    x1 = solve_symmetric(banded_cholesky(A_c, max(idx)), max(idx), bb)
    x1 = x1.array()
    x2 = solve_symmetric(A, max(idx), bb)
    x2 = x2.array()
print(np.linalg.norm(x1-x_ex, np.inf), np.linalg.norm(x2-x_ex, np.inf))

############################## CSR matvec test ##########################

A_csr = sp.csr_matrix(A)

ex = A@b
out = matvec_csr(A_csr.data, A_csr.indices, A_csr.indptr, bb)
err = np.linalg.norm(ex-out.array())
print("CSR Matvec test, error: ", err)

############################### diffND test #############################
def assemble_matrix(K, dim):
    e = np.ones((K,))
    L = sp.spdiags([-e,2*e,-e],[-1,0,1], K, K, format="csr")
    I = sp.eye(L.shape[0])
    if   dim == 1: A = L
    elif dim == 2: A = sp.kron(I,L) + sp.kron(L,I)
    elif dim == 3: A = sp.kron(sp.kron(I,I),L) + sp.kron(L,sp.kron(I,I)) + sp.kron(sp.kron(I,L),I)

    return -A

K = 8

for d in range(1,4):
    N = K**d
    A = assemble_matrix(K, d)
    b = np.random.randn(N)

    ex = A@b
    out = diffND(LPV(b, opt), d)
    err = np.linalg.norm(ex-out.array())/np.linalg.norm(ex)
    print("diff%dD test, error: " % d, err)

print("All test successful!")
