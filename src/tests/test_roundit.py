import numpy as np
from chopping import *

n = 0

def chk(a,b):
    assert(abs(np.array(a-b)).max()<1.0e-16)

def rounditc(A,opt):
    if isinstance(A,np.ndarray):
        B = A.copy()
        roundit(B,opt)
        return B
    else:
        return roundit(A,opt)

options = Option()
options.set_format("h")

y = roundit(np.pi, options)
chk(y,3)

options.flip = 0
options.set_round(1)

A = np.array([[0, 1.1, 1.5], [1.9, 2.4, 0.5]])
y = rounditc(A,options)
B = np.array([[0, 1, 2], [2, 2, 0]])
chk(y,B)
y = rounditc(A.T,options)
chk(y,B.T)

A = np.array([[0 ,-1.1, -1.5], [-1.9, -2.4 ,-0.5]])
y = rounditc(A,options);
B = np.array([[0,-1,-2],[-2,-2,0]])
chk(y,B)

options.set_round(2)
A = -A; #[0 1.1 1.5; 1.9 2.4 0.5];
y = rounditc(A,options);
B = np.array([[0,2,2],[2,3,1]])
chk(y,B)

A = np.array([[0 ,-1.1, -1.5], [-1.9, -2.4 ,-0.5]])
y = rounditc(A,options);
B = np.array([[0,-1,-1],[-1,-2,0]])
chk(y,B)

options.set_round(3)
A = -A; #[0 1.1 1.5; 1.9 2.4 0.5];
y = rounditc(A,options);
B = np.array([[0,1,1],[1,2,0]])
chk(y,B)

options.set_round(3)
A = np.array([[0 ,-1.1, -1.5], [-1.9, -2.4 ,-0.5]])
y = rounditc(A,options);
B = np.array([[0,-2,-2],[-2,-3,-1]])
chk(y,B)

options.set_round(4)
A = np.array([[0 ,-1.1, -1.5], [-2.9, -2 ,-0.5],[0.5, 1.5, 3]])
y = rounditc(A,options);
B = np.array([[0,-1,-1],[-2,-2,0],[0,1,3]])
chk(y,B)

options.set_round(5)
tol = 1e-6;
A = np.array([[0, 1+tol, 1-tol], [-tol, -1-tol, -1+tol]])
y = rounditc(A,options);
B = np.array([[0,1,1],[0,-1,-1]])
chk(y,B) # Test succeeds with high probability.

options.set_round(5)
A = np.array([-0.5, 0.5]);
Y = []
for i in range(1,10**3+1):
   Y.append(rounditc(A,options))

Y = np.array(Y)
assert np.linalg.norm(np.mean(Y,axis=0) - 0.5*np.array([-1,1])) <= 5e-2

z = 2; nsamp = 10**4;
options.set_round(5)
for h in [0.1, 0.2, 0.3, 0.4, 0.6, 0.7, 0.8, 0.9]:
   x = z + h
   a = 0; b = 0; 
   for i in range(1,nsamp+1):
       y = rounditc(x,options)
       if y == z:
           a += 1;
       elif y == z + 1:
           b += 1;
       else:
           raise RuntimeError('Unknown rounding.')

   assert(abs(a/nsamp-(1-h)) <= 5e-2)
   assert(abs( b/nsamp - h ) <= 5e-2)

options.set_round(6)
A = np.array([0.2, 0.5, 0.9])
Y = [];
for i in range(1,10**4+1):
   Y.append(rounditc(A,options))

Y = np.array(Y)

ratio = sum(Y.flatten() == 1)/ sum(Y.flatten() == 0);
assert abs(ratio-1) < 0.1 # Heuristic test.

# Test bit flip case.
options.set_round(1)
m = 500;
A = 15*np.random.rand(m,m);
y1 = rounditc(A,options); # Now y1 is an integer matrix.
options.flip = 1;
options.set_custom_format(4,10) # Integers with modulus on [0,15].
y2 = rounditc(y1,options); # No rounding, but bits flipped.
prop_changed = sum(sum(y1 != y2)) / m**2
assert abs(prop_changed - 0.5) < 5e-2 # Heuristic test.
options.p = 0.1;
y2 = rounditc(y1,options); # No rounding, but bits flipped.
prop_changed = sum(sum(y1 != y2)) / m**2
assert abs(prop_changed - options.p) < 5e-2 # Heuristic test.

# Make sure zero is bit-flipped correctly.
options = Option()
options.flip = 1; nsamp = 10**4
j = 0
options.set_custom_format(3,10)
options.p = 0.5;
for i in range(1,nsamp+1):
    y = roundit(0.,options)
    if y != 0: j += 1

assert abs(j/float(nsamp) - options.p) <= 1e-2

print('All tests successful!\n')
