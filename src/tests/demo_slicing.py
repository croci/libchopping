from chopping import *
import numpy as np
from math import pi

a = np.arange(1,6)*pi

op = Option()
op.set_format("h")

b = LPV(a, op)

print(a)
print(b.array())

# pi is rounded
b[0] = pi;

# this works as it should (a copy is made)
b[1] = -b[2];

# pi/2 is rounded
b[3] = pi/2;

# this works as it should (a copy is made since it is an assignment between scalar LPVs)
b[4] = b[0];

# since a copy was made in the previous step, this operation only affects b[4] and not b[0]
b[4] = -b[4];

print(b.array())

# if this were c++ b2 would work like a pointer and if we multiply it by 2 b[0] would be affected
# as well. However, this does not seem to happen on the Python side. 
# the behaviour of b3 is the same in both Python and C++

b2 = b[0];
b3 = -b[0];

b2 = b2*2;

print(b2.array(), b3.array(), b[0].array())

## this operation is ignored on the C++ side, but gives an error on the Python side.
#b4 = 2*b
#b4[0] = b
#
#print(b.array(), b4.array())
