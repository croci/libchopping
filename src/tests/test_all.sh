#!/usr/bin/bash
python3 -c "from chopping import *" && echo "PYTHONPATH SET CORRECTLY!" || (echo "PYTHONPATH NOT CORRECTLY SET. HAVE YOU SET THE PYTHONPATH?" && return 1)
python3 test_bindings.py >/dev/null && echo "BINDINGS TEST PASSED!" || echo "BINDING TEST FAILED!"
python3 test_roundit.py >/dev/null && echo "ROUNDIT TEST PASSED!" || echo "ROUNDIT TEST FAILED!"
python3 test_chop.py >/dev/null && echo "CHOP TEST PASSED!" || echo "CHOP TEST FAILED!"
python3 test_fastchop.py >/dev/null && echo "FASTCHOP TEST PASSED!" || echo "FASTCHOP TEST FAILED!"
python3 test_linalg.py >/dev/null && echo "LINALG TEST PASSED!" || echo "LINALG TEST FAILED!"
python3 test_mg_lpv.py >/dev/null && echo "MULTIGRID TEST PASSED!" || echo "MULTIGRID TEST FAILED!"
echo "TESTING COMPLETE"
