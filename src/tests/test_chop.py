import numpy as np
from numpy.random import rand, randn
from scipy.linalg import hilbert
from chopping import *

#NOTE: we did not translate all tests from test_chop.m. At the moment the interface does not support single precision inputs.

eps = lambda x : np.spacing(x)

def float_params(prec):
    if prec == "h": p = 11; emax = 15;
    if prec == "b": p = 8; emax = 127;
    if prec == "s": p = 24; emax = 127;
    if prec == "d": p = 53; emax = 1023;
    if prec == "q": p = 113; emax = 16383;

    emin = 1-emax;            # Exponent of smallest normal number.
    emins = emin + 1 - p;     # Exponent of smallest subnormal number.
    xmins = 2**emins;
    xmin = 2**emin;
    xmax = 2**emax * (2-2**(1-p));
    u = 2**(-p);
    return (u,xmins,xmin,xmax,p,emins,emin,emax)


def chk(a,b):
    if isinstance(a,np.ndarray):
        mask = np.isfinite(a)
        assert(abs(np.array(a[mask]-b[mask])).max()<1.0e-16)
    else:
        assert(abs(np.array(a-b)).max()<1.0e-16)

def chopc(A,opt):
    if isinstance(A,np.ndarray):
        B = A.copy()
        chop(B,opt)
        return B
    else:
        return chop(A,opt)

n = 0;
uh = 2**(-11);  # Unit roundoff for fp16.
pi_h = 6432*uh; # fp16(pi)
    
options = Option()

# Test matrix.
options.set_format('b');
A = np.array([[16, 2, 3, 13],[5, 11, 10, 8],[9, 7, 6, 12],[4, 14, 15, 1]])
C = chopc(A,options);
chk(A,C)
B = A + randn(*A.shape)*1e-12;
C = chopc(B,options);
chk(A,C)
A2 = hilbert(6); C = chopc(A2,options);

# bfloat16
tt = 8; eemax = 127
options.set_custom_format(tt, eemax);
C1 = chopc(A,options);
chk(A,C1)
C2 = chopc(B,options);
chk(A,C2)
chk(C,chopc(A2,options))

# half
tt = 11
eemax = 15 
options = Option()
options2 = Option()
options.set_custom_format(tt, eemax);
options2.set_format('h');
A = hilbert(6);
X1 = chopc(A,options);
X2 = chopc(A,options2);
chk(X1,X2)

# Row vector
options = Option()
options.set_format('h');
A = np.arange(-10,11)
C = chopc(A,options);
chk(A,C)
B = A + randn(*A.shape)*1e-12;
C = chopc(B,options);
chk(A,C)

# Column vector
options.set_format('s');
A = np.arange(-10,11).reshape((-1,1))
C = chopc(A,options);
chk(A,C)
B = A + A*rand(*A.shape)*1e-14;  # Keep 0 as 0.
C = chopc(B,options);
chk(A,C)

###################################################################
# Main loop: test np.float32 and half formats.
for i in [1,2]:

    options = Option()

    if i == 1:
        # Single precision tests.
        u,xmins,xmin,xmax,p,emins,emin,emax = float_params('s');
        options.set_format('s');
    elif i == 2:
        # Half precision tests.
        u,xmins,xmin,xmax,p,emins,emin,emax = float_params('h');
        options.set_format('h');

    options.subnormal = 0;
    
    x = np.pi;
    if i == 1:
        y = float(np.float32(np.pi))
    elif i == 2:
        y = pi_h; # float(fp16(x));
    c = chopc(x,options);
    chk(c,y)
    x = -np.pi;
    c = chopc(x,options);
    chk(c,-y)
    
    # Next number power of 2.
    y = 2**10;
    if i == 1:
        dy = float(eps(np.float32(y)));
    elif i == 2:
        dy = 2*y*uh; # float(eps(fp16(y)));
    
    x = y + dy;
    c = chopc(x,options);
    chk(c,x)
    
    # Number just before a power of 2.
    y = 2**10; x = y - dy;
    c = chopc(x,options);
    chk(c,x)
    
    # Next number power of 2.
    y = 2**(-4);
    if i == 1:
        dy = float(eps(np.float32(y)));
    elif i == 2:
        dy = 2*y*uh; # float(eps(fp16(y)));

    x = y + dy;
    c = chopc(x,options);
    chk(c,x)

    # Check other rounding options
    for rmode in range(1,7):
        options.set_round(rmode);
        x = y + (dy*10**(-3));
        c = chopc(x,options);
        if options.get_round() == 2:
            chk(c,y+dy) # Rounding up
        elif options.get_round() >= 5:
            # Check rounded either up or down.
            if c != y+dy:
                chk(c,y)
        else:
            chk(c,y)

    options.set_round(1); # reset the rounding mode to default

    # Overflow tests.
    x = xmax;
    c = chopc(x,options);
    chk(c,x)
    
    # IEEE 2008, page 16: rule for rounding to infinity.
    x = 2**emax * (2-(1/2)*2**(1-p));  # Round to inf.
    c = chopc(x,options);
    assert np.isinf(c)
    x = 2**emax * (2-(3/4)*2**(1-p));  # Round to realmax.
    c = chopc(x,options);
    chk(c,xmax)
    
    # Round to nearest.
    if i == 2:
       x = 1 + 2**(-11);
       c = chopc(x,options);
       chk(c,1)
    
    # Underflow tests.
    if i == 1:
        delta = float(eps(np.float32(1)));
    else:
        delta = 2*uh; # float(eps(fp16(1)));
    
    options.subnormal = 1;
    c = chopc(xmin,options); chk(c,xmin)
    x = np.array([xmins, xmin/2, xmin, 0, xmax, 2*xmax, 1-delta/5, 1+delta/4])
    c = chopc(x,options);
    c_expected = np.concatenate([x[:5], np.array([np.inf, 1, 1])]);
    chk(c,c_expected)
    
    options.subnormal = 0;
    c = chopc(xmin,options); chk(c,xmin)
    x = np.array([xmins, xmin/2, xmin, 0, xmax, 2*xmax, 1-delta/5, 1+delta/4])
    c = chopc(x,options);
    c_expected = np.array([0, 0, x[2], x[3], x[4], np.inf, 1, 1]);
    chk(c,c_expected)
    
    # Smallest normal number and spacing between the subnormal numbers.
    y = xmin; delta = xmin*2**(1-p);
    x = y - delta; # The largest subnormal number.
    options.subnormal = 1;
    c = chopc(x,options);
    chk(c,x)
    # Now try flushing to zero.
    options.subnormal = 0;
    c = chopc(x,options);
    chk(c,0)
    
    options.subnormal = 1;
    x = xmins*8;  # A subnormal number.
    c = chopc(x,options);
    chk(c,x)
    
    # Number too small too represent.
    x = xmins/2; c = chopc(x,options); chk(c,0)
    options.subnormal = 1;
    x = xmins/2; c = chopc(x,options); chk(c,0)
    
    # Do not limit exponent.
    options.explim = 0;
    x = xmin/2;  c = chopc(x,options); chk(c,x)
    x = -xmin/2;  c = chopc(x,options); chk(c,x)
    x = xmax*2;  c = chopc(x,options); chk(c,x)
    x = -xmax*2;  c = chopc(x,options); chk(c,x)
    x = xmins/2; c = chopc(x,options); chk(c,x)
    x = -xmins/2; c = chopc(x,options); chk(c,x)
    

###################################################################

options = Option()

# Double precision tests.
(u,xmins,xmin,xmax,p,emins,emin,emax) = float_params('d');
options.set_format('d');
x = np.array([1e-309, 1e-320, 1, 1e306])  # First two entries are subnormal.
c = chopc(x,options);
chk(c,x)
options.subnormal = 0;
c = chopc(x,options);
chk(c,np.array([0, 0, x[2], x[3]]))

options.set_format('d'); options.subnormal = 0
a = chopc(np.pi,options); chk(a,np.pi)
options.set_format('d'); options.subnormal = 1
a = chopc(np.pi,options); chk(a,np.pi)

x = np.pi**2;
options = Option()
options.set_format('d');
y = chopc(x,options);  # Should not change x.
chk(x,y);
options.set_round(2);
y = chopc(x,options);  # Should not change x.
chk(x,y);
options.set_round(3);
y = chopc(x,options);  # Should not change x.
chk(x,y);
options.set_round(4);
y = chopc(x,options);  # Should not change x.
chk(x,y);

###################################################################

n = 0;
uh = 2**(-11);  # Unit roundoff for fp16.
pi_h = 6432*uh; # fp16(pi)
    
options = Option(True)

# Test matrix.
options.set_format('b');
A = np.array([[16, 2, 3, 13],[5, 11, 10, 8],[9, 7, 6, 12],[4, 14, 15, 1]])
C = chopc(A,options);
chk(A,C)
B = A + randn(*A.shape)*1e-12;
C = chopc(B,options);
chk(A,C)
A2 = hilbert(6); C = chopc(A2,options);

# bfloat16
options.set_format("b")
options2 = Option(False)
options2.set_format("b")
C1 = chopc(A,options);
C2 = chopc(A,options2);
chk(C1,C2)
C1 = chopc(B,options);
C2 = chopc(B,options2);
chk(C1,C2)

# half
options.set_format("h")
options2 = Option(False)
options2.set_format("h")
C1 = chopc(A,options);
C2 = chopc(A,options2);
chk(C1,C2)
C1 = chopc(B,options);
C2 = chopc(B,options2);
chk(C1,C2)

# single
options.set_format("s")
options2 = Option(False)
options2.set_format("s")
C1 = chopc(A,options);
C2 = chopc(A,options2);
chk(C1,C2)
C1 = chopc(B,options);
C2 = chopc(B,options2);
chk(C1,C2)

# Row vector
options = Option()
options.set_format('h');
A = np.arange(-10,11)
C = chopc(A,options);
chk(A,C)
B = A + randn(*A.shape)*1e-12;
C = chopc(B,options);
chk(A,C)

# Column vector
options.set_format('s');
A = np.arange(-10,11).reshape((-1,1))
C = chopc(A,options);
chk(A,C)
B = A + A*rand(*A.shape)*1e-14;  # Keep 0 as 0.
C = chopc(B,options);
chk(A,C)

###################################################################
# Main loop: test np.float32 and half formats.
for i in [1,2]:

    options = Option(True)

    if i == 1:
        # Single precision tests.
        u,xmins,xmin,xmax,p,emins,emin,emax = float_params('s');
        options.set_format('s');
    elif i == 2:
        # Half precision tests.
        u,xmins,xmin,xmax,p,emins,emin,emax = float_params('h');
        options.set_format('h');

    #options.subnormal = 0;
    
    x = np.pi;
    if i == 1:
        y = float(np.float32(np.pi))
    elif i == 2:
        y = pi_h; # float(fp16(x));
    c = chopc(x,options);
    chk(c,y)
    x = -np.pi;
    c = chopc(x,options);
    chk(c,-y)
    
    # Next number power of 2.
    y = 2**10;
    if i == 1:
        dy = float(eps(np.float32(y)));
    elif i == 2:
        dy = 2*y*uh; # float(eps(fp16(y)));
    
    x = y + dy;
    c = chopc(x,options);
    chk(c,x)
    
    # Number just before a power of 2.
    y = 2**10; x = y - dy;
    c = chopc(x,options);
    chk(c,x)
    
    # Next number power of 2.
    y = 2**(-4);
    if i == 1:
        dy = float(eps(np.float32(y)));
    elif i == 2:
        dy = 2*y*uh; # float(eps(fp16(y)));

    x = y + dy;
    c = chopc(x,options);
    chk(c,x)

    # Check other rounding options
    for rmode in [1,5]:
        options.set_round(rmode);
        x = y + (dy*10**(-3));
        c = chopc(x,options);
        if options.get_round() == 2:
            chk(c,y+dy) # Rounding up
        elif options.get_round() >= 5:
            # Check rounded either up or down.
            if c != y+dy:
                chk(c,y)
        else:
            chk(c,y)

    options.set_round(1); # reset the rounding mode to default

    # Overflow tests.
    x = xmax;
    c = chopc(x,options);
    chk(c,x)
    
    # IEEE 2008, page 16: rule for rounding to infinity.
    x = 2**emax * (2-(1/2)*2**(1-p));  # Round to inf.
    c = chopc(x,options);
    assert np.isinf(c)
    x = 2**emax * (2-(3/4)*2**(1-p));  # Round to realmax.
    c = chopc(x,options);
    chk(c,xmax)
    
    # Round to nearest.
    if i == 2:
       x = 1 + 2**(-11);
       c = chopc(x,options);
       chk(c,1)
    
    # Underflow tests.
    if i == 1:
        delta = float(eps(np.float32(1)));
    else:
        delta = 2*uh; # float(eps(fp16(1)));
    
    #options.subnormal = 1;
    c = chopc(xmin,options); chk(c,xmin)
    x = np.array([xmins, xmin/2, xmin, 0, xmax, 2*xmax, 1-delta/5, 1+delta/4])
    c = chopc(x,options);
    c_expected = np.concatenate([x[:5], np.array([np.inf, 1, 1])]);
    chk(c,c_expected)
    
    # Smallest normal number and spacing between the subnormal numbers.
    y = xmin; delta = xmin*2**(1-p);
    x = y - delta; # The largest subnormal number.
    c = chopc(x,options);
    chk(c,x)
    
    x = xmins*8;  # A subnormal number.
    c = chopc(x,options);
    chk(c,x)
    
    # Number too small too represent. Multiply by 0.9 since with fast we still do RtN between 0 and xmins (i.e. xmins/2 is rounded to xmins)
    x = xmins/2*0.9; c = chopc(x,options); chk(c,0)
    
###################################################################

x = np.pi**2;
options = Option(True)
options.set_format('d');
y = chopc(x,options);  # Should not change x.
chk(x,y);

print('All tests successful!\n')
