#include "LPV.hpp"

int main(int argc, char *argv[]){
    VSLStreamStatePtr stream;
    vslNewStream(&stream, VSL_BRNG_MT19937, 777);
    int status = 0;
    const unsigned int m = 10, n = 20, N = m*n;
    double * in = (double*) allocate(N*sizeof(double));
    double * out = (double*) allocate(N*sizeof(double));
    status = vdRngGaussian( VSL_RNG_METHOD_GAUSSIAN_ICDF, stream, N, in, 0, 1);

    for(unsigned int i=0;i!=N;i++)
        std::cout << in[i] << " ";
    std::cout << "\n";

    Option opts;
    opts.set_format('h');
    opts.round = 1;
    opts.flip = false;

    LPV<double> lpv(in,m,n,&opts);
    LPV<double> lpv2(in, n, m, &opts);
    LPV<double> lpv3(transpose(lpv2));
    //lpv3 = (lpv*(0.5 + lpv3))/1;// + lpv/lpv3;
    std::cout << lpv3.x[0] << " " << lpv3.x[1] << "\n";
    lpv3 = lpv3/5;
    std::cout << lpv3.x[0] << " " << lpv3.x[1] << "\n\n";
    return 0;
    lpv3 = mtimes(lpv2,lpv3);

    for(unsigned int i=0;i!=N;i++)
        std::cout << lpv3.x[i] << " ";
    std::cout << "\n";

    mkl_free(in);
    mkl_free(out);
    //std::free(in);
    //std::free(out);

    return 0;
}
