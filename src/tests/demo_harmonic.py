import numpy as np
from chopping import *

opt = Option()

print('- Standard Option                             \n')
print('  Format    Round mode      Sum      No. terms\n')
print('----------------------------------------------\n')

for p in [0,1,2]:
    fm = ["c", "b", "h"][p]
    fullfm = ["custom", "bfloat16", "fp16"][p]
    if fm == "c": opt.set_custom_format(5,3)
    else:         opt.set_format(fm)

    for i in range(1,7):
        opt.set_round(i)

        s = 0; n = 1
        while True:
            sold = s + 0
            s = chop(s + chop(1./n, opt), opt)
            if s == sold:
                break
            n += 1

        print('%10s     %1.0f       %9.4e       %d\n' % (fullfm , i, s, n))

opt = Option(True)

print("")
print('- Fast Option                                 \n')
print('  Format    Round mode      Sum      No. terms\n')
print('----------------------------------------------\n')

for p in [0,1]:
    fm = ["b", "h"][p]
    fullfm = ["bfloat16", "fp16"][p]
    opt.set_format(fm)

    for i in [1,5]:
        opt.set_round(i)

        s = 0; n = 1
        while True:
            sold = s + 0
            s = chop(s + chop(1./n, opt), opt)
            if s == sold:
                break
            n += 1

        print('%10s     %1.0f       %9.4e       %d\n' % (fullfm , i, s, n))
