from chopping import *
import numpy as np
from time import time
import sys

N = 10**5
fmts = ["h", "b", "s"]

# first, test RtN
op = Option(True)
op.set_round(1)

ops = Option(False)
ops.set_round(1)

for fmt in fmts:
    op.set_format(fmt)
    ops.set_format(fmt)

    a = np.random.randn(N)
    b = a.copy()
    chop(a,op)
    chop(b,ops)
    assert np.allclose(a,b)

    a = np.random.randn(N)
    a1 = np.array([chop(item, op) for item in a])
    a2 = np.array([chop(item, ops) for item in a])
    assert np.allclose(a1,a2)

# then, test SR
M = 1000; m = 100
op.set_round(5)
ops = Option(False); ops.set_round(6) # round up/down with 50% chance
def chops(x,op):
    if isinstance(x,np.ndarray):
        y = x.copy()
        chop(y,op)
        return y
    else:
        return chop(x,op)

def checkSR(x):
    # check that the values are correct
    vals = np.array([chops(x,op) for i in range(M)])
    uvals = np.unique(vals)
    if len(uvals) == 1 and abs(uvals[0]-x)/abs(x) < 4/M: return # if too close to one value we might never get the other one
    checkvals = np.array([chops(x,ops) for i in range(100)])
    ucheckvals = np.unique(checkvals)
    assert len(uvals) == len(ucheckvals)
    assert np.allclose(np.sort(uvals), np.sort(ucheckvals))

    # check that SR is exact on average
    avg = np.mean(vals)
    err = abs(x-avg)/abs(x)
    ref_err = np.sqrt(abs(x-ucheckvals[1])*abs(x-ucheckvals[0]))/np.sqrt(M)/abs(ucheckvals[0]-ucheckvals[1])
    assert err < 2*ref_err

for fmt in fmts:
    op.set_format(fmt)
    pms = op.get_floating_point_parameters()
    xmins = pms["xmins"]; xmin = pms["xmin"] # minimum positive subnormal number and minimum positive normal number
    ops.set_format(fmt)

    a = np.random.randn(m)
    [checkSR(item) for item in a] # check normal numbers are rounded correctly
    chop(a, op); b = a.copy(); chop(b,op); assert np.allclose(a,b) # check that we never round away from a representable value

    a = (xmin-xmins)*np.random.rand(m) + xmins # random subnormal numbers
    [checkSR(item) for item in a] # check subnormals are rounded correctly
    chop(a, op); b = a.copy(); chop(b,op); assert np.allclose(a,b) # check that we never round away from a representable value

    a = np.random.randn(m)
    [checkSR(np.array([item])) for item in a] # check normal numbers are rounded correctly
    chop(a, op); b = a.copy(); chop(b,op); assert np.allclose(a,b) # check that we never round away from a representable value

    a = (xmin-xmins)*np.random.rand(m) + xmins # random subnormal numbers
    [checkSR(np.array([item])) for item in a] # check subnormals are rounded correctly
    chop(a, op); b = a.copy(); chop(b,op); assert np.allclose(a,b) # check that we never round away from a representable value

print("Test successful!")
