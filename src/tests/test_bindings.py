import chopping
import numpy as np

print("\n\n--------------- Normal Option --------------\n")

opt = chopping.Option()
opt.set_format("h")
opt.set_round(1)

a = np.random.randn(5)

b = 2*np.pi

print(a)

chopping.chop(a,opt)

print(a)

print(b)

chopping.chop(b,opt)

print(b)

b = chopping.chop(b,opt)

print(b)

A = chopping.LPV()

B = chopping.LPV(A)

B = chopping.LPV(a,opt, False, True)

B = chopping.LPV(a,opt)

A = B + B;

x = B.array()
print(x)

x = A.array()
print(x)
x = 2*x
print(x)
x = A.array()
print(x)

x = np.random.randn(4,5)
y = np.random.randn(5,3)

X = chopping.LPV(x,opt)
Y = chopping.LPV(y,opt)
Z = X@Y
z = x@y
print(z, "\n", Z.array())


z = np.random.randn(25,25)
Z = chopping.LPV(z,opt)
print((Z.T()@Z).array())

N = 26
lwd = np.random.randn(N-1)
ud = np.random.randn(N-1)
d = np.random.randn(N)
b = np.random.randn(N)

A = np.diag(lwd,-1) + np.diag(ud, 1) + np.diag(d)

out1 = A.dot(b)
out2 = np.linalg.solve(A,b)

lwd = chopping.LPV(lwd, opt)
ud  = chopping.LPV(ud, opt)
d   = chopping.LPV(d, opt)
b   = chopping.LPV(b, opt)

outa = chopping.matvec_tridiag(lwd, d, ud, b).array()
outb = chopping.solve_tridiag(lwd, d, ud, b).array()

err1 = abs(outa - out1)
err2 = abs(outb - out2)
print("\n", err1, "\n\n", err2)

print("\n\n---------------  Fast Option  --------------\n")

opt = chopping.Option(fast=True)
opt.set_format("h")
opt.set_round(1)

a = np.random.randn(5)

b = 2*np.pi

print(a)

chopping.chop(a,opt)

print(a)

print(b)

chopping.chop(b,opt)

print(b)

b = chopping.chop(b,opt)

print(b)

A = chopping.LPV()

B = chopping.LPV(A)

B = chopping.LPV(a,opt, False, True)

B = chopping.LPV(a,opt)

A = B + B;

x = B.array()
print(x)

x = A.array()
print(x)
x = 2*x
print(x)
x = A.array()
print(x)

x = np.random.randn(4,5)
y = np.random.randn(5,3)

X = chopping.LPV(x,opt)
Y = chopping.LPV(y,opt)
Z = X@Y
z = x@y
print(z, "\n", Z.array())


z = np.random.randn(25,25)
Z = chopping.LPV(z,opt)
print((Z.T()@Z).array())

N = 26
lwd = np.random.randn(N-1)
ud = np.random.randn(N-1)
d = np.random.randn(N)
b = np.random.randn(N)

A = np.diag(lwd,-1) + np.diag(ud, 1) + np.diag(d)

out1 = A.dot(b)
out2 = np.linalg.solve(A,b)

lwd = chopping.LPV(lwd, opt)
ud  = chopping.LPV(ud, opt)
d   = chopping.LPV(d, opt)
b   = chopping.LPV(b, opt)

outa = chopping.matvec_tridiag(lwd, d, ud, b).array()
outb = chopping.solve_tridiag(lwd, d, ud, b).array()

err1 = abs(outa - out1)
err2 = abs(outb - out2)
print("\n", err1, "\n\n", err2)

print("All test successful!")
