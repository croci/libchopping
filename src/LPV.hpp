#include "fastchop.hpp"

template <typename real>
class LPV {
    public:
        real * x __attribute__((aligned(ALIGN)));
        const Option * opts;
        unsigned int m,n,N;
        bool indexed;

        LPV(real * y, const unsigned int mm, const unsigned int nn, const Option * op, const bool inplace=false, const bool needchopping=true, const bool indexed=false);

        LPV();

        LPV(const LPV &a);

        void swap(LPV &other);

        LPV& operator=(LPV other);
        LPV& operator=(real val);

        LPV<real> operator[](const unsigned int i) const {
            if(i > N - 1)
                throw std::invalid_argument("Index requested is out of bounds for the LPV variable.");
            // NOTE: we are leaving things inplace. This is essential for the assignment operator to work
            //       on slices.
            return LPV<real>(&x[i], 1, 1, opts, true, false, true);
        }

        real* vec();

        LPV operator-();

        ~LPV();

        std::string str() const {
            std::stringstream buffer;
            buffer << *this;
            return std::regex_replace(buffer.str(), std::regex( "\\+" ), " " );
            //return buffer.str();
        }

        template <typename reals>
        friend std::ostream& operator<<(std::ostream &out, const LPV<reals> &a);

        template <typename reals>
        friend reals value(const LPV<reals> &a);

        // plus
        template <typename reals>
        friend LPV<reals> operator+(const LPV<reals> &a, const LPV<reals> &b);
        template <typename reals, typename T>
        friend LPV<reals> operator+(const LPV<reals> &a, const T &b);
        template <typename reals, typename T>
        friend LPV<reals> operator+(const T &a, const LPV<reals> &b);

        // times
        template <typename reals>
        friend LPV<reals> operator*(const LPV<reals> &a, const LPV<reals> &b);
        template <typename reals, typename T>
        friend LPV<reals> operator*(const LPV<reals> &a, const T &b);
        template <typename reals, typename T>
        friend LPV<reals> operator*(const T &a, const LPV<reals> &b);

        // minus
        template <typename reals>
        friend LPV<reals> operator-(const LPV<reals> &a, const LPV<reals> &b);
        template <typename reals, typename T>
        friend LPV<reals> operator-(const LPV<reals> &a, const T &b);
        template <typename reals, typename T>
        friend LPV<reals> operator-(const T &a, const LPV<reals> &b);
        
        // divide
        template <typename reals>
        friend LPV<reals> operator/(const LPV<reals> &a, const LPV<reals> &b);
        template <typename reals, typename T>
        friend LPV<reals> operator/(const LPV<reals> &a, const T &b);
        template <typename reals, typename T>
        friend LPV<reals> operator/(const T &a, const LPV<reals> &b);

        // matrix-matrix multiplication
        template <typename reals>
        friend LPV<reals> mtimes(const LPV<reals> &a, const LPV<reals> &b);

        // transposition
        template <typename reals>
        friend LPV<reals> transpose(const LPV<reals> &a);

    private:

};

template <typename real>
void check_all(const LPV<real> &a, const LPV<real> &b){
    check_opts(a,b);
    check_sizes(a,b);
}

template <typename real, typename T>
void check_all(const LPV<real> &a, const T &b){
    if (!std::is_arithmetic<T>::value)
        throw std::invalid_argument("Only operations supported are LPV with LPV or LPV with scalars");

    if(a.N == 0)
        throw std::invalid_argument("Operation not supported with empty LPV.");
}

template <typename real, typename T>
void check_all(const T &a, const LPV<real> &b){
    if (!std::is_arithmetic<T>::value)
        throw std::invalid_argument("Only operations supported are LPV with LPV or LPV with scalars");

    if(b.N == 0)
        throw std::invalid_argument("Operation not supported with empty LPV.");
}


template <typename real>
void check_opts(const LPV<real> &a, const LPV<real> &b){
    if (a.opts != b.opts)
        throw std::invalid_argument("Cannot work with low precision variables with different Options.\n");
}

template <typename real>
void check_sizes(const LPV<real> &a, const LPV<real> &b){
    if((a.N != b.N) || (a.m != b.m) || (a.n != b.n) || (a.N == 0))
        throw std::invalid_argument("Dimensions of the variables do not match.\n");
}

template<typename real>
real* LPV<real>::vec(){
    return x;
}

template<typename real>
LPV<real>::LPV(real * y, const unsigned int mm, const unsigned int nn, const Option * op, const bool inplace, const bool needchopping, const bool isindexed){
    m = mm;
    n = nn;
    N = mm*nn;
    indexed = isindexed;
    opts = op;
    if(!needchopping){
        if(!inplace){
            x = (real*) allocate(N*sizeof(real));
            memcpy(x,y,N*sizeof(real));
        }else{
            x = y;
        }
    } else {
        if(!inplace){
            x = (real*) allocate(N*sizeof(real));
            chop(y, x, N, opts);
        }else{
            if(N>1)
                chop(y, N, opts);
            else
                chop(*y, opts);
            x = y;
        }
    }
}

template<typename real>
LPV<real>::LPV(){
    x = NULL;
    opts = NULL;
    indexed = false;
    m = 0; n = 0; N = 0;
}

template<typename real>
LPV<real>::LPV(const LPV<real> &a){
    if(a.x == NULL){
        x = NULL;
        opts = NULL;
        indexed = false;
        m = 0; n = 0; N = 0;
    }else{
        m = a.m;
        n = a.n;
        N = a.N;
        opts = a.opts;
        indexed = false; // since allocation is done we can treat it as non-indexed
        x = (real*) allocate( N*sizeof(real));
        memcpy(x,a.x,N*sizeof(real));
    }
}

template <typename reals>
std::ostream& operator<<(std::ostream &out, const LPV<reals> &a){
    const unsigned int N = a.N;
    const unsigned int m = a.m;
    const unsigned int n = a.n;

    out << "\n";
    if(a.x == NULL){
        out << "LPV_array([])\n";
        return out;
    }
    if(n == 1){
        out << "LPV_array([";
        for(unsigned int i=0; i<N-1; i++){
            out << a.x[i] << ", ";
        }
        out << a.x[N-1] << "])\n";
        return out;
    }
    out << "LPV_array([[";
    out << std::fixed << std::setprecision(5) << std::setfill(' ') << std::showpos;
    for(unsigned int i=0; i<m; i++){
        if (i > 0)
            out << "           [";

        for(unsigned int j=0; j<n-1; j++)
            out << std::setw(6) << a.x[n*i + j] << ", ";
        
        if (i<m-1)
            out << std::setw(6) << a.x[n*i + n-1] << "],\n";
        else
            out << std::setw(6) << a.x[n*i + n-1] << "]";
    }
    out << "])\n";
    return out;
}

// Note: argument passed by value!
template<typename real>
LPV<real>& LPV<real>::operator=(LPV<real> other) {
    if(&other == this)
        return *this;

    if(other.x == NULL)
        throw std::invalid_argument("Copying or assigning an empty LPV is not allowed.");

    // MYNOTE: if this is scalar-valued with the same options
    // it is sufficient to copy the value of x[0];
    // this trick also allows assignments of scalar
    // LPV variables to slices of LPV vectors, e.g.
    // LPV[i] = scalar_LPV;
    // LPV1[i] = LPV2[j];
    if(other.N == 1 && N == 1 && opts == other.opts){
        x[0] = other.x[0];
    }else{
        // MYNOTE: if you try to do a[2] = b, where a and b are LPV vectors,
        // you get here. However, the swap function just swaps the pointers of a[2]
        // which are just copies. The final effect is that a is unaffected and that
        // the copy of a[2] is deleted without harm.
        //
        // swap this with other
        swap(other);
    }

    // by convention, always return *this
    return *this;
    // other is destroyed, releasing the memory
    // MYNOTE: if you do LPV1[i] = LPV2[j],
    // LPV2[j] will not be necessarily destroyed.
}

// The swap member function (should never fail!).
template<typename real>
void LPV<real>::swap(LPV<real> &other) {
    // swap all the members (and base subobject, if applicable) with other
    std::swap(x, other.x);
    std::swap(opts, other.opts);
    std::swap(m, other.m);
    std::swap(n, other.n);
    std::swap(N, other.N);
    std::swap(indexed, other.indexed);
}

// Note: argument passed by value!
template<typename real>
LPV<real>& LPV<real>::operator=(real val){
    if(N != 1)
        throw std::invalid_argument("Can only assign scalar values to a scalar LPV.");

    // val is a local copy so this is safe
    chop(val, opts);
    x[0] = val;

    // by convention, always return *this
    return *this;
}

template<typename real>
LPV<real> LPV<real>::operator-(){
    LPV<real> lpout = LPV(*this); 
    if(N>1){
        real * lpoutx __attribute__((aligned(ALIGN)));
        real * xx __attribute__((aligned(ALIGN)));
        xx = x; lpoutx = lpout.x;
        #ifdef __INTEL_COMPILER
            __assume_aligned(xx, ALIGN);
            __assume_aligned(lpoutx, ALIGN);
        #endif
        #pragma omp simd aligned(xx, lpoutx: ALIGN)
        for(unsigned int i=0; i<N;i++)
            lpoutx[i] = -xx[i];
    }else{
        lpout.x[0] = -x[0];
    }
    return lpout;
}

template<typename real>
LPV<real>::~LPV(){
    if (x!=NULL && !indexed){
        mkl_free(x);
        //std::free(x);
        x = NULL;
    }
}

template <typename real>
LPV<real> operator+(const LPV<real> &a, const LPV<real> &b){
    check_all(a, b);

    LPV<real> lpout;

    if(a.N == 1){
        real out_val = a.x[0] + b.x[0];
        lpout = LPV<real>(&out_val, a.m, a.n, a.opts, false);
    }else{
        real * out __attribute__((aligned(ALIGN)));
        real * ax __attribute__((aligned(ALIGN)));
        real * bx __attribute__((aligned(ALIGN)));
        out = (real*) allocate( a.N*sizeof(real));
        ax = a.x; bx = b.x;

        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
            __assume_aligned(ax, ALIGN);
            __assume_aligned(bx, ALIGN);
        #endif
        #pragma omp simd aligned(out, ax, bx: ALIGN)
        for(unsigned int i=0; i<a.N; i++)
            out[i] = ax[i] + bx[i];

        lpout = LPV<real>(out, a.m, a.n, a.opts, true);
    }

    return lpout;
} 

template <typename real, typename T>
LPV<real> operator+(const LPV<real> &a, const T &b){
    check_all(a, b);

    LPV<real> lpout;

    real bc = (real) b;
    chop(bc,a.opts);

    if(a.N == 1){
        real out_val = a.x[0] + bc;
        lpout = LPV<real>(&out_val, a.m, a.n, a.opts, false);
    }else{

        real * ax __attribute__((aligned(ALIGN)));
        real * out __attribute__((aligned(ALIGN)));
        out = (real*) allocate( a.N*sizeof(real));
        ax = a.x;

        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
            __assume_aligned(ax, ALIGN);
        #endif
        #pragma omp simd aligned(out, ax: ALIGN)
        for(unsigned int i=0; i<a.N; i++)
            out[i] = ax[i] + bc;

         lpout = LPV<real>(out, a.m, a.n, a.opts, true);
    }

    return lpout;
} 

template <typename real, typename T>
LPV<real> operator+(const T &a, const LPV<real> &b){
    return b + a;
} 

template <typename real>
LPV<real> operator-(const LPV<real> &a, const LPV<real> &b){
    check_all(a, b);

    LPV<real> lpout;

    if(a.N == 1){
        real out_val = a.x[0] - b.x[0];
        lpout = LPV<real>(&out_val, a.m, a.n, a.opts, false);
    }else{
        real * ax __attribute__((aligned(ALIGN)));
        real * bx __attribute__((aligned(ALIGN)));
        real * out __attribute__((aligned(ALIGN)));
        out = (real*) allocate(a.N*sizeof(real));
        ax = a.x; bx = b.x;

        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
            __assume_aligned(ax, ALIGN);
            __assume_aligned(bx, ALIGN);
        #endif
        #pragma omp simd aligned(out, ax, bx: ALIGN)
        for(unsigned int i=0; i<a.N; i++)
            out[i] = ax[i] - bx[i];

        lpout = LPV<real>(out, a.m, a.n, a.opts, true);
    }

    return lpout;
} 

template <typename real, typename T>
LPV<real> operator-(const LPV<real> &a, const T &b){
    check_all(a, b);

    LPV<real> lpout;

    real bc = (real) b;
    chop(bc,a.opts);

    if(a.N == 1){
        real out_val = a.x[0] - bc;
        lpout = LPV<real>(&out_val, a.m, a.n, a.opts, false);
    }else{

        real * ax __attribute__((aligned(ALIGN)));
        real * out __attribute__((aligned(ALIGN)));
        out = (real*) allocate( a.N*sizeof(real));
        ax = a.x;

        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
            __assume_aligned(ax, ALIGN);
        #endif
        #pragma omp simd aligned(out, ax: ALIGN)
        for(unsigned int i=0; i<a.N; i++)
            out[i] = ax[i] - bc;

        lpout = LPV<real>(out, a.m, a.n, a.opts, true);
    }

    return lpout;
} 

template <typename real, typename T>
LPV<real> operator-(const T &a, const LPV<real> &b){
    check_all(a, b);

    LPV<real> lpout;

    real ac = (real) a;
    chop(ac,b.opts);

    if(b.N == 1){
        real out_val = ac - b.x[0];
        lpout = LPV<real>(&out_val, b.m, b.n, b.opts, false);
    }else{

        real * bx __attribute__((aligned(ALIGN)));
        real * out __attribute__((aligned(ALIGN)));
        out = (real*) allocate( b.N*sizeof(real));
        bx = b.x;

        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
            __assume_aligned(bx, ALIGN);
        #endif
        #pragma omp simd aligned(out, bx: ALIGN)
        for(unsigned int i=0; i<b.N; i++)
            out[i] = ac - bx[i];

        lpout = LPV<real>(out, b.m, b.n, b.opts, true);
    }

    return lpout;
} 

template <typename real>
LPV<real> operator*(const LPV<real> &a, const LPV<real> &b){
    check_all(a, b);

    LPV<real> lpout;

    if(a.N == 1){
        real out_val = a.x[0] * b.x[0];
        lpout = LPV<real>(&out_val, a.m, a.n, a.opts, false);
    }else{
        real * ax __attribute__((aligned(ALIGN)));
        real * bx __attribute__((aligned(ALIGN)));
        real * out __attribute__((aligned(ALIGN)));
        out = (real*) allocate(a.N*sizeof(real));
        ax = a.x; bx = b.x;

        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
            __assume_aligned(ax, ALIGN);
            __assume_aligned(bx, ALIGN);
        #endif
        #pragma omp simd aligned(out, ax, bx: ALIGN)
        for(unsigned int i=0; i<a.N; i++)
            out[i] = ax[i] * bx[i];

        lpout = LPV<real>(out, a.m, a.n, a.opts, true);
    }

    return lpout;
} 

template <typename real, typename T>
LPV<real> operator*(const LPV<real> &a, const T &b){
    check_all(a, b);

    LPV<real> lpout;

    real bc = (real) b;
    chop(bc,a.opts);

    if(a.N == 1){
        real out_val = a.x[0] * bc;
        lpout = LPV<real>(&out_val, a.m, a.n, a.opts, false);
    }else{

        real * ax __attribute__((aligned(ALIGN)));
        real * out __attribute__((aligned(ALIGN)));
        out = (real*) allocate( a.N*sizeof(real));
        ax = a.x;

        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
            __assume_aligned(ax, ALIGN);
        #endif
        #pragma omp simd aligned(out, ax: ALIGN)
        for(unsigned int i=0; i<a.N; i++)
            out[i] = ax[i] * bc;

         lpout = LPV<real>(out, a.m, a.n, a.opts, true);
    }

    return lpout;
} 

template <typename real, typename T>
LPV<real> operator*(const T &a, const LPV<real> &b){
    return b * a;
} 

template <typename real>
LPV<real> operator/(const LPV<real> &a, const LPV<real> &b){
    check_all(a, b);

    LPV<real> lpout;

    if(a.N == 1){
        real out_val = a.x[0] / b.x[0];
        lpout = LPV<real>(&out_val, a.m, a.n, a.opts, false);
    }else{
        real * ax __attribute__((aligned(ALIGN)));
        real * bx __attribute__((aligned(ALIGN)));
        real * out __attribute__((aligned(ALIGN)));
        out = (real*) allocate(a.N*sizeof(real));
        ax = a.x; bx = b.x;

        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
            __assume_aligned(ax, ALIGN);
            __assume_aligned(bx, ALIGN);
        #endif
        #pragma omp simd aligned(out, ax, bx: ALIGN)
        for(unsigned int i=0; i<a.N; i++)
            out[i] = ax[i] / bx[i];

        lpout = LPV<real>(out, a.m, a.n, a.opts, true);
    }

    return lpout;
} 

template <typename real, typename T>
LPV<real> operator/(const LPV<real> &a, const T &b){
    check_all(a, b);

    LPV<real> lpout;

    const int babs = std::abs((int) b);
    const bool needchopping = !(std::is_integral<T>::value && (babs & (babs-1)) == 0);

    real bc = (real) b;
    chop(bc,a.opts);

    if(a.N == 1){
        real out_val = a.x[0] / bc;
        lpout = LPV<real>(&out_val, a.m, a.n, a.opts, false, needchopping);
    }else{

        real * ax __attribute__((aligned(ALIGN)));
        real * out __attribute__((aligned(ALIGN)));
        out = (real*) allocate( a.N*sizeof(real));
        ax = a.x;

        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
            __assume_aligned(ax, ALIGN);
        #endif
        #pragma omp simd aligned(out, ax: ALIGN)
        for(unsigned int i=0; i<a.N; i++)
            out[i] = ax[i] / bc;

        lpout = LPV<real>(out, a.m, a.n, a.opts, true, needchopping);
    }

    return lpout;
} 

template <typename real, typename T>
LPV<real> operator/(const T &a, const LPV<real> &b){
    check_all(a, b);

    LPV<real> lpout;

    const int aabs = std::abs((int) a);
    const bool needchopping = !(std::is_integral<T>::value && (aabs & (aabs-1)) == 0);

    real ac = (real) a;
    chop(ac,b.opts);

    if(b.N == 1){
        real out_val = ac / b.x[0];
        lpout = LPV<real>(&out_val, b.m, b.n, b.opts, false, needchopping);
    }else{

        real * bx __attribute__((aligned(ALIGN)));
        real * out __attribute__((aligned(ALIGN)));
        out = (real*) allocate( b.N*sizeof(real));
        bx = b.x;

        #ifdef __INTEL_COMPILER
            __assume_aligned(out, ALIGN);
            __assume_aligned(bx, ALIGN);
        #endif
        #pragma omp simd aligned(out, bx: ALIGN)
        for(unsigned int i=0; i<b.N; i++)
            out[i] = ac / bx[i];

        lpout = LPV<real>(out, b.m, b.n, b.opts, true, needchopping);
    }

    return lpout;
} 

template<typename real>
real value(const LPV<real> &a){
    return a.x[0];
}

template <typename real>
LPV<real> mtimes(const LPV<real> &a, const LPV<real> &b){
    check_opts(a, b);

    if(a.N == 0 || b.N == 0)
        throw std::invalid_argument("Cannot multiply empty matrices.");

    if(a.n != b.m)
        throw std::invalid_argument("Shape mismatch: the sizes of the matrices being multiplied are not compatible.");

    LPV<real> lpout;
    real temp;

    const unsigned int M = a.m*b.n;
    const unsigned int m = a.m;
    const unsigned int n = b.n;

    if(a.N == 1 || b.N == 1){
        return a*b;
    }else{
        real * out __attribute__((aligned(ALIGN)));
        out = (real*) allocate( M*sizeof(real));
        myfill(out, M, 0.0);

        for(unsigned int i=0; i<m; i++){
            for(unsigned int j=0; j<n; j++){
                for(unsigned int k=0; k<a.n; k++){
                    temp = a.x[k + a.n*i] * b.x[j + n*k];
                    chop(temp, a.opts);
                    temp += out[j + n*i];
                    chop(temp, a.opts);
                    out[j + n*i] = temp;
                }
            }
        }

        // no need to rechop anything here
        lpout = LPV<real>(out, m, n, a.opts, true, false);
    }

    return lpout;
} 

template <typename real>
LPV<real> transpose(const LPV<real> &a){
    LPV<real> lpout;

    if(a.N == 0)
        throw std::invalid_argument("Cannot transpose empty matrix.");

    if(a.N == 1){
        lpout = LPV<real>(a);
    }else{
        const unsigned int n = a.n;
        const unsigned int m = a.m;
        real * out __attribute__((aligned(ALIGN)));
        out = (real*) allocate( a.N*sizeof(real));

        for(unsigned int i=0; i<m; i++){
            for(unsigned int j=0; j<n; j++){
                out[m*j + i] = a.x[n*i + j];
            }
        }

        lpout = LPV<real>(out, n, m, a.opts, true, false);
    }
    return lpout;
}

template <typename real>
LPV<real> matvec_tridiag(const LPV<real> &ld, const LPV<real> &d, const LPV<real> &ud, const LPV<real> &b){
    check_opts(ld,d); check_opts(d,ud); check_opts(ud,b);
    if(d.N < 2 || d.N != (ld.N+1) || ld.N != ud.N || d.N != b.N || b.m != b.N)
        throw std::invalid_argument("matvec_tridiag: dimension mismatch or scalar input.");
    
    LPV<real> lpout;
    real a,s,c;

    const unsigned int N = d.N;

    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate( N*sizeof(real));

    s = d.x[0]*b.x[0];      chop(s,d.opts);
    c = ud.x[0]*b.x[1];     chop(c, d.opts);
    out[0] = s + c;         chop(out[0], d.opts);
    s = d.x[N-1]*b.x[N-1];  chop(s,d.opts);
    a = ld.x[N-2]*b.x[N-2]; chop(a, d.opts);
    out[N-1] = a + s;       chop(out[N-1], d.opts);
    for(unsigned int i=1; i<N-1; i++){
        a = ld.x[i-1]*b.x[i-1]; chop(a, d.opts);
        s = d.x[i]*b.x[i];      chop(s, d.opts);
        c = ud.x[i]*b.x[i+1];   chop(c, d.opts);
        out[i] = a + c;         chop(out[i], d.opts);
        out[i] += s;            chop(out[i], d.opts);
    }

    // no need to rechop anything here
    lpout = LPV<real>(out, N, 1, d.opts, true, false);

    return lpout;
}

template <typename real>
LPV<real> FEMdiff_csr(const real *data, const int * indices, const int * indptr, const LPV<real> &b){
    // nnz_diags is the number of non-zero diagonals in the upper triangular part of the matrix (excluding the main diagonal)
    // idx is a vector of length nnz_diags that contains the indices of the non-zero diagonals (excluding the main diagonal)
    // row is an LPV vector of length nnz_diags+1 such that row[0] is the value on the main diagonal and row[j+1] is the value on the idx[j] diagonal
    if(b.N < 2 || b.m != b.N)
        throw std::invalid_argument("FEMdiff_csr: dimension mismatch or scalar input.");
    
    LPV<real> lpout;
    real a,c;

    const int N = (int) b.N;
    const Option * op = b.opts;

    real * bx __attribute__((aligned(ALIGN)));
    bx = b.x;

    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate( N*sizeof(real));

    #pragma omp parallel for default(none) private(a,c) shared(out,indptr,indices,data,bx,op) firstprivate(N)
    for(int i = 0; i < N; i++){
        real outi = 0.0;
        for(int k = indptr[i]; k < indptr[i+1]; k++){
            //out[i] = out[i] + data[k]*b.x[indices[k]];
            int j = indices[k];
            if (j == i)
                continue;
            a = data[k];          //chop(a,op);
            c = bx[j] - bx[i];    chop(c,op);
            a = a*c;              chop(a,op);
            outi += a;            chop(outi, op);
        }
        out[i] = outi;
    }

    // no need to rechop anything here
    lpout = LPV<real>(out, N, 1, b.opts, true, false);

    return lpout;
}

template <typename real>
LPV<real> FEMdiff(const real * K, const LPV<real> &b, const int n_cells, const int edim){
    if(b.N < 2 || b.N != n_cells*edim)
        throw std::invalid_argument("FEMdiff: scalar input or dimension mismatch.");

    LPV<real> lpout;
    real a1,c1,a2,c2;

    const Option * op = b.opts;
    const unsigned int N = b.N;
    const int edimsq = edim*edim;

    real * bx __attribute__((aligned(ALIGN)));
    bx = b.x;

    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate( N*sizeof(real));
    myfill(out, N, 0.0);

    #pragma omp parallel for default(none) shared(bx, out, op, K) private(a1,c1,a2,c2) firstprivate(n_cells, edim, edimsq)
    for(int k=0; k<n_cells; k++){
        for(int i=0; i<edim; i++){
            for(int j=0; j<i; j++){
                //out[k*edim + i] += K[k*edimsq + edim*i + j]*(b[k*edim + j] - b[k*edim + i]);
                a1 = (bx[k*edim + j] - bx[k*edim + i]); chop(a1,op);
                c1 = K[k*edimsq + edim*i + j];          //chop(c1,op);
                a1 = a1*c1;                             //chop(a1,op);
                out[k*edim + i] += a1;                  chop(out[k*edim+i], op);
            }
            for(int j=i+1; j<edim; j++){
                //out[k*edim + i] += K[k*edimsq + edim*i + j]*(b[k*edim + j] - b[k*edim + i]);
                a2 = (bx[k*edim + j] - bx[k*edim + i]); chop(a2,op);
                c2 = K[k*edimsq + edim*i + j];          //chop(c2,op);
                a2 = a2*c2;                             //chop(a2,op);
                out[k*edim + i] += a2;                  chop(out[k*edim+i], op);
            }
        }
    }

    // no need to rechop anything here
    lpout = LPV<real>(out, N, 1, b.opts, true, false);

    return lpout;
}

template <typename real>
LPV<real> diffND(const LPV<real> &u, const int dim){
    if(dim == 1)
        return diff1D(u, 0.);
    else if(dim == 2)
        return diff2D(u, 0.);
    else if(dim == 3)
        return diff3D(u, 0.);
    else
        throw std::invalid_argument("diffND: only implemented for d = 1,2,3.");
}

template <typename real>
LPV<real> diffND(const LPV<real> &u, const int dim, const double bc){
    if(dim == 1)
        return diff1D(u, bc);
    else if(dim == 2)
        return diff2D(u, bc);
    else if(dim == 3)
        return diff3D(u, bc);
    else
        throw std::invalid_argument("diffND: only implemented for d = 1,2,3.");
}

// rounding error safe 2nd order differencing for 1D vectors
template <typename real>
LPV<real> diff1D(const LPV<real> &u, const double bc){
    if(u.N < 2 || u.m != u.N)
        throw std::invalid_argument("diff1D: scalar or matrix input.");
    
    LPV<real> lpout;
    real a,b,c;

    const Option * op = u.opts;

    const unsigned int N = u.N;

    real * ux __attribute__((aligned(ALIGN)));
    ux = u.x;

    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate( N*sizeof(real));

    a = u.x[1]-u.x[0]; chop(a, u.opts);
    b = u.x[0]-bc;     chop(b, u.opts);
    c = a - b;         chop(c, u.opts);
    out[0] = c;

    a = bc - u.x[N-1];       chop(a, u.opts);
    b = u.x[N-1] - u.x[N-2]; chop(b, u.opts);
    c = a - b;               chop(c, u.opts);
    out[N-1] = c;

    #pragma omp parallel for default(none) shared(ux, out, op) private(a,b,c) firstprivate(N)
    for(int i=1; i<N-1; i++){
        a = ux[i+1] - ux[i];  chop(a, op);
        b = ux[i] - ux[i-1];  chop(b, op);
        c = a - b;            chop(c, op);
        out[i] = c;
    }

    // no need to rechop anything here
    lpout = LPV<real>(out, N, 1, u.opts, true, false);

    return lpout;
}

// rounding error-safe 2nd order differencing for 2D vectors
template <typename real>
LPV<real> diff2D(const LPV<real> &u, const double bc){
    if(u.N < 2 || u.m != u.N)
        throw std::invalid_argument("diff2D: scalar or matrix input.");
    
    LPV<real> lpout;
    real a,b,c,d,dx,dy;

    const Option * op = u.opts;

    const unsigned int N = u.N;
    const unsigned int K = (int) std::round(std::sqrt(N));

    real * ux __attribute__((aligned(ALIGN)));
    ux = u.x;

    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate( N*sizeof(real));

    // u[i,j] = u[j*K+i]
    
    //i=0,   j=0    ((u[1] - u[0]) - (u[0] - u[-1])) + ((u[K] - u[0]) - (u[0] - u[-K]))
    a  = (ux[1] - ux[0]);   chop(a, op);
    b  = (ux[0] - bc);      chop(b, op);
    c  = (ux[K] - ux[0]);   chop(c, op);
    d  = (ux[0] - bc);      chop(d, op);
    dx = a - b;             chop(dx, op);
    dy = c - d;             chop(dy, op);
    out[0] = dx + dy;       chop(out[0], op);
    
    //i=K-1, j=0    ((-u[K-1]) - (u[K-1] - u[K-2])) + ((u[K+K-1] - u[K-1]) - (u[K-1]))
    a  = (bc - ux[K-1]);          chop(a, op);
    b  = (ux[K-1] - ux[K-2]);     chop(b, op);
    c  = (ux[K+K-1] - ux[K-1]);   chop(c, op);
    d  = (ux[K-1] - bc);          chop(d, op);
    dx = a - b;                   chop(dx, op);
    dy = c - d;                   chop(dy, op);
    out[K-1] = dx + dy;           chop(out[K-1], op);
    
    //i=0,   j=K-1  ((u[(K-1)*K+1] - u[(K-1)*K]) - (u[(K-1)*K])) + ((-u[(K-1)*K]) - (u[(K-1)*K] - u[(K-2)*K]))
    a  = (ux[(K-1)*K+1]-ux[(K-1)*K]);       chop(a, op);
    b  = (ux[(K-1)*K]-bc);                  chop(b, op);
    c  = (bc-ux[(K-1)*K]);                  chop(c, op);
    d  = (ux[(K-1)*K] - ux[(K-2)*K]);       chop(d, op);
    dx = a - b;                             chop(dx, op);
    dy = c - d;                             chop(dy, op);
    out[(K-1)*K] = dx + dy;                 chop(out[(K-1)*K], op);
    
    //i=K-1, j=K-1 ((- u[(K-1)*K+K-1]) - (u[(K-1)*K+K-1] - u[(K-1)*K+K-2])) + ((-u[(K-1)*K+K-1]) - (u[(K-1)*K+K-1] - u[(K-2)*K+K-1]))
    a  = (bc-ux[(K-1)*K+K-1]);                  chop(a, op);
    b  = (ux[(K-1)*K+K-1]-ux[(K-1)*K+K-2]);     chop(b, op);
    c  = (bc-ux[(K-1)*K+K-1]);                  chop(c, op);
    d  = (ux[(K-1)*K+K-1] - ux[(K-2)*K+K-1]);   chop(d, op);
    dx = a - b;                                 chop(dx, op);
    dy = c - d;                                 chop(dy, op);
    out[(K-1)*K+K-1] = dx + dy;                 chop(out[(K-1)*K+K-1], op);
    
    #pragma omp parallel default(none) shared(out,ux,op) private(a,b,c,d,dx,dy) firstprivate(K,bc)
    {
    #pragma omp for
    for(int i=1; i<K-1; i++){
        //j=K-1 ((u[(K-1)*K+i+1] - u[(K-1)*K+i]) - (u[(K-1)*K+i] - u[(K-1)*K+i-1])) + ((-u[(K-1)*K+i]) - (u[(K-1)*K+i] - u[(K-2)*K+i]))
        a  = (ux[(K-1)*K+i+1]-ux[(K-1)*K+i]);   chop(a, op);
        b  = (ux[(K-1)*K+i]-ux[(K-1)*K+i-1]);   chop(b, op);
        c  = (bc-ux[(K-1)*K+i]);                chop(c, op);
        d  = (ux[(K-1)*K+i] - ux[(K-2)*K+i]);   chop(d, op);
        dx = a - b;                             chop(dx, op);
        dy = c - d;                             chop(dy, op);
        out[(K-1)*K+i] = dx + dy;               chop(out[(K-1)*K+i], op);
        //j=0   ((u[i+1] - u[i]) - (u[i] - u[i-1])) + ((u[K+i] - u[i]) - (u[i]))
        a  = (ux[i+1] - ux[i]);   chop(a, op);
        b  = (ux[i] - ux[i-1]);   chop(b, op);
        c  = (ux[K+i] - ux[i]);   chop(c, op);
        d  = (ux[i] - bc);        chop(d, op);
        dx = a - b;               chop(dx, op);
        dy = c - d;               chop(dy, op);
        out[i] = dx + dy;         chop(out[i], op);
    }
    #pragma omp for
    for(int j=1; j<K-1; j++){
        //i=K-1 ((-u[j*K+K-1]) - (u[j*K+K-1] - u[j*K+K-2])) + ((u[(j+1)*K+K-1] - u[j*K+K-1]) - (u[j*K+K-1] - u[(j-1)*K+K-1]))
        a  = (bc - ux[j*K+K-1]);                chop(a, op);
        b  = (ux[j*K+K-1] - ux[j*K+K-2]);       chop(b, op);
        c  = (ux[(j+1)*K+K-1] - ux[j*K+K-1]);   chop(c, op);
        d  = (ux[j*K+K-1] - ux[(j-1)*K+K-1]);   chop(d, op);
        dx = a - b;                             chop(dx, op);
        dy = c - d;                             chop(dy, op);
        out[j*K+K-1] = dx + dy;                 chop(out[j*K+K-1], op);
        //i=0   ((u[j*K+1] - u[j*K]) - (u[j*K])) + ((u[(j+1)*K] - u[j*K]) - (u[j*K] - u[(j-1)*K]))
        a  = (ux[j*K+1] - ux[j*K]);     chop(a, op);
        b  = (ux[j*K] - bc);            chop(b, op);
        c  = (ux[(j+1)*K] - ux[j*K]);   chop(c, op);
        d  = (ux[j*K]-ux[(j-1)*K]);     chop(d, op);
        dx = a - b;                     chop(dx, op);
        dy = c - d;                     chop(dy, op);
        out[j*K] = dx + dy;             chop(out[j*K], op);
    }


    #pragma omp for
    for(int j=1; j<K-1; j++){
        for(int i=1; i<K-1; i++){
            //((u[j*K+i+1] - u[j*K+i]) - (u[j*K+i] - u[j*K+i-1])) + ((u[(j+1)*K+i] - u[j*K+i]) - (u[j*K+i] - u[(j-1)*K+i]))
            a  = (ux[j*K+i+1] - ux[j*K+i]);     chop(a, op);
            b  = (ux[j*K+i] - ux[j*K+i-1]);     chop(b, op);
            c  = (ux[(j+1)*K+i] - ux[j*K+i]);   chop(c, op);
            d  = (ux[j*K+i] - ux[(j-1)*K+i]);   chop(d, op);
            dx = a - b;                         chop(dx, op);
            dy = c - d;                         chop(dy, op);
            out[j*K+i] = dx + dy;               chop(out[j*K+i], op);
        }
    }
    }

    // no need to rechop anything here
    lpout = LPV<real>(out, N, 1, u.opts, true, false);

    return lpout;
}

// rounding error-safe 2nd order differencing for 3D vectors
template <typename real>
LPV<real> diff3D(const LPV<real> &u, const double bc){
    if(u.N < 2 || u.m != u.N)
        throw std::invalid_argument("diff3D: scalar or matrix input.");
    
    LPV<real> lpout;
    real a,b,c,d,e,f,dx,dy,dz;

    const unsigned int N = u.N;
    const unsigned int K = (int) std::round(std::cbrt(N));
    const unsigned int J = K+2;
    const unsigned int M = std::pow(J,3);

    const Option * op = u.opts;

    real * ux __attribute__((aligned(ALIGN)));
    ux = u.x;
    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate(N*sizeof(real));
    
    real * temp __attribute__((aligned(ALIGN)));
    temp = (real*) allocate(M*sizeof(real));
    myfill(temp, M, bc);

    // u[i,j,k] = u[(k*K+j)*K+i]
    #ifdef __INTEL_COMPILER
        __assume_aligned(temp, ALIGN);
        __assume_aligned(ux, ALIGN);
    #endif
    #pragma omp parallel shared(temp,ux,out,op) private(a,b,c,d,e,f,dx,dy,dz)
    {
    #pragma omp for collapse(2)
    for(int k=1; k<J-1; k++){
        for(int j=1; j<J-1; j++){
            #pragma omp simd aligned(temp, ux: ALIGN)
            for(int i=1; i<J-1; i++){
                temp[(k*J+j)*J+i] = ux[((k-1)*K+j-1)*K + i-1];
            }
        }
    }

    #pragma omp barrier 

    #pragma omp for collapse(2)
    for(int k=1; k<J-1; k++){
        for(int j=1; j<J-1; j++){
            for(int i=1; i<J-1; i++){
                //((u[(k*K+j)*K+i+1] - u[(k*K+j)*K+i]) - (u[(k*K+j)*K+i] - u[(k*K+j)*K+i-1])) + ((u[(k*K+j+1)*K+i] - u[(k*K+j)*K+i]) - (u[(k*K+j)*K+i] - u[(k*K+j-1)*K+i])) + ((u[((k+1)*K+j)*K+i] - u[(k*K+j)*K+i]) - (u[(k*K+j)*K+i] - u[((k-1)*K+j)*K+i]))
                a  = (temp[(k*J+j)*J+i+1] - temp[(k*J+j)*J+i]);     chop(a,  op);
                b  = (temp[(k*J+j)*J+i] - temp[(k*J+j)*J+i-1]);     chop(b,  op);
                c  = (temp[(k*J+j+1)*J+i] - temp[(k*J+j)*J+i]);     chop(c,  op);
                d  = (temp[(k*J+j)*J+i] - temp[(k*J+j-1)*J+i]);     chop(d,  op);
                e  = (temp[((k+1)*J+j)*J+i] - temp[(k*J+j)*J+i]);   chop(e,  op);
                f  = (temp[(k*J+j)*J+i] - temp[((k-1)*J+j)*J+i]);   chop(f,  op);
                dx = a - b;                                         chop(dx, op);
                dy = c - d;                                         chop(dy, op);
                dz = e - f;                                         chop(dz, op);
                out[((k-1)*K+j-1)*K+i-1] = dx + dy;                 chop(out[((k-1)*K+j-1)*K+i-1], op);
                out[((k-1)*K+j-1)*K+i-1] += dz;                     chop(out[((k-1)*K+j-1)*K+i-1], op);
            }
        }
    }
    }

    mkl_free(temp);

    // no need to rechop anything here
    lpout = LPV<real>(out, N, 1, u.opts, true, false);

    return lpout;
}

template <typename real>
LPV<real> solve_tridiag(const LPV<real> &a, const LPV<real> &b, const LPV<real> &c, const LPV<real> &d){
    check_opts(a,b); check_opts(b,c); check_opts(c,b);
    if(b.N < 2 || b.N != (a.N+1) || a.N != c.N || b.N != b.N || b.m != b.N)
        throw std::invalid_argument("solve_tridiag: dimension mismatch or scalar input.");
    
    LPV<real> lpout;
    real t,s;

    const unsigned int N = b.N;

    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate( N*sizeof(real));
    real * temp __attribute__((aligned(ALIGN)));
    temp = (real*) allocate( (N-1)*sizeof(real));

    temp[0] = c.x[0]/b.x[0]; chop(temp[0], b.opts);
    out[0]  = d.x[0]/b.x[0]; chop(out[0], b.opts);
    for(int i=1; i<N-1; i++){
        t = -a.x[i-1]*temp[i-1];    chop(t, b.opts);
        t += b.x[i];                chop(t, b.opts);
        temp[i] = c.x[i]/t;         chop(temp[i], b.opts);
        //temp[i] = c.x[i] / (b.x[i] - a.x[i-1]*temp[i-1]);

        t = -a.x[i-1]*out[i-1]; chop(t, b.opts);
        t += d.x[i]; chop(t, b.opts);
        s = -a.x[i-1]*temp[i-1]; chop(s, b.opts);
        s += b.x[i]; chop(s, b.opts);
        out[i] = t/s;   chop(out[i], b.opts);
        //out[i] = (d.x[i] - a.x[i-1]*out[i-1]) / (b.x[i] - a.x[i-1]*temp[i-1]);
    }
    t = -a.x[N-2]*out[N-2];   chop(t, b.opts);
    t += d.x[N-1];            chop(t, b.opts);
    s = -a.x[N-2]*temp[N-2];  chop(s, b.opts);
    s += b.x[N-1];            chop(s, b.opts);
    out[N-1] = t/s;           chop(out[N-1], b.opts);
    //out[N-1] = (d.x[N-1] - a.x[N-2]*out[N-2]) / (b.x[N-1] - a.x[N-2]*temp[N-2]);

    for(int i=N-2; i>-1; i--){
        t = -temp[i]*out[i+1]; chop(t, b.opts);
        out[i] += t; chop(out[i], b.opts);
        //out[i] = out[i] - temp[i]*out[i+1];
    }

    // no need to rechop anything here
    lpout = LPV<real>(out, N, 1, b.opts, true, false);

    mkl_free(temp);

    return lpout;
}

template <typename real>
LPV<real> matvec_symm_toeplitz(const real * row, const int * idx, const int nnz_diags, const LPV<real> &b){
    // nnz_diags is the number of non-zero diagonals in the upper triangular part of the matrix (excluding the main diagonal)
    // idx is a vector of length nnz_diags that contains the indices of the non-zero diagonals (excluding the main diagonal)
    // row is a vector of length nnz_diags+1 such that row[0] is the value on the main diagonal and row[j+1] is the value on the idx[j] diagonal
    if(b.N < 3 || b.m != b.N || nnz_diags >= b.N || nnz_diags < 0)
        throw std::invalid_argument("matvec_symm_toeplitz: dimension mismatch or scalar input. Not implemented for 2-by-2 matrices.");
    
    LPV<real> lpout;
    real a,c;
    int lb,ub;

    const int N = (int) b.N;

    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate( N*sizeof(real));

    for(int i = 0; i < N; i++){
        a = row[0]; chop(a, b.opts);
        out[i] = a*b.x[i]; chop(out[i], b.opts);
        for(int j = 0; j < nnz_diags; j++){
            // these make sure we do not go below 0 or above N-1.
            // the second terms in the multiplication are actually just logicals
            // alternatively:
            // a = (i + idx[j] < N ? b.x[i + idx[j]] : 0) + (i - idx[j] >= 0 ? b.x[i-idx[j]] : 0); chop(a, b.opts);
            a = (b.x[(i+idx[j])%N]*(1 - (i + idx[j])/N) + b.x[(N + i-idx[j])%N]*((N + i - idx[j])/N)); chop(a, b.opts);
            c = row[j+1];        chop(c, b.opts);
            a = a*c;             chop(a, b.opts);
            out[i] += a;         chop(out[i], b.opts);
            // out[i] = out[i] + row[j]*(b[i+idx[j]] + b[i-idx[j]]);
        }
    }

    // no need to rechop anything here
    lpout = LPV<real>(out, N, 1, b.opts, true, false);

    return lpout;
}

template <typename real>
LPV<real> matvec_symm_toeplitz(const LPV<real> &row, const int * idx, const int nnz_diags, const LPV<real> &b){
    // nnz_diags is the number of non-zero diagonals in the upper triangular part of the matrix (excluding the main diagonal)
    // idx is a vector of length nnz_diags that contains the indices of the non-zero diagonals (excluding the main diagonal)
    // row is an LPV vector of length nnz_diags+1 such that row[0] is the value on the main diagonal and row[j+1] is the value on the idx[j] diagonal
    check_opts(row, b);
    if(b.N < 3 || b.m != b.N || nnz_diags >= b.N || nnz_diags < 0)
        throw std::invalid_argument("matvec_symm_toeplitz: dimension mismatch or scalar input. Not implemented for 2-by-2 matrices.");
    
    LPV<real> lpout;
    real a,c;
    int lb,ub;

    const int N = (int) b.N;

    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate( N*sizeof(real));

    for(int i = 0; i < N; i++){
        out[i] = row.x[0]*b.x[i]; chop(out[i], b.opts);
        for(int j = 0; j < nnz_diags; j++){
            // these make sure we do not go below 0 or above N-1.
            // the second terms in the multiplication are actually just logicals
            // alternatively:
            // a = (i + idx[j] < N ? b.x[i + idx[j]] : 0) + (i - idx[j] >= 0 ? b.x[i-idx[j]] : 0); chop(a, b.opts);
            a = (b.x[(i+idx[j])%N]*(1 - (i + idx[j])/N) + b.x[(N + i-idx[j])%N]*((N + i - idx[j])/N)); chop(a, b.opts);
            c = row.x[j+1];        chop(c, b.opts);
            a = a*c;               chop(a, b.opts);
            out[i] += a;           chop(out[i], b.opts);
            // out[i] = out[i] + row.x[j]*(b.x[i+idx[j]] + b.x[i-idx[j]]);
        }
    }

    // no need to rechop anything here
    lpout = LPV<real>(out, N, 1, b.opts, true, false);

    return lpout;
}

template <typename real>
LPV<real> matvec_csr(const real *data, const int * indices, const int * indptr, const LPV<real> &b){
    // nnz_diags is the number of non-zero diagonals in the upper triangular part of the matrix (excluding the main diagonal)
    // idx is a vector of length nnz_diags that contains the indices of the non-zero diagonals (excluding the main diagonal)
    // row is an LPV vector of length nnz_diags+1 such that row[0] is the value on the main diagonal and row[j+1] is the value on the idx[j] diagonal
    if(b.N < 2 || b.m != b.N)
        throw std::invalid_argument("matvec_csr: dimension mismatch or scalar input.");
    
    LPV<real> lpout;
    real a;
    int j;

    const int N = (int) b.N;
    const Option * op = b.opts;

    real * bx __attribute__((aligned(ALIGN)));
    bx = b.x;

    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate( N*sizeof(real));

    #pragma omp parallel for default(none) private(j,a) shared(out,indptr,indices,data,bx,op) firstprivate(N)
    for(int i = 0; i < N; i++){
        real outi = 0.0;
        for(int k = indptr[i]; k < indptr[i+1]; k++){
            //out[i] = out[i] + data[k]*b.x[indices[k]];
            j = indices[k];
            a = data[k];          //chop(a,op);
            a = a*bx[j];          chop(a,op);
            outi += a;            chop(outi, op);
            //outi += data[k]*bx[j];  chop(outi, op);
        }
        out[i] = outi;
    }

    // no need to rechop anything here
    lpout = LPV<real>(out, N, 1, b.opts, true, false);

    return lpout;
}


template <typename real>
LPV<real> banded_cholesky(const real *a, const int nbands, const int N, const Option * opts){
    // NxN symmetric positive definite linear system. nbands excludes the main diag
    // only uses the lower triangular part of a
    if(N < 2)
        throw std::invalid_argument("banded_cholesky: scalar input not allowed.");
    real x, r, s;
    int i, j, k, lb, ub;

    LPV<real> lpout;
    real * L __attribute__((aligned(ALIGN)));
    L = (real*) allocate(N*N*sizeof(real));
    myfill(L, N*N, 0.0);

    /* Loop over columns */
    for(j = 0; j < N; j++){
        /* i = j */
        x = a[j*N+j];  chop(x, opts); /* A_jj */
        lb = (j-nbands)*((N + j - nbands)/N);
        for(k = lb; k < j; k++){
            s  = L[j*N+k]*L[j*N+k]; chop(s, opts);
            x -= s;                 chop(x, opts); /* L_jk L_jk */
        }

        x = std::sqrt(x); chop(x, opts);
        L[j*N+j] = x;  /* L_jj */
        // NOTE: the following line must be recomputed each time
        //r = 1.0 / x; 
        /* i != j */
        ub = (j + nbands)*(1 - (j + nbands)/N) + (N-1)*((j + nbands)/N);
        for(i = j+1; i <= ub; i++) {
            x = a[i*N+j];  chop(x, opts); /* A_ij */
            lb = (i-nbands)*((N + i - nbands)/N);
            for(k = lb; k < j; k++){
                s  = L[i*N+k]*L[j*N+k]; chop(s, opts);
                x -= s;                 chop(x, opts); /* L_ik L_jk */
            }
            r = 1.0 / L[j*N + j];       chop(r, opts);
            L[i*N+j] = x * r;           chop(L[i*N+j], opts); /* L_ij = x / L_jj */
        }
    }

    lpout = LPV<real>(L, N*N, 1, opts, true, false);

    return lpout;
}

template <typename real>
LPV<real> banded_choleskyLPV(const LPV<real> a, const int nbands){
    if(a.m != a.n  || nbands < 0 || nbands > a.m)
        throw std::invalid_argument("banded_cholesky: dimension mismatch.");
    return banded_cholesky(&a.x[0], nbands, a.m, a.opts);
}

template <typename real>
LPV<real> solve_symmetric(const real *a, const int nbands, const LPV<real> &b) {
    if(nbands < 0 || nbands > b.N)
        throw std::invalid_argument("solve_symmetric: dimension mismatch.");
    LPV<real> L = banded_cholesky(a, nbands, b.N, b.opts);
    return solve_symmetricLPV(L, nbands, b);
}

template <typename real>
LPV<real> solve_symmetricLPV(const LPV<real> &L, const int nbands, const LPV<real> &b) {
    // nbands excludes the main diag
    check_opts(L,b);
    if(((L.N != b.N*b.N) && (L.m != b.N || L.m != L.n || b.m != b.N)) || nbands < 0 || nbands > b.N)
        throw std::invalid_argument("solve_symmetric: dimension mismatch.");

    double x, r, s;
    int i, j, k, lb, ub;

    const unsigned int N = b.N;

    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate(N*sizeof(real));

    // Lower Triangular
    for (i = 0; i < N; i++)
        out[i] = b.x[i];

    for (j = 0; j < N; j++){
        out[j] = out[j]/L.x[j*N+j]; chop(out[j], b.opts);
        ub = (j + nbands)*(1 - (j + nbands)/N) + (N-1)*((j + nbands)/N);
        for(i = j+1; i <= ub; i++){
            s       = L.x[i*N+j]*out[j]; chop(s, b.opts);
            out[i] -= s;                 chop(out[i], b.opts);
        }
    }

    // upper Triangular
    for(j = N-1; 0 <= j; j--){
        out[j] = out[j]/L.x[j*N+j]; chop(out[j], b.opts);
        lb = (j-nbands)*((N + j - nbands)/N);
        for(i = lb; i < j; i++ ){
            s       = L.x[j*N+i]*out[j]; chop(s, b.opts);
            out[i] -= s;                 chop(out[i], b.opts);
        }
    }

    // no need to rechop anything here
    LPV<real> lpout = LPV<real>(out, N, 1, b.opts, true, false);

    return lpout;
}
