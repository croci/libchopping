// #include<LPV.hpp>
//#include "../chopping.hpp"
//

#if !defined(I2)
#define I2(i,j,J) ((j)*J+i)
#endif
#if !defined(I3)
#define I3(i,j,k,J) (((k)*J+j)*J+i)
#endif

void print_vec(const int N, double * v){
    for(int i = 0; i< N; i++)
        std::cout << v[i] << " ";

    std::cout << "\n";

}

double * add_zero_boundary(const int dim, const int J, double * x){
    const int N = std::pow(J, dim);
    const int Jb = J+2;
    const int Nb = std::pow(Jb, dim);

    double * out __attribute__((aligned(ALIGN)));
    out = (double*) allocate(Nb*sizeof(double));

    std::fill(out, out + Nb, 0.0);

    if (dim == 1){
        for(int i=1; i<Jb-1; i++){
            out[i] = x[i-1];
        }

    }else if (dim == 2){
        for(int i=1; i<Jb-1; i++){
            for(int j=1; j<Jb-1; j++){
                out[I2(i,j,Jb)] = x[I2(i-1,j-1,J)];
            }
        }

    } else {
        for(int i=1; i<Jb-1; i++){
            for(int j=1; j<Jb-1; j++){
                for(int k=1; k<Jb-1; k++){
                    out[I3(i,j,k,Jb)] = x[I3(i-1,j-1,k-1,J)];
                }
            }
        }

    }

    return out;
}

double * remove_zero_boundary(const int dim, const int Jb, double * x){
    const int Nb = std::pow(Jb, dim);
    const int J = Jb-2;
    const int N = std::pow(J, dim);

    double * out __attribute__((aligned(ALIGN)));
    out = (double*) allocate(N*sizeof(double));

    if (dim == 1){
        for(int i=1; i<Jb-1; i++){
            out[i-1] = x[i];
        }

    }else if (dim == 2){
        for(int i=1; i<Jb-1; i++){
            for(int j=1; j<Jb-1; j++){
                out[I2(i-1,j-1,J)] = x[I2(i,j,Jb)];
            }
        }

    } else {
        for(int i=1; i<Jb-1; i++){
            for(int j=1; j<Jb-1; j++){
                for(int k=1; k<Jb-1; k++){
                    out[I3(i-1,j-1,k-1,J)] = x[I3(i,j,k,Jb)];
                }
            }
        }

    }

    return out;
}

//////////////////////////////////// Jacobi /////////////////////////////////////////////////////////////////////

void jacobi1D(const double w, const double lambda, const int n_it, const int J, const double * rhs, double * x){
    // assumes BC are incorporated in the vector so that we are safe; vector is J \times 1, with J = K+2
    const double wd = w/(2*1*lambda + 1);
    const double omw = 1-w;
    const int N = J;

    double * temp __attribute__((aligned(ALIGN)));
    temp = (double*) allocate(N*sizeof(double));

    for(int i=0; i<n_it; i++){
        for(int j=1; j<J-1; j++)
            temp[j] = omw*x[j] + wd*(rhs[j] + lambda*(x[j+1] + x[j-1])); 

        for(int j=1; j<J-1; j++)
            x[j] = temp[j];
        
    }
    mkl_free(temp);
}

void jacobi2D(const double w, const double lambda, const int n_it, const int J, const double * rhs, double * x){
    // assumes BC are incorporated in the vector so that we are safe; vector is J \times J, with J = K+2
    const double wd = w/(2*2*lambda + 1);
    const double omw = 1-w;
    const int N = J*J;

    double * temp __attribute__((aligned(ALIGN)));
    temp = (double*) allocate(N*sizeof(double));

    for(int it=0; it<n_it; it++){

        for(int i=1; i<J-1; i++){
            for(int j=1; j<J-1; j++){
                temp[j*J+i] = omw*x[j*J+i] + wd*(rhs[j*J+i] + lambda*(x[(j+1)*J+i] + x[(j-1)*J+i] + x[j*J+i+1] + x[j*J+i-1])); 
            }
        }

        for(int i=1; i<J-1; i++){
            for(int j=1; j<J-1; j++){
                x[j*J+i] = temp[j*J+i];
            }
        }
        
    }
    mkl_free(temp);
}

void jacobi3D(const double w, const double lambda, const int n_it, const int J, const double * rhs, double * x){
    // assumes BC are incorporated in the vector so that we are safe; vector is J \times J, with J = K+2
    const double wd = w/(2*3*lambda + 1);
    const double omw = 1-w;
    const int N = J*J*J;

    double * temp __attribute__((aligned(ALIGN)));
    temp = (double*) allocate(N*sizeof(double));

    for(int it=0; it<n_it; it++){

        for(int i=1; i<J-1; i++){
            for(int j=1; j<J-1; j++){
                for(int k=1; k<J-1; k++){
                    temp[(J*k+j)*J+i] = omw*x[(J*k+j)*J+i] + wd*(rhs[(J*k+j)*J+i] + lambda*(x[(J*k+j)*J+i+1] + x[(J*k+j)*J+i-1] + x[(J*k+j+1)*J+i] + x[(J*k+j-1)*J+i] + x[(J*(k+1)+j)*J+i] + x[(J*(k-1)+j)*J+i])); 
                }
            }
        }

        for(int i=1; i<J-1; i++){
            for(int j=1; j<J-1; j++){
                for(int k=1; k<J-1; k++){
                    x[(J*k+j)*J+i] = temp[(J*k+j)*J+i];
                }
            }
        }
        
    }

    mkl_free(temp);
}

//////////////////////////////////// SSOR //////////////////////////////////////////////////////////////////////

void ssor1D(const double w, const double lambda, const int n_it, const int J, const double * rhs, double * x){
    // assumes BC are incorporated in the vector so that we are safe; vector is J \times 1, with J = K+2
    const double wd = w/(2*1*lambda + 1);
    const double omw = 1-w;
    const int N = J;

    for(int i=0; i<n_it; i++){
        for(int j=1; j<J-1; j++)
            x[j] = omw*x[j] + wd*(rhs[j] + lambda*(x[j+1] + x[j-1])); 
    }
}

void ssor2D(const double w, const double lambda, const int n_it, const int J, const double * rhs, double * x){
    // assumes BC are incorporated in the vector so that we are safe; vector is J \times J, with J = K+2
    const double wd = w/(2*2*lambda + 1);
    const double omw = 1-w;
    const int N = J*J;

    for(int it=0; it<n_it; it++){

        for(int i=1; i<J-1; i++){
            for(int j=1; j<J-1; j++){
                x[j*J+i] = omw*x[j*J+i] + wd*(rhs[j*J+i] + lambda*(x[(j+1)*J+i] + x[(j-1)*J+i] + x[j*J+i+1] + x[j*J+i-1])); 
            }
        }

    }
}

void ssor3D(const double w, const double lambda, const int n_it, const int J, const double * rhs, double * x){
    // assumes BC are incorporated in the vector so that we are safe; vector is J \times J, with J = K+2
    const double wd = w/(2*3*lambda + 1);
    const double omw = 1-w;
    const int N = J*J*J;

    for(int it=0; it<n_it; it++){

        for(int i=1; i<J-1; i++){
            for(int j=1; j<J-1; j++){
                for(int k=1; k<J-1; k++){
                    x[(J*k+j)*J+i] = omw*x[(J*k+j)*J+i] + wd*(rhs[(J*k+j)*J+i] + lambda*(x[(J*k+j)*J+i+1] + x[(J*k+j)*J+i-1] + x[(J*k+j+1)*J+i] + x[(J*k+j-1)*J+i] + x[(J*(k+1)+j)*J+i] + x[(J*(k-1)+j)*J+i])); 
                }
            }
        }

    }
}

//////////////////////////////////// 1D ///////////////////////////////////////////////////////////////////////

void residual1D(const double * rhs, const double lambda, const int J, const double * x, double * res){
    const int N = J;

    res[0] = 0.0; res[N-1] = 0.0;
    for(int j=1; j<J-1; j++){
        res[j] = rhs[j] - (1+2*lambda)*x[j] + lambda*(x[j-1] + x[j+1]); // b[j] - (x[j] + lambda*diff1D(x))
    }
    

}

double * restrict1D(const int J, double * r, const bool deallocate){
    const int N = J;
    const int Jc = (J+1)/2;
    const int Nc = Jc;

    double * rc __attribute__((aligned(ALIGN)));
    rc = (double*) allocate(Nc*sizeof(double));

    rc[0] = 0.0; rc[Nc-1] = 0.0;
    for(int j=1; j<Jc-1; j++){
        //rc[j] = 0.5*r[2*j] + 0.25*(r[2*j+1] + r[2*j-1]);
        rc[j] = r[2*j];
    }

    if(deallocate)
        mkl_free(r);

    return rc;

}

double * interpolate1D(const int Jc, double * rc, const bool deallocate){
    const int Jf = 2*Jc-1;
    const int Nf = Jf;
    const int Nc = Jc;
    //const double weight = (4*lambda + 1)/(2*lambda + 1);

    double * rf __attribute__((aligned(ALIGN)));
    rf = (double*) allocate(Nf*sizeof(double));

    // interpolate coarse solution to finer grid using weighted interpolator
    for(int j=0; j<Jc-1; j++){
        rf[2*j] = rc[j];
        rf[2*j+1] = (rc[j] + rc[j+1])/2;
        //rf[2*j+1] = (rc[j] + rc[j+1])*weight;
    } 
    rf[2*(Jc-1)] = rc[Jc-1];

    if(deallocate)
        mkl_free(rc);

    return rf;

}

double * mgvrhs1D(const int J, const int Jc, double * rhs){
    if(Jc > J)
        throw std::invalid_argument("mgvrhs1D: Jc > J, cannot restrict from coarser to finer.");
    else if(Jc == J){
        // making a copy if on the same level. This is not necessary,
        // but avoids confusion with the pointers
        double * out __attribute__((aligned(ALIGN)));
        out = (double*) allocate(J*sizeof(double));
        for(int i=0; i<J; i++){
            out[i] = rhs[i];
        }

        return out;
    }

    const int nk = (int) std::round(std::log2(J-1));
    const int mk = (int) std::round(std::log2(Jc-1));

    double * out;

    out = restrict1D(J, rhs, false);
    int JJ = (J+1)/2;
    for(int i=nk-2; i>=mk; i--){
        out = restrict1D(JJ, out, true);
        JJ = (JJ+1)/2;
    }

    return out;
}


void mgv1D(const double * rhs, const double lambda, const std::string& smoother, const int nsmooth, const int J, double * x){
    if(J == 3){
        x[1] = rhs[1]/(1+2*lambda);
        return;
    }

    const int N = J;
    const int Jc = (J+1)/2;
    const int Nc = Jc;
    const double w = (2*lambda + 1)/(3*lambda + 1); // 1/(2*dim*lambda/(1+2*dim*lambda)+1);
    const double w_ssor = (4*lambda + 1)/(3*lambda + 1); // 1/(2*dim*lambda/(1+2*dim*lambda)+1);

    double * res __attribute__((aligned(ALIGN)));
    res = (double*) allocate(N*sizeof(double));

    double * resc __attribute__((aligned(ALIGN)));
    resc = (double*) allocate(Nc*sizeof(double));

    double * zerosc __attribute__((aligned(ALIGN)));
    zerosc = (double*) allocate(Nc*sizeof(double));
    std::fill(zerosc, zerosc+Nc, 0.0);

    if (smoother == "jacobi"){
        // jacobi smoothing
        jacobi1D(w, lambda, nsmooth, J, rhs, x);
    }else if (smoother == "ssor" || smoother == "sor"){
        // ssor smoothing
        ssor1D(w_ssor, lambda, nsmooth, J, rhs, x);
    }

    // compute residual b-Ax
    // use diff1D later on for this
    residual1D(rhs, lambda, J, x, res);

    // restrict residual to coarser grid
    resc = restrict1D(J, res, true);

    // apply mgv recusrively for coarser grid
    mgv1D(zerosc, lambda/2, smoother, nsmooth, Jc, resc);

    // interpolate coarse solution to fine grid and add to x
    res = interpolate1D(Jc, resc, true);
    for(int j=1; j<J-1;j++)
        x[j] += res[j];

    if (smoother == "jacobi"){
        // jacobi smoothing
        jacobi1D(w, lambda, nsmooth, J, rhs, x);
    }else if (smoother == "ssor" || smoother == "sor"){
        // ssor smoothing
        ssor1D(w_ssor, lambda, nsmooth, J, rhs, x);
    }

    mkl_free(res);
    mkl_free(zerosc);
}

void fmgv1D(const double * b, const double lambda, const std::string& smoother, const int nsmooth, const int J, double * x){

    const int N = J;
    const int k = (int) std::round(std::log2(N-1));
    double lambdaf = lambda*std::exp2(-(k-1));
    int Jf,Jc;

    double * res __attribute__((aligned(ALIGN)));
    res = (double*) allocate(N*sizeof(double));

    // no need to allocate since mgvrhs1D does it for you
    double * resc __attribute__((aligned(ALIGN)));
    double * resf __attribute__((aligned(ALIGN)));
    //resf = nullptr;

    // compute residual
    residual1D(b, lambda, J, x, res);

    // restrict residual to coarsest grid
    resc = mgvrhs1D(J, 3, res);

    // exact solution on coarsest grid
    resc[0] = 0.0; resc[1] = resc[1]/(1+2*lambdaf); resc[2] = 0.0;

    for(int i=2; i<k+1; i++){
        Jf = std::exp2(i) + 1;
        Jc = std::exp2(i-1)+1;
        lambdaf = lambda*std::exp2(-(k-i));
        
        // interpolate coarse residual to this level
        resc = interpolate1D(Jc, resc, true);

        // get residual on current grid by repeated restriction
        // of original residual to coarser grids
        resf = mgvrhs1D(J, Jf, res);

        // Multigrid V-cycle
        mgv1D(resf, lambdaf, smoother, nsmooth, Jf, resc); 

        // free resf after use
        mkl_free(resf);
    }

    for(int i=1; i<J-1; i++){
        x[i] += resc[i];
    }

    mkl_free(res);
    mkl_free(resc);
}

//////////////////////////////////// 2D ///////////////////////////////////////////////////////////////////////

void residual2D(const double * rhs, const double lambda, const int J, const double * x, double * res){
    const int N = J*J;

    std::fill(res, res + N, 0.0);

    for(int i=1; i<J-1; i++){
        for(int j=1; j<J-1; j++){
            res[I2(i,j,J)] = rhs[I2(i,j,J)] - (1+4*lambda)*x[I2(i,j,J)] + lambda*(x[I2(i+1,j,J)] + x[I2(i-1,j,J)] + x[I2(i,j+1,J)] + x[I2(i,j-1,J)]); // b[j] - (x[j] + lambda*diff2D(x))
        }
    }
    

}

double * restrict2D(const int J, double * r, const bool deallocate){
    const int N = J*J;
    const int Jc = (J+1)/2;
    const int Nc = Jc*Jc;

    double * rc __attribute__((aligned(ALIGN)));
    rc = (double*) allocate(Nc*sizeof(double));

    std::fill(rc, rc + Nc, 0.0);

    for(int i=1; i<Jc-1; i++){
        for(int j=1; j<Jc-1; j++){
            rc[I2(i,j,Jc)] = r[I2(2*i,2*j,J)];
        }
    }

    if(deallocate)
        mkl_free(r);

    return rc;

}

double * interpolate2D(const int Jc, double * rc, const bool deallocate){
    const int Jf = 2*Jc-1;
    const int Nf = Jf*Jf;
    const int Nc = Jc*Jc;

    double * rf __attribute__((aligned(ALIGN)));
    rf = (double*) allocate(Nf*sizeof(double));

    for(int i=0; i<Jc; i++){
        for(int j=0; j<Jc; j++){
            rf[I2(2*i,2*j,Jf)] = rc[I2(i,j,Jc)];
        } 
    }
    for(int i=0; i<Jc-1; i++){
        for(int j=0; j<Jc; j++){
            rf[I2(2*i+1,2*j,Jf)] = (rc[I2(i,j,Jc)] + rc[I2(i+1,j,Jc)])/2;
        } 
    }
    for(int i=0; i<Jc; i++){
        for(int j=0; j<Jc-1; j++){
            rf[I2(2*i,2*j+1,Jf)] = (rc[I2(i,j,Jc)] + rc[I2(i,j+1,Jc)])/2;
        } 
    }
    for(int i=0; i<Jc-1; i++){
        for(int j=0; j<Jc-1; j++){
            rf[I2(2*i+1,2*j+1,Jf)] = (rc[I2(i,j,Jc)] + rc[I2(i+1,j,Jc)] + rc[I2(i,j+1,Jc)] + rc[I2(i+1,j+1,Jc)])/4;
        } 
    }

    if(deallocate)
        mkl_free(rc);

    return rf;

}

double * mgvrhs2D(const int J, const int Jc, double * rhs){

    const int N = J*J;

    if(Jc > J)
        throw std::invalid_argument("mgvrhs2D: Jc > J, cannot restrict from coarser to finer.");
    else if(Jc == J){
        // making a copy if on the same level. This is not necessary,
        // but avoids confusion with the pointers
        double * out __attribute__((aligned(ALIGN)));
        out = (double*) allocate(N*sizeof(double));
        for(int i=0; i<N; i++){
            out[i] = rhs[i];
        }

        return out;
    }

    const int nk = (int) std::round(std::log2(J-1));
    const int mk = (int) std::round(std::log2(Jc-1));

    double * out;

    out = restrict2D(J, rhs, false);
    int JJ = (J+1)/2;
    for(int i=nk-2; i>=mk; i--){
        out = restrict2D(JJ, out, true);
        JJ = (JJ+1)/2;
    }

    return out;
}


void mgv2D(const double * rhs, const double lambda, const std::string& smoother, const int nsmooth, const int J, double * x){
    if(J == 3){
        x[I2(1,1,J)] = rhs[I2(1,1,J)]/(1+4*lambda);
        return;
    }

    const int N = J*J;
    const int Jc = (J+1)/2;
    const int Nc = Jc*Jc;
    const double w = (7*lambda + 1)/(8*lambda + 1);  //(2*dim*lambda + 1)/(4*dim*lambda + 1);
    const double w_ssor = (6*lambda + 1)/(4*lambda + 1);  //(2*dim*lambda + 1)/(4*dim*lambda + 1);

    double * res __attribute__((aligned(ALIGN)));
    res = (double*) allocate(N*sizeof(double));

    double * resc __attribute__((aligned(ALIGN)));
    resc = (double*) allocate(Nc*sizeof(double));

    double * zerosc __attribute__((aligned(ALIGN)));
    zerosc = (double*) allocate(Nc*sizeof(double));
    std::fill(zerosc, zerosc+Nc, 0.0);

    if (smoother == "jacobi"){
        // jacobi smoothing
        jacobi2D(w, lambda, nsmooth, J, rhs, x);
    }else if (smoother == "ssor" || smoother == "sor"){
        // ssor smoothing
        ssor2D(w_ssor, lambda, nsmooth, J, rhs, x);
    }

    // compute residual b-Ax
    // use diff1D later on for this
    residual2D(rhs, lambda, J, x, res);

    // restrict residual to coarser grid
    resc = restrict2D(J, res, true);

    // apply mgv recusrively for coarser grid
    mgv2D(zerosc, lambda/2, smoother, nsmooth, Jc, resc);

    // interpolate coarse solution to fine grid and add to x
    res = interpolate2D(Jc, resc, true);
    for(int i=1; i<J-1;i++){
        for(int j=1; j<J-1;j++){
            x[I2(i,j,J)] += res[I2(i,j,J)];
        }
    }

    if (smoother == "jacobi"){
        // jacobi smoothing
        jacobi2D(w, lambda, nsmooth, J, rhs, x);
    }else if (smoother == "ssor" || smoother == "sor"){
        // ssor smoothing
        ssor2D(w_ssor, lambda, nsmooth, J, rhs, x);
    }

    mkl_free(res);
    mkl_free(zerosc);
}

void fmgv2D(const double * b, const double lambda, const std::string& smoother, const int nsmooth, const int J, double * x){

    const int N = J*J;
    const int k = (int) std::round(std::log2(J-1));
    double lambdaf = lambda*std::exp2(-(k-1));
    int Jf,Jc;

    double * res __attribute__((aligned(ALIGN)));
    res = (double*) allocate(N*sizeof(double));

    // no need to allocate since mgvrhs2D does it for you
    double * resc __attribute__((aligned(ALIGN)));
    double * resf __attribute__((aligned(ALIGN)));
    //resf = nullptr;

    // compute residual
    residual2D(b, lambda, J, x, res);
    
    // restrict residual to coarsest grid
    resc = mgvrhs2D(J, 3, res);

    // exact solution on coarsest grid. BCs should be correctly enforced
    resc[I2(1,1,3)] = resc[I2(1,1,3)]/(1+4*lambdaf);

    for(int i=2; i<k+1; i++){
        Jf = std::exp2(i) + 1;
        Jc = std::exp2(i-1)+1;
        lambdaf = lambda*std::exp2(-(k-i));
        
        // interpolate coarse residual to this level
        resc = interpolate2D(Jc, resc, true);

        // get residual on current grid by repeated restriction
        // of original residual to coarser grids
        resf = mgvrhs2D(J, Jf, res);

        // Multigrid V-cycle
        mgv2D(resf, lambdaf, smoother, nsmooth, Jf, resc); 

        // free resf after use
        mkl_free(resf);
    }

    for(int i=1; i<J-1; i++){
        for(int j=1; j<J-1; j++){
            x[I2(i,j,J)] += resc[I2(i,j,J)];
        }
    }

    mkl_free(res);
    mkl_free(resc);
}

//////////////////////////////////// 3D ///////////////////////////////////////////////////////////////////////

void residual3D(const double * rhs, const double lambda, const int J, const double * x, double * res){
    const int N = J*J*J;

    std::fill(res, res + N, 0.0);

    for(int i=1; i<J-1; i++){
        for(int j=1; j<J-1; j++){
            for(int k=1; k<J-1; k++){
                res[I3(i,j,k,J)] = rhs[I3(i,j,k,J)] - (1+6*lambda)*x[I3(i,j,k,J)] + lambda*(x[I3(i+1,j,k,J)] + x[I3(i-1,j,k,J)] + x[I3(i,j+1,k,J)] + x[I3(i,j-1,k,J)] + x[I3(i,j,k+1,J)] + x[I3(i,j,k-1,J)]); // b[j] - (x[j] + lambda*diff2D(x))
            }
        }
    }
    

}

double * restrict3D(const int J, double * r, const bool deallocate){
    const int N = J*J*J;
    const int Jc = (J+1)/2;
    const int Nc = Jc*Jc*Jc;

    double * rc __attribute__((aligned(ALIGN)));
    rc = (double*) allocate(Nc*sizeof(double));

    std::fill(rc, rc + Nc, 0.0);

    for(int i=1; i<Jc-1; i++){
        for(int j=1; j<Jc-1; j++){
            for(int k=1; k<Jc-1; k++){
                rc[I3(i,j,k,Jc)] = r[I3(2*i,2*j,2*k,J)];
            }
        }
    }

    if(deallocate)
        mkl_free(r);

    return rc;

}

double * interpolate3D(const int Jc, double * rc, const bool deallocate){
    const int Jf = 2*Jc-1;
    const int Nf = Jf*Jf*Jf;
    const int Nc = Jc*Jc*Jc;

    double * rf __attribute__((aligned(ALIGN)));
    rf = (double*) allocate(Nf*sizeof(double));

    for(int i=0; i<Jc; i++){
        for(int j=0; j<Jc; j++){
            for(int k=0; k<Jc; k++){
                rf[I3(2*i,2*j,2*k,Jf)] = rc[I3(i,j,k,Jc)];
            }
        }
    }

    for(int i=0; i<Jc-1; i++){
        for(int j=0; j<Jc; j++){
            for(int k=0; k<Jc; k++){
                rf[I3(2*i+1,2*j,2*k,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i+1,j,k,Jc)])/2;
            }
        }
    }

    for(int i=0; i<Jc; i++){
        for(int j=0; j<Jc-1; j++){
            for(int k=0; k<Jc; k++){
                rf[I3(2*i,2*j+1,2*k,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i,j+1,k,Jc)])/2;
            }
        }
    }

    for(int i=0; i<Jc; i++){
        for(int j=0; j<Jc; j++){
            for(int k=0; k<Jc-1; k++){
                rf[I3(2*i,2*j,2*k+1,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i,j,k+1,Jc)])/2;
            }
        }
    }

    for(int i=0; i<Jc-1; i++){
        for(int j=0; j<Jc-1; j++){
            for(int k=0; k<Jc; k++){
                rf[I3(2*i+1,2*j+1,2*k,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i+1,j,k,Jc)] + rc[I3(i,j+1,k,Jc)] + rc[I3(i+1,j+1,k,Jc)])/4;
            }
        }
    }

    for(int i=0; i<Jc-1; i++){
        for(int j=0; j<Jc; j++){
            for(int k=0; k<Jc-1; k++){
                rf[I3(2*i+1,2*j,2*k+1,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i+1,j,k,Jc)] + rc[I3(i,j,k+1,Jc)] + rc[I3(i+1,j,k+1,Jc)])/4;
            }
        }
    }

    for(int i=0; i<Jc; i++){
        for(int j=0; j<Jc-1; j++){
            for(int k=0; k<Jc-1; k++){
                rf[I3(2*i,2*j+1,2*k+1,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i,j,k+1,Jc)] + rc[I3(i,j+1,k,Jc)] + rc[I3(i,j+1,k+1,Jc)])/4;
            }
        }
    }

    for(int i=0; i<Jc-1; i++){
        for(int j=0; j<Jc-1; j++){
            for(int k=0; k<Jc-1; k++){
                rf[I3(2*i+1,2*j+1,2*k+1,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i+1,j,k,Jc)] + rc[I3(i,j+1,k,Jc)] + rc[I3(i,j,k+1,Jc)] + rc[I3(i+1,j+1,k,Jc)] + rc[I3(i+1,j,k+1,Jc)] + rc[I3(i,j+1,k+1,Jc)] + rc[I3(i+1,j+1,k+1,Jc)])/8;
            }
        }
    }

    if(deallocate)
        mkl_free(rc);

    return rf;

}

double * mgvrhs3D(const int J, const int Jc, double * rhs){

    const int N = J*J*J;

    if(Jc > J)
        throw std::invalid_argument("mgvrhs2D: Jc > J, cannot restrict from coarser to finer.");
    else if(Jc == J){
        // making a copy if on the same level. This is not necessary,
        // but avoids confusion with the pointers
        double * out __attribute__((aligned(ALIGN)));
        out = (double*) allocate(N*sizeof(double));
        for(int i=0; i<N; i++){
            out[i] = rhs[i];
        }

        return out;
    }

    const int nk = (int) std::round(std::log2(J-1));
    const int mk = (int) std::round(std::log2(Jc-1));

    double * out;

    out = restrict3D(J, rhs, false);
    int JJ = (J+1)/2;
    for(int i=nk-2; i>=mk; i--){
        out = restrict3D(JJ, out, true);
        JJ = (JJ+1)/2;
    }

    return out;
}


void mgv3D(const double * rhs, const double lambda, const std::string& smoother, const int nsmooth, const int J, double * x){
    if(J == 3){
        x[I3(1,1,1,J)] = rhs[I3(1,1,1,J)]/(1+6*lambda);
        return;
    }

    const int N = J*J*J;
    const int Jc = (J+1)/2;
    const int Nc = Jc*Jc*Jc;
    const double w = (11*lambda + 1)/(12*lambda + 1);  //(2*dim*lambda + 1)/(4*dim*lambda + 1);
    const double w_ssor = (19*lambda + 1)/(12*lambda + 1);  //(2*dim*lambda + 1)/(4*dim*lambda + 1);

    double * res __attribute__((aligned(ALIGN)));
    res = (double*) allocate(N*sizeof(double));

    double * resc __attribute__((aligned(ALIGN)));
    resc = (double*) allocate(Nc*sizeof(double));

    double * zerosc __attribute__((aligned(ALIGN)));
    zerosc = (double*) allocate(Nc*sizeof(double));
    std::fill(zerosc, zerosc+Nc, 0.0);

    if (smoother == "jacobi"){
        // jacobi smoothing
        jacobi3D(w, lambda, nsmooth, J, rhs, x);
    }else if (smoother == "ssor" || smoother == "sor"){
        // ssor smoothing
        ssor3D(w_ssor, lambda, nsmooth, J, rhs, x);
    }

    // compute residual b-Ax
    // use diff1D later on for this
    residual3D(rhs, lambda, J, x, res);

    // restrict residual to coarser grid
    resc = restrict3D(J, res, true);

    // apply mgv recusrively for coarser grid
    mgv3D(zerosc, lambda/2, smoother, nsmooth, Jc, resc);

    // interpolate coarse solution to fine grid and add to x
    res = interpolate3D(Jc, resc, true);
    for(int i=1; i<J-1;i++){
        for(int j=1; j<J-1;j++){
            for(int k=1; k<J-1;k++){
                x[I3(i,j,k,J)] += res[I3(i,j,k,J)];
            }
        }
    }

    if (smoother == "jacobi"){
        // jacobi smoothing
        jacobi3D(w, lambda, nsmooth, J, rhs, x);
    }else if (smoother == "ssor" || smoother == "sor"){
        // ssor smoothing
        ssor3D(w_ssor, lambda, nsmooth, J, rhs, x);
    }

    mkl_free(res);
    mkl_free(zerosc);
}

void fmgv3D(const double * b, const double lambda, const std::string& smoother, const int nsmooth, const int J, double * x){

    const int N = J*J*J;
    const int k = (int) std::round(std::log2(J-1));
    double lambdaf = lambda*std::exp2(-(k-1));
    int Jf,Jc;

    double * res __attribute__((aligned(ALIGN)));
    res = (double*) allocate(N*sizeof(double));

    // no need to allocate since mgvrhs2D does it for you
    double * resc __attribute__((aligned(ALIGN)));
    double * resf __attribute__((aligned(ALIGN)));
    //resf = nullptr;

    // compute residual
    residual3D(b, lambda, J, x, res);

    // restrict residual to coarsest grid
    resc = mgvrhs3D(J, 3, res);

    // exact solution on coarsest grid. BCs should be correctly enforced
    resc[I3(1,1,1,3)] = resc[I3(1,1,1,3)]/(1+6*lambdaf);

    for(int i=2; i<k+1; i++){
        Jf = std::exp2(i) + 1;
        Jc = std::exp2(i-1)+1;
        lambdaf = lambda*std::exp2(-(k-i));
        
        // interpolate coarse residual to this level
        resc = interpolate3D(Jc, resc, true);

        // get residual on current grid by repeated restriction
        // of original residual to coarser grids
        resf = mgvrhs3D(J, Jf, res);

        // Multigrid V-cycle
        mgv3D(resf, lambdaf, smoother, nsmooth, Jf, resc); 

        // free resf after use
        mkl_free(resf);
    }

    for(int i=1; i<J-1; i++){
        for(int j=1; j<J-1; j++){
            for(int k=1; k<J-1; k++){
                x[I3(i,j,k,J)] += resc[I3(i,j,k,J)];
            }
        }
    }

    mkl_free(res);
    mkl_free(resc);
}

//////////////////////////////////// Putting things together //////////////////////////////////////////////////

void jacobi(const int dim, const double w, const double lambda, const int n_it, const int J, const double * rhs, double *x){
    if(((J-2) & (J-1)) != 0)
        throw std::invalid_argument("jacobi: J-1 is not a power of 2");

    //const double w = (2*dim*lambda + 1)/(4*dim*lambda + 1);
    if(dim == 1){
        jacobi1D(w, lambda, n_it, J, rhs, x);
    }else if(dim == 2){
        jacobi2D(w, lambda, n_it, J, rhs, x);
    }else{
        jacobi3D(w, lambda, n_it, J, rhs, x);
    }

}

void ssor(const int dim, const double w, const double lambda, const int n_it, const int J, const double * rhs, double *x){
    if(((J-2) & (J-1)) != 0)
        throw std::invalid_argument("ssor: J-1 is not a power of 2");

    if(dim == 1){
        ssor1D(w, lambda, n_it, J, rhs, x);
    }else if(dim == 2){
        ssor2D(w, lambda, n_it, J, rhs, x);
    }else{
        ssor3D(w, lambda, n_it, J, rhs, x);
    }

}


void mgv(const int dim, const double * rhs, const double lambda, const std::string& smoother, const int nsmooth, const int J,  double * x){
    if(((J-2) & (J-1)) != 0)
        throw std::invalid_argument("mgv: J-1 is not a power of 2");

    if(dim == 1){
        mgv1D(rhs, lambda, smoother, nsmooth, J, x);
    }else if(dim == 2){
        mgv2D(rhs, lambda, smoother, nsmooth, J, x);
    }else{
        mgv3D(rhs, lambda, smoother, nsmooth, J, x);
    }

}

void fmgv(const int dim, const double * rhs, const double lambda, const std::string& smoother, const int nsmooth, const int J,  double * x){
    if(((J-2) & (J-1)) != 0)
        throw std::invalid_argument("fmgv: J-1 is not a power of 2");

    if(dim == 1){
        fmgv1D(rhs, lambda, smoother, nsmooth, J, x);
    }else if(dim == 2){
        fmgv2D(rhs, lambda, smoother, nsmooth, J, x);
    }else{
        fmgv3D(rhs, lambda, smoother, nsmooth, J, x);
    }

}


void residual(const int dim, const double * rhs, const double lambda, const int J, const double * x, double * res){
    if(((J-2) & (J-1)) != 0)
        throw std::invalid_argument("residual: J-1 is not a power of 2");

    if(dim == 1){
        residual1D(rhs, lambda, J, x, res);
    }else if(dim == 2){
        residual2D(rhs, lambda, J, x, res);
    }else{
        residual3D(rhs, lambda, J, x, res);
    }
}
