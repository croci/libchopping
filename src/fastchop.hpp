#include "chop.hpp"

#define SHIFTBITS_D2H 42
#define MASKBITS_D2H 0x000001FFFFFFFFFF
#define FILTER_NOTSIGNIFICANT_D2H 0xfffffc0000000000

#define SHIFTBITS_D2B 45
#define MASKBITS_D2B 0x00000fffffffffff
#define FILTER_NOTSIGNIFICANT_D2B 0xffffe00000000000

void fastchop_h_rtn(double & x, const Option* op = NULL){
    static_assert(sizeof(double) == sizeof(uint64_t));
    
    uint64_t ui = reinterpret_cast<uint64_t&>(x);
    // the next two lines separate the sign bit from the other bits
    const uint64_t sign = ui & 0x8000000000000000;
    ui = ui & 0x7FFFFFFFFFFFFFFF;

    // check for fp16 subnormals and find how much precision are we losing in case
    // 52 = fp64 significand. After >> 52 you are only left with the exponent bits on the right
    // since we already removed the sign
    // The first number in the scale is Emax(double) - Emin(half) (do not ask me why)
    // the number scale < (number) is 2^t, where t is the precision of fp16 (alternatively
    // it is just 2^(n_significand_bits(half) + 1) (again, do not ask me why)
    const uint64_t scale = 0x00000000000003f1 - (ui >> 52);
    const bool subnormals = scale < 0x000000000000000b;
    // for x>=subnormal mask&shift operators are constant, for subnormals the
    // precision is decreased by 1 bit for every power 2
    const uint64_t shiftbits = subnormals ? SHIFTBITS_D2H + scale : SHIFTBITS_D2H;
    const uint64_t filter = subnormals ? FILTER_NOTSIGNIFICANT_D2H << scale : FILTER_NOTSIGNIFICANT_D2H;
    const uint64_t maskbits = subnormals ? (~filter) >> 1 : MASKBITS_D2H;
    
    // this is the actual RtN
    ui += maskbits + ((ui >> shiftbits) & 0x0000000000000001);
    ui &= filter;

    // This checks the limits of fp16. If smaller than the minimum
    // subnormal number in fp16 / 2, then round to zero
    // if greater than the maximum non-inf fp16 number then round
    // to inf in double
    ui = ui < 0x3e60000000000000 ? 0x0000000000000000 : ui;
    ui = ui > 0x40effc0000000000 ? 0x7ff0000000000000 : ui;

    // put the sign back
    ui = sign | ui;
    
    x = reinterpret_cast<double&>(ui);
}

void fastchopvec_h_rtn(double * x, const unsigned int N, const Option* op = NULL){
    static_assert(sizeof(double) == sizeof(uint64_t));

    uint64_t * ui __attribute__((aligned(ALIGN)));
    uint64_t sign,scale,shiftbits,filter,maskbits;

    ui = reinterpret_cast<uint64_t*>(x);

    #ifdef __INTEL_COMPILER
        __assume_aligned(ui, ALIGN);
    #endif
    #pragma omp parallel for simd default(none) firstprivate(scale,shiftbits,filter,maskbits,N,sign) shared(x,ui) aligned(x,ui: ALIGN)
    for(int i=0; i<N; i++){
        // the next two lines separate the sign bit from the other bits
        sign = ui[i] & 0x8000000000000000;
        ui[i] = ui[i] & 0x7FFFFFFFFFFFFFFF;

        scale = 0x00000000000003f1 - (ui[i] >> 52);
        if(scale < 0x000000000000000b){
            shiftbits = SHIFTBITS_D2H + scale;
            filter = FILTER_NOTSIGNIFICANT_D2H << scale;
            maskbits = (~filter) >> 1;
        }else{
            shiftbits = SHIFTBITS_D2H;
            filter = FILTER_NOTSIGNIFICANT_D2H;
            maskbits = MASKBITS_D2H;
        }

        // this is the actual RtN
        ui[i] += maskbits + ((ui[i] >> shiftbits) & 0x0000000000000001);
        ui[i] &= filter;

        // This checks the limits of fp16. If smaller than the minimum
        // subnormal number in fp16 / 2, then round to zero
        // if greater than the maximum non-inf fp16 number then round
        // to inf in double
        ui[i] = ui[i] < 0x3e60000000000000 ? 0x0000000000000000 : ui[i];
        ui[i] = ui[i] > 0x40effc0000000000 ? 0x7ff0000000000000 : ui[i];

        // put the sign back
        ui[i] = sign | ui[i];
    }
    
    // I suspect this line is not needed in this case
    //x = reinterpret_cast<double*>(ui);
}


void fastchop_h_sr(double & x, const Option* op){
    static_assert(sizeof(double) == sizeof(uint64_t));
    static_assert(sizeof(double) == sizeof(int64_t));
    static_assert(sizeof(double) == sizeof(unsigned MKL_INT64));

    const int tid = omp_get_thread_num();
    const double xmins = std::exp2(-24) - std::exp2(-77); // this is the closest smaller number than xmins(fp16) we are doing this to avoid issues with rand sampling a 0 value
    const double xmin  = std::exp2(-14);

    // if below the subnormal range round to zero (same as in Higham's chop)
    if(std::fabs(x) < xmin){
        double rs;
        vdRngUniform(VSL_RNG_METHOD_UNIFORM_STD_ACCURATE, op->streamptr[tid], 1, &rs, -0.5, 0.5);
        x += xmins*rs;
    }else{
        int64_t  vi = reinterpret_cast<int64_t&>(x);
        unsigned MKL_INT64 r;
        viRngUniformBits64(VSL_RNG_METHOD_UNIFORMBITS64_STD, op->streamptr[tid], 1, &r);
        vi += (reinterpret_cast<int64_t&>(r) >> 22) | 1; // 22 = 64 - (52 - 10) = (nbits_double - delta_significand_double_half)
        x = reinterpret_cast<double&>(vi);
    }
    
    fastchop_h_rtn(x);
}

void fastchopvec_h_sr(double * x, const unsigned int N, const Option* op){
    static_assert(sizeof(double) == sizeof(uint64_t));
    static_assert(sizeof(double) == sizeof(int64_t));
    static_assert(sizeof(double) == sizeof(unsigned MKL_INT64));

    const int tid = omp_get_thread_num();
    int64_t *            vi __attribute__((aligned(ALIGN)));
    int64_t *            ri __attribute__((aligned(ALIGN)));
    unsigned MKL_INT64 * r  __attribute__((aligned(ALIGN)));

    // if below the subnormal range round to zero (same as in Higham's chop)
    vi = reinterpret_cast<int64_t*>(x);

    r = (unsigned MKL_INT64 *) allocate(N*sizeof(unsigned MKL_INT64));
    viRngUniformBits64(VSL_RNG_METHOD_UNIFORMBITS64_STD, op->streamptrvec[tid], N, r);
    ri = reinterpret_cast<int64_t*>(r);

    #ifdef __INTEL_COMPILER
        __assume_aligned(vi, ALIGN);
        __assume_aligned(x,  ALIGN);
        __assume_aligned(ri,  ALIGN);
    #endif
    #pragma omp parallel for simd default(none) firstprivate(N) shared(x,vi,ri) aligned(x,vi,ri: ALIGN)
    for(int i=0; i<N; i++){
        int_fast16_t scale = ((vi[i] & 0x7FFFFFFFFFFFFFFF) >> 52) - 1009;
        if(scale < 0){
            //if scale < -10 we are below the subnormal range
            if (scale < -10){
                x[i] = 0.0;
            }else{
                int_fast8_t shiftbits = 22 + scale; // scale >> 63 is 0 if scale > 0 and -1 otherwise
                int64_t prevpow2 = (vi[i] & 0xfff0000000000000); // this is with sign
                int64_t sign = (vi[i] & 0x8000000000000000) | 0x3ff0000000000000; // +- 1 in fp64

                vi[i] += (ri[i] >> shiftbits) | 1;
                
                double xfix = reinterpret_cast<const double&>(prevpow2) - x[i]/2;
                double sxfix = reinterpret_cast<const double&>(sign)*xfix;
                int_fast8_t needfix = reinterpret_cast<const int64_t&>(sxfix) >> 63;
                x[i] -= needfix*xfix;
            }
        }else{
            vi[i] += (ri[i] >> 22) | 1;
        }
    }

    fastchopvec_h_rtn(x,N);
    
    mkl_free(r);
}

void fastchop_b_rtn(double & x, const Option* op = NULL){
    static_assert(sizeof(double) == sizeof(uint64_t));
    
    uint64_t ui = reinterpret_cast<uint64_t&>(x);
    // the next two lines separate the sign bit from the other bits
    const uint64_t sign = ui & 0x8000000000000000;
    ui = ui & 0x7FFFFFFFFFFFFFFF;

    // check for bfloat16 subnormals and find how much precision are we losing in case
    const uint64_t scale = 0x0000000000000381 - (ui >> 52);
    const bool subnormals = scale < 0x0000000000000100;
    // for x>=subnormal mask&shift operators are constant, for subnormals the
    // precision is decreased by 1 bit for every power 2
    const uint64_t shiftbits = subnormals ? SHIFTBITS_D2B + scale : SHIFTBITS_D2B;
    const uint64_t filter = subnormals ? FILTER_NOTSIGNIFICANT_D2B << scale : FILTER_NOTSIGNIFICANT_D2B;
    const uint64_t maskbits = subnormals ? (~filter) >> 1 : MASKBITS_D2B;
    
    // this is the actual RtN
    ui += maskbits + ((ui >> shiftbits) & 0x0000000000000001);
    ui &= filter;

    // This checks the limits of fp16. If smaller than the minimum
    // subnormal number in fp16 / 2, then round to zero
    // if greater than the maximum non-inf fp16 number then round
    // to inf in double
    ui = ui < 0x3790000000000000 ? 0x0000000000000000 : ui;
    ui = ui > 0x47efe00000000000 ? 0x7ff0000000000000 : ui;

    // put the sign back
    ui = sign | ui;
    
    x = reinterpret_cast<double&>(ui);
}

void fastchopvec_b_rtn(double * x, const unsigned int N, const Option* op = NULL){
    static_assert(sizeof(double) == sizeof(uint64_t));

    uint64_t * ui __attribute__((aligned(ALIGN)));
    uint64_t sign,scale,shiftbits,filter,maskbits;

    ui = reinterpret_cast<uint64_t*>(x);

    #ifdef __INTEL_COMPILER
        __assume_aligned(ui, ALIGN);
    #endif
    #pragma omp parallel for simd default(none) firstprivate(scale,shiftbits,filter,maskbits,N,sign) shared(x,ui) aligned(x,ui: ALIGN)
    for(int i=0; i<N; i++){
        // the next two lines separate the sign bit from the other bits
        sign = ui[i] & 0x8000000000000000;
        ui[i] = ui[i] & 0x7FFFFFFFFFFFFFFF;

        scale = 0x0000000000000381 - (ui[i] >> 52);
        if(scale < 0x0000000000000100){
            shiftbits = SHIFTBITS_D2B + scale;
            filter = FILTER_NOTSIGNIFICANT_D2B << scale;
            maskbits = (~filter) >> 1;
        }else{
            shiftbits = SHIFTBITS_D2B;
            filter = FILTER_NOTSIGNIFICANT_D2B;
            maskbits = MASKBITS_D2B;
        }
        
        // this is the actual RtN
        ui[i] += maskbits + ((ui[i] >> shiftbits) & 0x0000000000000001);
        ui[i] &= filter;

        // This checks the limits of fp16. If smaller than the minimum
        // subnormal number in fp16 / 2, then round to zero
        // if greater than the maximum non-inf fp16 number then round
        // to inf in double
        ui[i] = ui[i] < 0x3790000000000000 ? 0x0000000000000000 : ui[i];
        ui[i] = ui[i] > 0x47efe00000000000 ? 0x7ff0000000000000 : ui[i];

        // put the sign back
        ui[i] = sign | ui[i];
    }
    
    // I suspect this line is not needed in this case
    //x = reinterpret_cast<double*>(ui);
}

void fastchop32_b_rtn(float & x, const Option* op = NULL){
    static_assert(sizeof(float) == sizeof(uint32_t));
    
    uint32_t ui = reinterpret_cast<uint32_t&>(x);
    ui += 0x00007fff + ((ui >> 16) & 0x00000001);
    ui &= 0xffff0000;
    x = reinterpret_cast<float&>(ui);
}

void fastchop_b_sr(double & x, const Option* op){
    static_assert(sizeof(float) == sizeof(uint32_t));
    static_assert(sizeof(float) == sizeof(int32_t));

    const int tid = omp_get_thread_num();
    // cast to float since fp32 and bfloat16 have the same subnormal spacing
    float xx = (float) x;

    int32_t vi = reinterpret_cast<int32_t&>(xx);
    uint32_t ui = reinterpret_cast<uint32_t&>(xx);
    // if below the subnormal range round to zero (same as in Higham's chop)
    const uint32_t absui = ui & 0x7FFFFFFF;
    if(absui < 0x00010000){
        x = 0.0;
        return;
    }

    uint32_t r;
    viRngUniformBits32(VSL_RNG_METHOD_UNIFORMBITS32_STD, op->streamptr[tid], 1, &r);

    vi += (reinterpret_cast<int32_t&>(r) >> 16) | 1; // 16 = 32 - (23 - 7) = nbits_single - (delta_significand_single_bfloat16)

    // RtN fp32 -> bfloat16
    ui = reinterpret_cast<uint32_t&>(vi);
    ui += 0x00007fff + ((ui >> 16) & 0x00000001);
    ui &= 0xffff0000;
    x = (double) reinterpret_cast<float&>(ui);
}

void fastchopvec_b_sr(double * x, const unsigned int N, const Option* op){
    static_assert(sizeof(float) == sizeof(uint32_t));
    static_assert(sizeof(float) == sizeof(int32_t));

    const int tid = omp_get_thread_num();
    float *    xx __attribute__((aligned(ALIGN)));
    uint32_t * ui __attribute__((aligned(ALIGN)));
    int32_t *  vi __attribute__((aligned(ALIGN)));
    int32_t *  ri __attribute__((aligned(ALIGN)));
    uint32_t * r  __attribute__((aligned(ALIGN)));

    xx = (float *) allocate(N*sizeof(float));
    #ifdef __INTEL_COMPILER
        __assume_aligned(x,  ALIGN);
        __assume_aligned(xx,  ALIGN);
    #endif
    #pragma omp parallel for simd default(none) firstprivate(N) shared(x,xx) aligned(x,xx: ALIGN)
    for(int i=0; i<N; i++){
        xx[i] = (float) x[i];
    }

    // if below the subnormal range round to zero (same as in Higham's chop)
    ui = reinterpret_cast<uint32_t*>(xx);
    vi = reinterpret_cast<int32_t*>(xx);

    r = (uint32_t *) allocate(N*sizeof(uint32_t));
    viRngUniformBits32(VSL_RNG_METHOD_UNIFORMBITS32_STD, op->streamptrvec[tid], N, r);
    ri = reinterpret_cast<int32_t*>(r);

    #ifdef __INTEL_COMPILER
        __assume_aligned(ui, ALIGN);
        __assume_aligned(vi, ALIGN);
        __assume_aligned(x,  ALIGN);
        __assume_aligned(xx,  ALIGN);
        __assume_aligned(ri,  ALIGN);
    #endif
    #pragma omp parallel for simd default(none) firstprivate(N) shared(x,xx,ui,vi,ri) aligned(x,xx,ui,vi,ri: ALIGN)
    for(int i=0; i<N; i++){
        uint32_t absui = ui[i] & 0x7FFFFFFF;
        if(absui < 0x00010000){
            x[i] = 0.0;
        }else{
            vi[i] += (ri[i] >> 16) | 1;
            ui[i] += 0x00007fff + ((ui[i] >> 16) & 0x00000001);
            ui[i] &= 0xffff0000;
            x[i] = (double) xx[i];
        }
    }

    mkl_free(r);
    mkl_free(xx);
}

void fastchop_s_rtn(double & x, const Option* op = NULL){
    x = (double) ((float) x);
}

void fastchopvec_s_rtn(double * x, const unsigned int N, const Option* op = NULL){
    #ifdef __INTEL_COMPILER
        __assume_aligned(x,  ALIGN);
    #endif
    #pragma omp parallel for simd default(none) firstprivate(N) shared(x) aligned(x: ALIGN)
    for(int i=0; i<N; i++){
        x[i] = (double) ((float) x[i]);
    }
}

void fastchop_s_sr(double & x, const Option* op){
    static_assert(sizeof(double) == sizeof(uint64_t));
    static_assert(sizeof(double) == sizeof(int64_t));
    static_assert(sizeof(double) == sizeof(unsigned MKL_INT64));

    const int tid = omp_get_thread_num();
    const double xmins = std::exp2(-149) - std::exp2(-202); // this is the closest float which is smaller than xmins(fp32). This is to avoid the 0 case when calling rand
    const double xmin  = std::exp2(-126);

    // if below the subnormal range round to zero (same as in Higham's chop)
    if(std::fabs(x) < xmin){
        double rs;
        vdRngUniform(VSL_RNG_METHOD_UNIFORM_STD_ACCURATE, op->streamptr[tid], 1, &rs, -0.5, 0.5);
        x += xmins*rs;
    }else{
        int64_t vi = reinterpret_cast<int64_t&>(x);
        unsigned MKL_INT64 r;
        viRngUniformBits64(VSL_RNG_METHOD_UNIFORMBITS64_STD, op->streamptr[tid], 1, &r);
        vi += (reinterpret_cast<int64_t&>(r) >> 35) | 1; // 35 = 64 - (52 - 23) = nbits_double - (delta_significand_double_single)
        x = reinterpret_cast<double&>(vi);
        //fastchop_s_rtn(x); // no need to call RtN separately
    }
    x = (double) ((float) x);
}

void fastchopvec_s_sr(double * x, const unsigned int N, const Option* op){
    static_assert(sizeof(double) == sizeof(uint64_t));
    static_assert(sizeof(double) == sizeof(int64_t));
    static_assert(sizeof(double) == sizeof(unsigned MKL_INT64));

    const int tid = omp_get_thread_num();
    uint64_t *           ui __attribute__((aligned(ALIGN)));
    int64_t *            vi __attribute__((aligned(ALIGN)));
    int64_t *            ri __attribute__((aligned(ALIGN)));
    unsigned MKL_INT64 * r  __attribute__((aligned(ALIGN)));

    // if below the subnormal range round to zero (same as in Higham's chop)
    vi = reinterpret_cast<int64_t*>(x);

    r = (unsigned MKL_INT64 *) allocate(N*sizeof(unsigned MKL_INT64));
    viRngUniformBits64(VSL_RNG_METHOD_UNIFORMBITS64_STD, op->streamptrvec[tid], N, r);
    ri = reinterpret_cast<int64_t*>(r);

    #ifdef __INTEL_COMPILER
        __assume_aligned(vi, ALIGN);
        __assume_aligned(x,  ALIGN);
        __assume_aligned(ri,  ALIGN);
    #endif
    #pragma omp parallel for simd default(none) firstprivate(N) shared(x,vi,ri) aligned(x,vi,ri: ALIGN)
    for(int i=0; i<N; i++){
        int_fast16_t scale =((vi[i] & 0x7FFFFFFFFFFFFFFF) >> 52) - 897;
        if(scale < 0){
            if(scale < -23){
                x[i] = 0.0;
            }else{
                int_fast8_t shiftbits = 35 + scale; // scale >> 63 is 0 if scale > 0 and -1 otherwise
                int64_t prevpow2 = (vi[i] & 0xfff0000000000000); // this is with sign
                int64_t sign = (vi[i] & 0x8000000000000000) | 0x3ff0000000000000; // +- 1 in fp64

                vi[i] += (ri[i] >> shiftbits) | 1;
                
                double xfix = reinterpret_cast<const double&>(prevpow2) - x[i]/2;
                double sxfix = reinterpret_cast<const double&>(sign)*xfix;
                int_fast8_t needfix = reinterpret_cast<const int64_t&>(sxfix) >> 63;
                x[i] -= needfix*xfix;
                x[i] = (double) ((float) x[i]); // no need to call RtN separately
            }
        }else{
            vi[i] += (ri[i] >> 35) | 1;
            x[i] = (double) ((float) x[i]); // no need to call RtN separately
        }
    }

    mkl_free(r);
}

//// if x > 0
//old_vi + (new_vi - nextpow2)/2 + nextpow2 - old_vi
//(new_vi - nextpow2 + 2*nextpow2)/2
//(new_vi + nextpow2)/2
//new_vi/2 + prevpow2 // if new_vi/2 - prevpow2 > 0 // if new_vi/2 + prevpow2 > 2*prevpow2
//// if x < 0
//old_vi + (new_vi - nextpow2)/2 + nextpow2 - old_vi
//new_vi/2 + prevpow2 // if new_vi/2 - prevpow2 < 0

/*
void fastchop_h_sr_experimental(double & x, const Option* op){
    static_assert(sizeof(double) == sizeof(uint64_t));
    static_assert(sizeof(double) == sizeof(int64_t));
    static_assert(sizeof(double) == sizeof(unsigned MKL_INT64));

    const int tid = omp_get_thread_num();

    int64_t  vi = reinterpret_cast<int64_t&>(x);

    //// if below the subnormal range round to zero (same as in Higham's chop)
    //uint64_t ui = reinterpret_cast<uint64_t&>(x);
    //const uint64_t absui = ui & 0x7FFFFFFFFFFFFFFF;
    //if(absui < 0x3e70000000000000){
    //    x = 0.0;
    //    return;
    //}

    unsigned MKL_INT64 r;
    viRngUniformBits64(VSL_RNG_METHOD_UNIFORMBITS64_STD, op->streamptr[tid], 1, &r);

    //const int64_t scale = 1009 - (reinterpret_cast<const int64_t&>(absui) >> 52);
    // 22 = 64 - (52 - (10-scale)) = 22 - scale (i.e. you lose precision if you are in the subnormals, compare this with the RtN algorithm)
    //const int64_t shiftbits = scale > 0 ? 22 - |scale| : 22;
    //const int64_t scale = (reinterpret_cast<const int64_t&>(absui) >> 52) - 1009;
    const int64_t scale = ((vi & 0x7FFFFFFFFFFFFFFF) >> 52) - 1009;
    const int64_t below_subnormal_range = -(-(scale + 10) >> 63); // 0 if below range 1 otherwise
    const int64_t subnormal = scale >> 63; // 0 if normal, -1 if subnormal
    const int64_t shiftbits = 22 - subnormal*scale; // scale >> 63 is 0 if scale > 0 and -1 otherwise
    const int64_t prevpow2 = (vi & 0xfff0000000000000); // this is with sign
    const int64_t sign = (vi & 0x8000000000000000) | 0x3ff0000000000000; // +- 1 in fp64

    vi = vi + (reinterpret_cast<int64_t&>(r) >> shiftbits); // 22 = 64 - (52 - 10) = (nbits_double - delta_significand_double_half)
    x = reinterpret_cast<double&>(vi);
    
    // x = scale < 0 && s*(x/2-pp2) > 0 ? x/2 + pp2 : x; // equivalent to:
    // x + (x/2-pp2)*(-(scale >> 63))*(reinterpret(Int64, s*(pp2 -x/2)) >> 63) // this gets rid of both conditions
    const double xfix = reinterpret_cast<const double&>(prevpow2) - x/2;
    const double sxfix = reinterpret_cast<const double&>(sign)*xfix;
    const int64_t needfix = subnormal*(reinterpret_cast<const int64_t&>(sxfix) >> 63); // this is either -1 if needfix or 0
    x += needfix*xfix;
    x *= below_subnormal_range;

    fastchop_h_rtn(x);
}
*/
