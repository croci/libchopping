#include "chopping.hpp"

#define th    11
#define emaxh 15
#define tb    8
#define emaxb 127
#define ts    24
#define emaxs 127
#define td    53
#define emaxd 1023

struct float_params {
    int t,emax,emin,emins;
    double xmins, xmin, xmax, u;
};

float_params floating_point_parameters(const char fmt){
    float_params params;

    switch (fmt){
        case 'h': params.t = th; params.emax = emaxh; break;
        case 'b': params.t = tb; params.emax = emaxb; break;
        case 's': params.t = ts; params.emax = emaxs; break;
        case 'd': params.t = td; params.emax = emaxd; break;
        default: throw std::invalid_argument("Option: invalid format\n");
    }

    params.emin = 1-params.emax;                   // Exponent of smallest normal number.
    params.emins = params.emin + 1 - params.t;     // Exponent of smallest subnormal number.

    params.xmins = std::exp2(params.emins);
    params.xmin = std::exp2(params.emin);
    params.xmax = std::exp2(params.emax) * (2-std::exp2(1-params.t));
    params.u = std::exp2(-params.t);
    return params;
}

class Option;

template <typename real>
void basechop(real &x, const Option *opts);
template <typename real>
void basechopvec(real *x, const unsigned int N, const Option *opts);

void fastchop_d(double & x, const Option* op = NULL){};
void fastchopvec_d(double * x, const unsigned int N, const Option* op = NULL){};

void fastchop_h_rtn(double & x, const Option* op);
void fastchopvec_h_rtn(double * x, const unsigned int N, const Option* op);
void fastchop_h_sr(double & x, const Option* op);
void fastchopvec_h_sr(double * x, const unsigned int N, const Option* op);

void fastchop_b_rtn(double & x, const Option* op);
void fastchopvec_b_rtn(double * x, const unsigned int N, const Option* op);
void fastchop_b_sr(double & x, const Option* op);
void fastchopvec_b_sr(double * x, const unsigned int N, const Option* op);

void fastchop_s_rtn(double & x, const Option* op);
void fastchopvec_s_rtn(double * x, const unsigned int N, const Option* op);
void fastchop_s_sr(double & x, const Option* op);
void fastchopvec_s_sr(double * x, const unsigned int N, const Option* op);

class Option {
    public:
        bool fast;
        bool subnormal;
        bool flip;
        double p;
        bool explim;
        VSLStreamStatePtr* streamptr;
        VSLStreamStatePtr* streamptrvec;
        bool issetup;
        void (*chopfun)(double & x, const Option * op);
        void (*chopvecfun)(double * x, const unsigned int N, const Option * op);

        Option(const bool fastflag=false);
        ~Option();

        void setUp();
        void set_custom_format(const int tt, const int emax);
        void set_format(const char fmt);
        void set_seed(const int sd);
        void set_round(const int mode);
        void set(const char fmt, const int mode);
        int get_round();
        int get_seed();
        char get_format();
        bool is_fast();
        void set_fast(const bool fastflag);
        void deallocate_streams();
        int get_precision();
        int get_max_exponent();
        float_params get_floating_point_parameters();

        template <typename real>
        friend void roundit(real *x, const unsigned int N, const Option *opts);
        template <typename real>
        friend void basechop(real &x, const Option *opts);
        template <typename real>
        friend void basechopvec(real *x, const unsigned int N, const Option *opts);

    private:
        int seed;
        char format;
        int round;
        int t;
        int emax;
};

Option::Option(const bool fastflag){
    fast         = fastflag;
    format       = 'h';
    subnormal    = true;
    round        = 1;
    flip         = false;
    p            = 0.5;
    explim       = true;
    seed         = 3;
    issetup      = false;
    streamptr    = NULL;
    streamptrvec = NULL;
    chopfun      = &basechop<double>;
    chopvecfun   = &basechopvec<double>;
    this->setUp();
}

Option::~Option(){
    this->deallocate_streams();
}

void Option::deallocate_streams(){
    if(issetup){
        if (streamptr != NULL){
            for(int i=0;i<nthreads;i++){
                vslDeleteStream(&streamptr[i]);  
            }
            mkl_free(streamptr);
            streamptr = NULL;
        }
        if (streamptrvec != NULL){
            for(int i=0;i<nthreads;i++){
                vslDeleteStream(&streamptrvec[i]);  
            }
            mkl_free(streamptrvec);
            streamptrvec = NULL;
        }

        issetup = false;
    }
}

float_params Option::get_floating_point_parameters(){
    float_params params;

    params.t = t;
    params.emax = emax;

    params.emin = 1-params.emax;                   // Exponent of smallest normal number.
    params.emins = params.emin + 1 - params.t;     // Exponent of smallest subnormal number.

    params.xmins = std::exp2(params.emins);
    params.xmin = std::exp2(params.emin);
    params.xmax = std::exp2(params.emax) * (2-std::exp2(1-params.t));
    params.u = std::exp2(-params.t);
    return params;
}

bool Option::is_fast(){
    return fast;
}

int Option::get_precision(){
    return t;
}

int Option::get_max_exponent(){
    return emax;
}

void Option::set_fast(const bool fastflag){
    if(fastflag && ((round != 1 && round != 5) || format == 'c')){
        throw std::invalid_argument("Unsupported rounding mode or format for fast emulation.\n");
    }
    fast = fastflag;
    this->setUp();
}

char Option::get_format(){
    return format;
}

void Option::set_format(const char fmt){
    if(fmt == 'c')
        throw std::invalid_argument("Use option.set_custom_format to set a custom floating point format.\n");
    format = fmt;
    this->setUp();
}

void Option::set_custom_format(const int tt, const int eemax){
    if(fast)
        throw std::invalid_argument("Fast emulation does not support custom formats.\n");
    format = 'c';
    t = tt;
    emax = eemax;
    this->setUp();
}

void Option::set_round(const int mode){
    if(mode > 6)
        throw std::invalid_argument("Unsupported value of rounding mode.\n");
    if(fast && (mode != 1 && mode != 5))
        throw std::invalid_argument("Unsupported rounding mode for fast emulation.\n");
    round = mode;
    this->setUp();
}

int Option::get_round(){
    return round;
}


int Option::get_seed(){
    return seed;
}

void Option::set_seed(const int sd){
    this->deallocate_streams();
    seed = sd;
    this->setUp();
}

void Option::setUp(){

    if(fast && ((round != 1 && round != 5) || format == 'c')){
        std::cout << "WARNING! Unsupported rounding mode or format for fast emulation, switching to basic emulation.\n";
        fast = false;
    }

    chopfun = &basechop<double>;
    chopvecfun = &basechopvec<double>;

    // No rounding if double and no custom format
    switch (format) {
        case 'h':
            t = th; emax = emaxh;
            if (fast){
                if (round == 1){
                    chopfun = &fastchop_h_rtn; chopvecfun = &fastchopvec_h_rtn;
                }
                if (round == 5){
                    chopfun = &fastchop_h_sr; chopvecfun = &fastchopvec_h_sr;
                }
            }
            break;
        case 'b':
            t = tb; emax = emaxb;
            if (fast){
                if (round == 1){
                    chopfun = &fastchop_b_rtn; chopvecfun = &fastchopvec_b_rtn;
                }
                if (round == 5){
                    chopfun = &fastchop_b_sr;  chopvecfun = &fastchopvec_b_sr;
                }
            }
            break;
        case 's':
            t = ts; emax = emaxs;
            if (fast){
                if (round == 1){
                    chopfun = &fastchop_s_rtn; chopvecfun = &fastchopvec_s_rtn;
                }
                if (round == 5){
                    chopfun = &fastchop_s_sr;  chopvecfun = &fastchopvec_s_sr;
                }
            }
            break;
        case 'd':
            t = td; emax = emaxd;
            if (fast){
                chopfun = &fastchop_d; chopvecfun = fastchopvec_d;
            }
            break;
        case 'c':
              //if(t > 11)
              //    throw std::invalid_argument("Precision of custom format must be less than 12 if working in single\n");
              //else if(t > 25)
              if(t > 25)
                  throw std::invalid_argument("Precision of custom format must be less than 26\n");

            break;
        default: 
            throw std::invalid_argument("Option: invalid format\n");
    }

    if(!issetup){
        streamptr = (VSLStreamStatePtr*) allocate(nthreads*sizeof(VSLStreamStatePtr));
        for(int i=0;i<nthreads;i++){
            vslNewStream(&streamptr[i], VSL_BRNG_MT19937, seed+i);
        }
        streamptrvec = (VSLStreamStatePtr*) allocate(nthreads*sizeof(VSLStreamStatePtr));
        for(int i=0;i<nthreads;i++){
            vslNewStream(&streamptrvec[i], VSL_BRNG_SFMT19937, seed+nthreads+i);
        }

        issetup = true;
    }

}

