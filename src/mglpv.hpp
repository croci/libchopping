// #include<LPV.hpp>
//#include "../chopping.hpp"
//

#if !defined(I2)
#define I2(i,j,J) ((j)*J+i)
#endif
#if !defined(I3)
#define I3(i,j,k,J) (((k)*J+j)*J+i)
#endif

template <typename real>
LPV<real> add_zero_boundary(const int dim, LPV<real> x){
    const int N = x.N;
    const int J = (int) std::round(std::pow(N, 1.0/dim));
    const int Jb = J+2;
    const int Nb = std::pow(Jb, dim);

    real * xx __attribute__((aligned(ALIGN)));
    xx = x.x;

    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate(Nb*sizeof(real));

    #ifdef __INTEL_COMPILER
        __assume_aligned(out, ALIGN);
        __assume_aligned(xx, ALIGN);
    #endif

    myfill(out, Nb, 0.0);

    if (dim == 1){
        for(int i=1; i<Jb-1; i++){
            out[i] = xx[i-1];
        }

    }else if (dim == 2){
        #pragma omp parallel for default(none) firstprivate(Jb, J) shared(xx,out)
        for(int j=1; j<Jb-1; j++){
            #pragma omp simd aligned(out, xx: ALIGN)
            for(int i=1; i<Jb-1; i++){
                out[I2(i,j,Jb)] = xx[I2(i-1,j-1,J)];
            }
        }

    } else {
        #pragma omp parallel for default(none) firstprivate(Jb, J) shared(xx,out) collapse(2)
        for(int k=1; k<Jb-1; k++){
            for(int j=1; j<Jb-1; j++){
                #pragma omp simd aligned(out, xx: ALIGN)
                for(int i=1; i<Jb-1; i++){
                    out[I3(i,j,k,Jb)] = xx[I3(i-1,j-1,k-1,J)];
                }
            }
        }

    }

    LPV<real> lpout = LPV<real>(out, Nb, 1, x.opts, true, false);

    return lpout;
}

template <typename real>
LPV<real> remove_zero_boundary(const int dim, LPV<real> x){
    const int Nb = x.N;
    const int Jb = (int) std::round(std::pow(Nb, 1.0/dim));
    const int J = Jb-2;
    const int N = std::pow(J, dim);

    real * xx __attribute__((aligned(ALIGN)));
    xx = x.x;

    real * out __attribute__((aligned(ALIGN)));
    out = (real*) allocate(N*sizeof(real));

    #ifdef __INTEL_COMPILER
        __assume_aligned(out, ALIGN);
        __assume_aligned(xx, ALIGN);
    #endif

    if (dim == 1){
        for(int i=1; i<Jb-1; i++){
            out[i-1] = xx[i];
        }

    }else if (dim == 2){
        #pragma omp parallel for default(none) shared(xx,out) firstprivate(Jb, J)
        for(int j=1; j<Jb-1; j++){
            #pragma omp simd aligned(out, xx: ALIGN)
            for(int i=1; i<Jb-1; i++){
                out[I2(i-1,j-1,J)] = xx[I2(i,j,Jb)];
            }
        }

    } else {
        #pragma omp parallel for default(none) shared(xx,out) firstprivate(Jb, J) collapse(2)
        for(int k=1; k<Jb-1; k++){
            for(int j=1; j<Jb-1; j++){
                #pragma omp simd aligned(out, xx: ALIGN)
                for(int i=1; i<Jb-1; i++){
                    out[I3(i-1,j-1,k-1,J)] = xx[I3(i,j,k,Jb)];
                }
            }
        }

    }

    LPV<real> lpout = LPV<real>(out, N, 1, x.opts, true, false);

    return lpout;
}

//////////////////////////////////// Jacobi /////////////////////////////////////////////////////////////////////

template <typename real>
void jacobi1D(const double w, const double lambda, const int n_it, const LPV<real>& rhs, LPV<real>& x){
    check_all(rhs, x);
    // assumes BC are incorporated in the vector so that we are safe; vector is J \times 1, with J = K+2
    const int N = x.N;
    const int J = x.N;
    const double wd = w/(2*1*lambda + 1);
    const double omw = 1-w;

    real * temp __attribute__((aligned(ALIGN)));
    temp = (real*) allocate(N*sizeof(real));

    for(int it=0; it<n_it; it++){
        for(int j=1; j<J-1; j++)
            temp[j] = value((omw*x[j] + wd*(rhs[j] + lambda*(x[j+1] + x[j-1])))); 

        for(int j=1; j<J-1; j++)
            x.x[j] = temp[j];
        
    }

    mkl_free(temp);
}

template <typename real>
void jacobi2D(const double w, const double lambda, const int n_it, const LPV<real> &rhs, LPV<real> &x){
    check_all(rhs, x);
    // assumes BC are incorporated in the vector so that we are safe; vector is J \times J, with J = K+2
    const int N = x.N;
    const int J = (int) std::round(std::sqrt(N));
    const double wd = w/(2*2*lambda + 1);
    const double omw = 1-w;

    real * xx __attribute__((aligned(ALIGN)));
    xx = x.x;

    real * temp __attribute__((aligned(ALIGN)));
    temp = (real*) allocate(N*sizeof(real));

    #ifdef __INTEL_COMPILER
        __assume_aligned(temp, ALIGN);
        __assume_aligned(xx, ALIGN);
    #endif
    #pragma omp parallel default(none) firstprivate(J,n_it,lambda,wd,omw) shared(rhs,temp,x,xx)
    {
    for(int it=0; it<n_it; it++){

        #pragma omp for
        for(int j=1; j<J-1; j++){
            for(int i=1; i<J-1; i++){
                temp[j*J+i] = value((omw*x[j*J+i] + wd*(rhs[j*J+i] + lambda*(x[(j+1)*J+i] + x[(j-1)*J+i] + x[j*J+i+1] + x[j*J+i-1])))); 
            }
        }

        #pragma omp barrier

        #pragma omp for
        for(int j=1; j<J-1; j++){
            #pragma omp simd aligned(temp, xx: ALIGN)
            for(int i=1; i<J-1; i++){
                xx[j*J+i] = temp[j*J+i];
            }
        }
        
    }
    }
    mkl_free(temp);
}

template <typename real>
void jacobi3D(const double w, const double lambda, const int n_it, const LPV<real> &rhs, LPV<real> &x){
    check_all(rhs, x);
    // assumes BC are incorporated in the vector so that we are safe; vector is J \times J, with J = K+2
    const int N = x.N;
    const int J = (int) std::round(std::cbrt(N));
    const double wd = w/(2*3*lambda + 1);
    const double omw = 1-w;

    real * xx __attribute__((aligned(ALIGN)));
    xx = x.x;

    real * temp __attribute__((aligned(ALIGN)));
    temp = (real*) allocate(N*sizeof(real));

    #ifdef __INTEL_COMPILER
        __assume_aligned(temp, ALIGN);
        __assume_aligned(xx, ALIGN);
    #endif
    #pragma omp parallel default(none) firstprivate(J,omw,wd,lambda,n_it) shared(rhs,temp,x,xx)
    {
    for(int it=0; it<n_it; it++){

        #pragma omp for collapse(2)
        for(int k=1; k<J-1; k++){
            for(int j=1; j<J-1; j++){
                for(int i=1; i<J-1; i++){
                    temp[(J*k+j)*J+i] = value((omw*x[(J*k+j)*J+i] + wd*(rhs[(J*k+j)*J+i] + lambda*(x[(J*k+j)*J+i+1] + x[(J*k+j)*J+i-1] + x[(J*k+j+1)*J+i] + x[(J*k+j-1)*J+i] + x[(J*(k+1)+j)*J+i] + x[(J*(k-1)+j)*J+i])))); 
                }
            }
        }

        #pragma omp barrier

        #pragma omp for collapse(2)
        for(int k=1; k<J-1; k++){
            for(int j=1; j<J-1; j++){
                #pragma omp simd aligned(temp, xx: ALIGN)
                for(int i=1; i<J-1; i++){
                    xx[(J*k+j)*J+i] = temp[(J*k+j)*J+i];
                }
            }
        }
        
    }
    }

    mkl_free(temp);
}

//////////////////////////////////// SSOR //////////////////////////////////////////////////////////////////////

//NOTE: CANNOT USE OpenMP with SSOR since each x[j] depends on the previous ones.
//      we could implement red-black ordering, but it is a pain

template <typename real>
void ssor1D(const double w, const double lambda, const int n_it, const LPV<real> &rhs, LPV<real> &x){
    check_all(rhs, x);
    // assumes BC are incorporated in the vector so that we are safe; vector is J \times 1, with J = K+2
    const int N = x.N;
    const int J = x.N;
    const double wd = w/(2*1*lambda + 1);
    const double omw = 1-w;

    for(int it=0; it<n_it; it++){
        for(int j=1; j<J-1; j++)
            x[j] = omw*x[j] + wd*(rhs[j] + lambda*(x[j+1] + x[j-1])); 
    }
}

template <typename real>
void ssor2D(const double w, const double lambda, const int n_it, const LPV<real> &rhs, LPV<real> &x){
    check_all(rhs, x);
    // assumes BC are incorporated in the vector so that we are safe; vector is J \times J, with J = K+2
    const int N = x.N;
    const int J = (int) std::round(std::sqrt(N));
    const double wd = w/(2*2*lambda + 1);
    const double omw = 1-w;

    for(int it=0; it<n_it; it++){

        for(int i=1; i<J-1; i++){
            for(int j=1; j<J-1; j++){
                x[j*J+i] = omw*x[j*J+i] + wd*(rhs[j*J+i] + lambda*(x[(j+1)*J+i] + x[(j-1)*J+i] + x[j*J+i+1] + x[j*J+i-1])); 
            }
        }

    }
}

template <typename real>
void ssor3D(const double w, const double lambda, const int n_it, const LPV<real> &rhs, LPV<real> &x){
    check_all(rhs, x);
    // assumes BC are incorporated in the vector so that we are safe; vector is J \times J, with J = K+2
    const int N = x.N;
    const int J = (int) std::round(std::cbrt(N));
    const double wd = w/(2*3*lambda + 1);
    const double omw = 1-w;

    for(int it=0; it<n_it; it++){

        for(int i=1; i<J-1; i++){
            for(int j=1; j<J-1; j++){
                for(int k=1; k<J-1; k++){
                    x[(J*k+j)*J+i] = omw*x[(J*k+j)*J+i] + wd*(rhs[(J*k+j)*J+i] + lambda*(x[(J*k+j)*J+i+1] + x[(J*k+j)*J+i-1] + x[(J*k+j+1)*J+i] + x[(J*k+j-1)*J+i] + x[(J*(k+1)+j)*J+i] + x[(J*(k-1)+j)*J+i])); 
                }
            }
        }

    }
}

//////////////////////////////////// 1D ///////////////////////////////////////////////////////////////////////

template <typename real>
void residual1D(const LPV<real> &rhs, const double lambda, const LPV<real> &x, LPV<real> &res){
    check_all(rhs, x); check_all(rhs, res);
    const int N = x.N;
    const int J = x.N;

    res.x[0] = 0.0; res.x[N-1] = 0.0;
    for(int j=1; j<J-1; j++){
        //res[j] = rhs[j] - (1+2*lambda)*x[j] + lambda*(x[j-1] + x[j+1]); // b[j] - x[j] + lambda*diff1D(x)
        res[j] = rhs[j] + (-x[j] + lambda*((x[j+1] - x[j]) - (x[j] - x[j-1]))); 
    }
}

template <typename real>
LPV<real> restrict1D(const LPV<real> &r){
    const int N = r.N;
    const int J = r.N;
    const int Jc = (J+1)/2;
    const int Nc = Jc;

    real * rc __attribute__((aligned(ALIGN)));
    rc = (real*) allocate(Nc*sizeof(real));

    rc[0] = 0.0; rc[Nc-1] = 0.0;
    for(int j=1; j<Jc-1; j++){
        //rc[j] = 0.5*r[2*j] + 0.25*(r[2*j+1] + r[2*j-1]);
        rc[j] = r.x[2*j];
    }

    LPV<real> rc_lpv = LPV<real>(rc, Nc, 1, r.opts, true, false);
    return rc_lpv;

}

template <typename real>
LPV<real> interpolate1D(const LPV<real> &rc){
    const int Nc = rc.N;
    const int Jc = rc.N;
    const int Jf = 2*Jc-1;
    const int Nf = Jf;

    real * rf_arr __attribute__((aligned(ALIGN)));
    rf_arr = (real*) allocate(Nf*sizeof(real));

    LPV<real> rf = LPV<real>(rf_arr, Nf, 1, rc.opts, true, false);

    // interpolate coarse solution to finer grid using weighted interpolator
    for(int j=0; j<Jc-1; j++){
        rf.x[2*j] = rc.x[j];
        rf[2*j+1] = (rc[j] + rc[j+1])/2;
    } 
    rf.x[2*(Jc-1)] = rc.x[Jc-1];

    return rf;
}

template <typename real>
LPV<real> mgvrhs1D(const int Jc, const LPV<real> &rhs){
    const int J = rhs.N;
    if(Jc > J)
        throw std::invalid_argument("mgvrhs1D: Jc > J, cannot restrict from coarser to finer.");
    else if(Jc == J){
        // making a copy if on the same level. This is not necessary,
        // but avoids confusion with the pointers
        return LPV<real>(rhs);
    }

    const int nk = (int) std::round(std::log2(J-1));
    const int mk = (int) std::round(std::log2(Jc-1));

    LPV<real> out = restrict1D(rhs);
    for(int i=nk-2; i>=mk; i--){
        out = restrict1D(out);
    }

    return out;
}

template <typename real>
void mgv1D(const LPV<real> &rhs, const double lambda, const std::string& smoother, const int nsmooth,  LPV<real> &x){
    check_all(rhs, x);
    const int N = x.N;
    const int J = x.N;
    if(J == 3){
        // using double precision here and rounding the double precision value
        x[1] = rhs.x[1]/(1+2*lambda);
        return;
    }

    const int Jc = (J+1)/2;
    const int Nc = Jc;
    const double w = (2*lambda + 1)/(3*lambda + 1); // 1/(2*dim*lambda/(1+2*dim*lambda)+1);
    const double w_ssor = (4*lambda + 1)/(3*lambda + 1); // 1/(2*dim*lambda/(1+2*dim*lambda)+1);

    real * res_arr __attribute__((aligned(ALIGN)));
    res_arr = (real*) allocate(N*sizeof(real));
    LPV<real> res = LPV<real>(res_arr, N, 1, x.opts, true, false);

    real * resc_arr __attribute__((aligned(ALIGN)));
    resc_arr = (real*) allocate(Nc*sizeof(real));
    LPV<real> resc = LPV<real>(resc_arr, Nc, 1, x.opts, true, false);

    real * zerosc_arr __attribute__((aligned(ALIGN)));
    zerosc_arr = (real*) allocate(Nc*sizeof(real));
    myfill(zerosc_arr, Nc, 0.0);
    LPV<real> zerosc = LPV<real>(zerosc_arr, Nc, 1, x.opts, true, false);

    if (smoother == "jacobi"){
        // jacobi smoothing
        jacobi1D(w, lambda, nsmooth, rhs, x);
    }else if (smoother == "ssor" || smoother == "sor"){
        // ssor smoothing
        ssor1D(w_ssor, lambda, nsmooth, rhs, x);
    }

    // compute residual b-Ax
    // use diff1D later on for this
    residual1D(rhs, lambda, x, res);

    // restrict residual to coarser grid
    resc = restrict1D(res);

    // apply mgv recusrively for coarser grid
    mgv1D(zerosc, lambda/2, smoother, nsmooth, resc);

    // interpolate coarse solution to fine grid and add to x
    res = interpolate1D(resc);
    for(int j=1; j<J-1;j++)
        x[j] = x[j] + res[j];

    if (smoother == "jacobi"){
        // jacobi smoothing
        jacobi1D(w, lambda, nsmooth, rhs, x);
    }else if (smoother == "ssor" || smoother == "sor"){
        // ssor smoothing
        ssor1D(w_ssor, lambda, nsmooth, rhs, x);
    }
}

template <typename real>
void fmgv1D(const LPV<real> &b, const double lambda, const std::string& smoother, const int nsmooth,  LPV<real> &x){
    check_all(b, x);

    const int N = x.N;
    const int J = x.N;
    const int k = (int) std::round(std::log2(N-1));
    double lambdaf = lambda*std::exp2(-(k-1));
    int Jf,Jc;

    real * res_arr __attribute__((aligned(ALIGN)));
    res_arr = (real*) allocate(N*sizeof(real));
    LPV<real> res = LPV<real>(res_arr, N, 1, x.opts, true, false);

    // no need to allocate since mgvrhs1D does it for you
    LPV<real> resc, resf;

    // compute residual
    residual1D(b, lambda, x, res);

    // restrict residual to coarsest grid
    resc = mgvrhs1D(3, res);

    // exact solution on coarsest grid
    // using double precision here and rounding the double precision value
    resc.x[0] = 0.0; resc[1] = resc.x[1]/(1+2*lambdaf); resc.x[2] = 0.0;

    for(int i=2; i<k+1; i++){
        Jf = std::exp2(i) + 1;
        Jc = std::exp2(i-1)+1;
        lambdaf = lambda*std::exp2(-(k-i));
        
        // interpolate coarse residual to this level
        resc = interpolate1D(resc);

        // get residual on current grid by repeated restriction
        // of original residual to coarser grids
        resf = mgvrhs1D(Jf, res);

        // Multigrid V-cycle
        mgv1D(resf, lambdaf, smoother, nsmooth, resc); 
    }

    for(int i=1; i<J-1; i++){
        x[i] = x[i] + resc[i];
    }

}

//////////////////////////////////// 2D ///////////////////////////////////////////////////////////////////////

template <typename real>
void residual2D(const LPV<real> &rhs, const double lambda, const LPV<real> &x, LPV<real> &res){
    check_all(rhs, x); check_all(rhs, res);
    const int N = x.N;
    const int J = (int) std::round(std::sqrt(N));

    myfill(res.x, N, 0.0);

    #pragma omp parallel for default(none) firstprivate(J,lambda) shared(res, rhs, x)
    for(int j=1; j<J-1; j++){
        for(int i=1; i<J-1; i++){
            res[I2(i,j,J)] = rhs[I2(i,j,J)] + (-x[I2(i,j,J)] + lambda*( ((x[I2(i+1,j,J)] - x[I2(i,j,J)]) - (x[I2(i,j,J)] - x[I2(i-1,j,J)]))
                                                                      + ((x[I2(i,j+1,J)] - x[I2(i,j,J)]) - (x[I2(i,j,J)] - x[I2(i,j-1,J)])) )); // b[j] - (x[j] + lambda*diff2D(x))
        }
    }
}

template <typename real>
LPV<real> restrict2D(const LPV<real> &r){
    const int N = r.N;
    const int J = (int) std::round(std::sqrt(N));
    const int Jc = (J+1)/2;
    const int Nc = Jc*Jc;

    real * rx __attribute__((aligned(ALIGN)));
    rx = r.x;

    real * rc __attribute__((aligned(ALIGN)));
    rc = (real*) allocate(Nc*sizeof(real));


    #ifdef __INTEL_COMPILER
        __assume_aligned(rc, ALIGN);
        __assume_aligned(rx, ALIGN);
    #endif

    myfill(rc, Nc, 0.0);

    #pragma omp parallel for default(none) firstprivate(Jc,J) shared(rc, rx)
    for(int j=1; j<Jc-1; j++){
        #pragma omp simd aligned(rc,rx: ALIGN)
        for(int i=1; i<Jc-1; i++){
            rc[I2(i,j,Jc)] = rx[I2(2*i,2*j,J)];
        }
    }

    LPV<real> rc_lpv = LPV<real>(rc, Nc, 1, r.opts, true, false);
    return rc_lpv;

}

template <typename real>
LPV<real> interpolate2D(const LPV<real> &rc){
    const int Nc = rc.N;
    const int Jc = (int) std::round(std::sqrt(Nc));
    const int Jf = 2*Jc-1;
    const int Nf = Jf*Jf;

    real * rf_arr __attribute__((aligned(ALIGN)));
    rf_arr = (real*) allocate(Nf*sizeof(real));

    LPV<real> rf = LPV<real>(rf_arr, Nf, 1, rc.opts, true, false);

    real * rfx __attribute__((aligned(ALIGN)));
    real * rcx __attribute__((aligned(ALIGN)));
    rfx = rf.x; rcx = rc.x;

    #ifdef __INTEL_COMPILER
        __assume_aligned(rcx, ALIGN);
        __assume_aligned(rfx, ALIGN);
    #endif

    #pragma omp parallel default(none) firstprivate(Jc,Jf) shared(rfx, rcx, rc, rf)
    {
    #pragma omp for
    for(int j=0; j<Jc; j++){
        #pragma omp simd aligned(rcx,rfx: ALIGN)
        for(int i=0; i<Jc; i++){
            rfx[I2(2*i,2*j,Jf)] = rcx[I2(i,j,Jc)];
        } 
    }

    #pragma omp for
    for(int j=0; j<Jc; j++){
        for(int i=0; i<Jc-1; i++){
            rf[I2(2*i+1,2*j,Jf)] = (rc[I2(i,j,Jc)] + rc[I2(i+1,j,Jc)])/2;
        } 
    }

    #pragma omp for
    for(int j=0; j<Jc-1; j++){
        for(int i=0; i<Jc; i++){
            rf[I2(2*i,2*j+1,Jf)] = (rc[I2(i,j,Jc)] + rc[I2(i,j+1,Jc)])/2;
        } 
    }

    #pragma omp for
    for(int j=0; j<Jc-1; j++){
        for(int i=0; i<Jc-1; i++){
            rf[I2(2*i+1,2*j+1,Jf)] = (rc[I2(i,j,Jc)] + rc[I2(i+1,j,Jc)] + rc[I2(i,j+1,Jc)] + rc[I2(i+1,j+1,Jc)])/4;
        } 
    }
    }

    return rf;
}

template <typename real>
LPV<real> mgvrhs2D(const int Jc, const LPV<real> &rhs){

    const int N = rhs.N;
    const int J = (int) std::round(std::sqrt(N));

    if(Jc > J)
        throw std::invalid_argument("mgvrhs2D: Jc > J, cannot restrict from coarser to finer.");
    else if(Jc == J){
        // making a copy if on the same level. This is not necessary,
        // but avoids confusion with the pointers
        return LPV<real>(rhs);
    }

    const int nk = (int) std::round(std::log2(J-1));
    const int mk = (int) std::round(std::log2(Jc-1));

    LPV<real> out = restrict2D(rhs);
    for(int i=nk-2; i>=mk; i--){
        out = restrict2D(out);
    }

    return out;
}


template <typename real>
void mgv2D(const LPV<real> &rhs, const double lambda, const std::string& smoother, const int nsmooth, LPV<real> &x){
    check_all(rhs, x);

    const int N = x.N;
    const int J = (int) std::round(std::sqrt(N));

    if(J == 3){
        // using double precision here and rounding the double precision value
        x[I2(1,1,J)] = rhs.x[I2(1,1,J)]/(1+4*lambda);
        return;
    }

    const int Jc = (J+1)/2;
    const int Nc = Jc*Jc;
    const double w = (7*lambda + 1)/(8*lambda + 1);  //(2*dim*lambda + 1)/(4*dim*lambda + 1);
    const double w_ssor = (6*lambda + 1)/(4*lambda + 1);  //(2*dim*lambda + 1)/(4*dim*lambda + 1);

    real * res_arr __attribute__((aligned(ALIGN)));
    res_arr = (real*) allocate(N*sizeof(real));
    LPV<real> res = LPV<real>(res_arr, N, 1, x.opts, true, false);

    real * resc_arr __attribute__((aligned(ALIGN)));
    resc_arr = (real*) allocate(Nc*sizeof(real));
    LPV<real> resc = LPV<real>(resc_arr, Nc, 1, x.opts, true, false);

    real * zerosc_arr __attribute__((aligned(ALIGN)));
    zerosc_arr = (real*) allocate(Nc*sizeof(real));
    myfill(zerosc_arr, Nc, 0.0);
    LPV<real> zerosc = LPV<real>(zerosc_arr, Nc, 1, x.opts, true, false);

    if (smoother == "jacobi"){
        // jacobi smoothing
        jacobi2D(w, lambda, nsmooth, rhs, x);
    }else if (smoother == "ssor" || smoother == "sor"){
        // ssor smoothing
        ssor2D(w_ssor, lambda, nsmooth, rhs, x);
    }

    // compute residual b-Ax
    // use diff1D later on for this
    residual2D(rhs, lambda, x, res);

    // restrict residual to coarser grid
    resc = restrict2D(res);

    // apply mgv recusrively for coarser grid
    mgv2D(zerosc, lambda/2, smoother, nsmooth, resc);

    // interpolate coarse solution to fine grid and add to x
    res = interpolate2D(resc);
    #pragma omp parallel for default(none) firstprivate(J) shared(x, res)
    for(int j=1; j<J-1;j++){
        for(int i=1; i<J-1;i++){
            x[I2(i,j,J)] = x[I2(i,j,J)] + res[I2(i,j,J)];
        }
    }

    if (smoother == "jacobi"){
        // jacobi smoothing
        jacobi2D(w, lambda, nsmooth, rhs, x);
    }else if (smoother == "ssor" || smoother == "sor"){
        // ssor smoothing
        ssor2D(w_ssor, lambda, nsmooth, rhs, x);
    }
}

template <typename real>
void fmgv2D(const LPV<real> &b, const double lambda, const std::string& smoother, const int nsmooth,  LPV<real> &x){
    check_all(b, x);

    const int N = x.N;
    const int J = (int) std::round(std::sqrt(N));
    const int k = (int) std::round(std::log2(J-1));
    double lambdaf = lambda*std::exp2(-(k-1));
    int Jf,Jc;

    real * res_arr __attribute__((aligned(ALIGN)));
    res_arr = (real*) allocate(N*sizeof(real));
    LPV<real> res = LPV<real>(res_arr, N, 1, x.opts, true, false);

    // no need to allocate since mgvrhs2D does it for you
    LPV<real> resc, resf;

    // compute residual
    residual2D(b, lambda, x, res);

    // restrict residual to coarsest grid
    resc = mgvrhs2D(3, res);

    // exact solution on coarsest grid. BCs should be correctly enforced
    // using double precision here and rounding the double precision value
    resc[I2(1,1,3)] = resc.x[I2(1,1,3)]/(1+4*lambdaf);

    for(int i=2; i<k+1; i++){
        Jf = std::exp2(i) + 1;
        Jc = std::exp2(i-1)+1;
        lambdaf = lambda*std::exp2(-(k-i));
        
        // interpolate coarse residual to this level
        resc = interpolate2D(resc);

        // get residual on current grid by repeated restriction
        // of original residual to coarser grids
        resf = mgvrhs2D(Jf, res);

        // Multigrid V-cycle
        mgv2D(resf, lambdaf, smoother, nsmooth, resc); 
    }

    #pragma omp parallel for default(none) firstprivate(J) shared(x, resc)
    for(int j=1; j<J-1; j++){
        for(int i=1; i<J-1; i++){
            x[I2(i,j,J)] = x[I2(i,j,J)] + resc[I2(i,j,J)];
        }
    }
}

//////////////////////////////////// 3D ///////////////////////////////////////////////////////////////////////

template <typename real>
void residual3D(const LPV<real> &rhs, const double lambda, const LPV<real> &x, LPV<real> &res){
    check_all(rhs, x); check_all(rhs, res);
    const int N = res.N;
    const int J = (int) std::round(std::cbrt(N));

    myfill(res.x, N, 0.0);

    #pragma omp parallel for default(none) firstprivate(J,lambda) shared(res, rhs, x) collapse(2)
    for(int k=1; k<J-1; k++){
        for(int j=1; j<J-1; j++){
            for(int i=1; i<J-1; i++){
                res[I3(i,j,k,J)] = rhs[I3(i,j,k,J)] + (-x[I3(i,j,k,J)] + lambda*( ((x[I3(i+1,j,k,J)] - x[I3(i,j,k,J)]) - (x[I3(i,j,k,J)] - x[I3(i-1,j,k,J)]))
                                                                                + ((x[I3(i,j+1,k,J)] - x[I3(i,j,k,J)]) - (x[I3(i,j,k,J)] - x[I3(i,j-1,k,J)]))
                                                                                + ((x[I3(i,j,k+1,J)] - x[I3(i,j,k,J)]) - (x[I3(i,j,k,J)] - x[I3(i,j,k-1,J)])) )); // b[j] - (x[j] + lambda*diff2D(x))
            }
        }
    }
    

}

template <typename real>
LPV<real> restrict3D(const LPV<real> &r){
    const int N = r.N;
    const int J = (int) std::round(std::cbrt(N));
    const int Jc = (J+1)/2;
    const int Nc = Jc*Jc*Jc;

    real * rx __attribute__((aligned(ALIGN)));
    rx = r.x;

    real * rc __attribute__((aligned(ALIGN)));
    rc = (real*) allocate(Nc*sizeof(real));

    #ifdef __INTEL_COMPILER
        __assume_aligned(rc, ALIGN);
        __assume_aligned(rx, ALIGN);
    #endif

    myfill(rc, Nc, 0.0);

    #pragma omp parallel for default(none) firstprivate(J,Jc) shared(rc, rx) collapse(2)
    for(int k=1; k<Jc-1; k++){
        for(int j=1; j<Jc-1; j++){
            #pragma omp simd aligned(rc,rx: ALIGN)
            for(int i=1; i<Jc-1; i++){
                rc[I3(i,j,k,Jc)] = rx[I3(2*i,2*j,2*k,J)];
            }
        }
    }

    LPV<real> rc_lpv = LPV<real>(rc, Nc, 1, r.opts, true, false);
    return rc_lpv;

}

template <typename real>
LPV<real> interpolate3D(const LPV<real> &rc){
    const int Nc = rc.N;
    const int Jc = (int) std::round(std::cbrt(Nc));
    const int Jf = 2*Jc-1;
    const int Nf = Jf*Jf*Jf;

    real * rf_arr __attribute__((aligned(ALIGN)));
    rf_arr = (real*) allocate(Nf*sizeof(real));

    LPV<real> rf = LPV<real>(rf_arr, Nf, 1, rc.opts, true, false);

    real * rfx __attribute__((aligned(ALIGN)));
    real * rcx __attribute__((aligned(ALIGN)));
    rfx = rf.x; rcx = rc.x;

    #ifdef __INTEL_COMPILER
        __assume_aligned(rcx, ALIGN);
        __assume_aligned(rfx, ALIGN);
    #endif

    #pragma omp parallel default(none) firstprivate(Jc,Jf) shared(rfx, rcx, rc, rf)
    {
    #pragma omp for collapse(2)
    for(int k=0; k<Jc; k++){
        for(int j=0; j<Jc; j++){
            #pragma omp simd aligned(rcx,rfx: ALIGN)
            for(int i=0; i<Jc; i++){
                rfx[I3(2*i,2*j,2*k,Jf)] = rcx[I3(i,j,k,Jc)];
            }
        }
    }

    #pragma omp for collapse(2)
    for(int k=0; k<Jc; k++){
        for(int j=0; j<Jc; j++){
            for(int i=0; i<Jc-1; i++){
                rf[I3(2*i+1,2*j,2*k,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i+1,j,k,Jc)])/2;
            }
        }
    }

    #pragma omp for collapse(2)
    for(int k=0; k<Jc; k++){
        for(int j=0; j<Jc-1; j++){
            for(int i=0; i<Jc; i++){
                rf[I3(2*i,2*j+1,2*k,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i,j+1,k,Jc)])/2;
            }
        }
    }

    #pragma omp for collapse(2)
    for(int k=0; k<Jc-1; k++){
        for(int j=0; j<Jc; j++){
            for(int i=0; i<Jc; i++){
                rf[I3(2*i,2*j,2*k+1,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i,j,k+1,Jc)])/2;
            }
        }
    }

    #pragma omp for collapse(2)
    for(int k=0; k<Jc; k++){
        for(int j=0; j<Jc-1; j++){
            for(int i=0; i<Jc-1; i++){
                rf[I3(2*i+1,2*j+1,2*k,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i+1,j,k,Jc)] + rc[I3(i,j+1,k,Jc)] + rc[I3(i+1,j+1,k,Jc)])/4;
            }
        }
    }

    #pragma omp for collapse(2)
    for(int k=0; k<Jc-1; k++){
        for(int j=0; j<Jc; j++){
            for(int i=0; i<Jc-1; i++){
                rf[I3(2*i+1,2*j,2*k+1,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i+1,j,k,Jc)] + rc[I3(i,j,k+1,Jc)] + rc[I3(i+1,j,k+1,Jc)])/4;
            }
        }
    }

    #pragma omp for collapse(2)
    for(int k=0; k<Jc-1; k++){
        for(int j=0; j<Jc-1; j++){
            for(int i=0; i<Jc; i++){
                rf[I3(2*i,2*j+1,2*k+1,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i,j,k+1,Jc)] + rc[I3(i,j+1,k,Jc)] + rc[I3(i,j+1,k+1,Jc)])/4;
            }
        }
    }

    #pragma omp for collapse(2)
    for(int k=0; k<Jc-1; k++){
        for(int j=0; j<Jc-1; j++){
            for(int i=0; i<Jc-1; i++){
                rf[I3(2*i+1,2*j+1,2*k+1,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i+1,j,k,Jc)] + rc[I3(i,j+1,k,Jc)] + rc[I3(i,j,k+1,Jc)] + rc[I3(i+1,j+1,k,Jc)] + rc[I3(i+1,j,k+1,Jc)] + rc[I3(i,j+1,k+1,Jc)] + rc[I3(i+1,j+1,k+1,Jc)])/8;
            }
        }
    }
    }

    return rf;

}

template <typename real>
LPV<real> mgvrhs3D(const int Jc, const LPV<real> &rhs){

    const int N = rhs.N;
    const int J = (int) std::round(std::cbrt(N));

    if(Jc > J)
        throw std::invalid_argument("mgvrhs2D: Jc > J, cannot restrict from coarser to finer.");
    else if(Jc == J){
        // making a copy if on the same level. This is not necessary,
        // but avoids confusion with the pointers
        return LPV<real>(rhs);
    }

    const int nk = (int) std::round(std::log2(J-1));
    const int mk = (int) std::round(std::log2(Jc-1));

    LPV<real> out = restrict3D(rhs);
    for(int i=nk-2; i>=mk; i--){
        out = restrict3D(out);
    }

    return out;
}


template <typename real>
void mgv3D(const LPV<real> &rhs, const double lambda, const std::string& smoother, const int nsmooth, LPV<real> &x){
    check_all(rhs, x);

    const int N = x.N;
    const int J = (int) std::round(std::cbrt(N));

    if(J == 3){
        // using double precision here and rounding the double precision value
        x[I3(1,1,1,J)] = rhs.x[I3(1,1,1,J)]/(1+6*lambda);
        return;
    }

    const int Jc = (J+1)/2;
    const int Nc = Jc*Jc*Jc;
    const double w = (11*lambda + 1)/(12*lambda + 1);  //(2*dim*lambda + 1)/(4*dim*lambda + 1);
    const double w_ssor = (19*lambda + 1)/(12*lambda + 1);  //(2*dim*lambda + 1)/(4*dim*lambda + 1);

    real * res_arr __attribute__((aligned(ALIGN)));
    res_arr = (real*) allocate(N*sizeof(real));
    LPV<real> res = LPV<real>(res_arr, N, 1, x.opts, true, false);

    real * resc_arr __attribute__((aligned(ALIGN)));
    resc_arr = (real*) allocate(Nc*sizeof(real));
    LPV<real> resc = LPV<real>(resc_arr, Nc, 1, x.opts, true, false);

    real * zerosc_arr __attribute__((aligned(ALIGN)));
    zerosc_arr = (real*) allocate(Nc*sizeof(real));
    myfill(zerosc_arr, Nc, 0.0);
    LPV<real> zerosc = LPV<real>(zerosc_arr, Nc, 1, x.opts, true, false);

    if (smoother == "jacobi"){
        // jacobi smoothing
        jacobi3D(w, lambda, nsmooth, rhs, x);
    }else if (smoother == "ssor" || smoother == "sor"){
        // ssor smoothing
        ssor3D(w_ssor, lambda, nsmooth, rhs, x);
    }

    // compute residual b-Ax
    // use diff1D later on for this
    residual3D(rhs, lambda, x, res);

    // restrict residual to coarser grid
    resc = restrict3D(res);

    // apply mgv recusrively for coarser grid
    mgv3D(zerosc, lambda/2, smoother, nsmooth, resc);

    // interpolate coarse solution to fine grid and add to x
    res = interpolate3D(resc);
    #pragma omp parallel for default(none) firstprivate(J) shared(x, res) collapse(2)
    for(int k=1; k<J-1;k++){
        for(int j=1; j<J-1;j++){
            for(int i=1; i<J-1;i++){
                x[I3(i,j,k,J)] = x[I3(i,j,k,J)] + res[I3(i,j,k,J)];
            }
        }
    }

    if (smoother == "jacobi"){
        // jacobi smoothing
        jacobi3D(w, lambda, nsmooth, rhs, x);
    }else if (smoother == "ssor" || smoother == "sor"){
        // ssor smoothing
        ssor3D(w_ssor, lambda, nsmooth, rhs, x);
    }

}

template <typename real>
void fmgv3D(const LPV<real> &b, const double lambda, const std::string& smoother, const int nsmooth, LPV<real> &x){
    check_all(b, x);

    const int N = x.N;
    const int J = (int) std::round(std::cbrt(N));
    const int k = (int) std::round(std::log2(J-1));
    double lambdaf = lambda*std::exp2(-(k-1));
    int Jf,Jc;

    real * res_arr __attribute__((aligned(ALIGN)));
    res_arr = (real*) allocate(N*sizeof(real));
    LPV<real> res = LPV<real>(res_arr, N, 1, x.opts, true, false);

    // no need to allocate since mgvrhs2D does it for you
    LPV<real> resc, resf;

    // compute residual
    residual3D(b, lambda, x, res);

    // restrict residual to coarsest grid
    resc = mgvrhs3D(3, res);

    // exact solution on coarsest grid. BCs should be correctly enforced
    // using double precision here and rounding the double precision value
    resc[I3(1,1,1,3)] = resc.x[I3(1,1,1,3)]/(1+6*lambdaf);

    for(int i=2; i<k+1; i++){
        Jf = std::exp2(i) + 1;
        Jc = std::exp2(i-1)+1;
        lambdaf = lambda*std::exp2(-(k-i));
        
        // interpolate coarse residual to this level
        resc = interpolate3D(resc);

        // get residual on current grid by repeated restriction
        // of original residual to coarser grids
        resf = mgvrhs3D(Jf, res);

        // Multigrid V-cycle
        mgv3D(resf, lambdaf, smoother, nsmooth, resc); 
    }

    #pragma omp parallel for default(none) firstprivate(J) shared(x, resc) collapse(2)
    for(int k=1; k<J-1; k++){
        for(int j=1; j<J-1; j++){
            for(int i=1; i<J-1; i++){
                x[I3(i,j,k,J)] = x[I3(i,j,k,J)] + resc[I3(i,j,k,J)];
            }
        }
    }
}

//////////////////////////////////// Putting things together //////////////////////////////////////////////////

template <typename real>
LPV<real> jacobi(const int dim, const double w, const double lambda, const int n_it, const LPV<real> &rhs, const LPV<real> &x){
    const int J = (int) std::round(std::pow(x.N, 1.0/dim));

    if(((J-2) & (J-1)) != 0)
        throw std::invalid_argument("jacobi: J-1 is not a power of 2");

    LPV<real> out = LPV<real>(x);
    if(dim == 1){
        jacobi1D(w, lambda, n_it, rhs, out);
    }else if(dim == 2){
        jacobi2D(w, lambda, n_it, rhs, out);
    }else{
        jacobi3D(w, lambda, n_it, rhs, out);
    }
    return out;
}

template <typename real>
LPV<real> ssor(const int dim, const double w, const double lambda, const int n_it, const LPV<real> &rhs, const LPV<real> &x){
    const int J = (int) std::round(std::pow(x.N, 1.0/dim));

    if(((J-2) & (J-1)) != 0)
        throw std::invalid_argument("ssor: J-1 is not a power of 2");

    LPV<real> out = LPV<real>(x);
    if(dim == 1){
        ssor1D(w, lambda, n_it, rhs, out);
    }else if(dim == 2){
        ssor2D(w, lambda, n_it, rhs, out);
    }else{
        ssor3D(w, lambda, n_it, rhs, out);
    }
    return out;
}


template <typename real>
LPV<real> mgv(const int dim, const LPV<real> &rhs, const double lambda, const std::string& smoother, const int nsmooth, const LPV<real> &x){
    const int J = (int) std::round(std::pow(x.N, 1.0/dim));

    if(((J-2) & (J-1)) != 0)
        throw std::invalid_argument("mgv: J-1 is not a power of 2");

    LPV<real> out = LPV<real>(x);
    if(dim == 1){
        mgv1D(rhs, lambda, smoother, nsmooth, out);
    }else if(dim == 2){
        mgv2D(rhs, lambda, smoother, nsmooth, out);
    }else{
        mgv3D(rhs, lambda, smoother, nsmooth, out);
    }
    return out;
}

template <typename real>
LPV<real> fmgv(const int dim, const LPV<real> &rhs, const double lambda, const std::string& smoother, const int nsmooth, const LPV<real> &x){
    const int J = (int) std::round(std::pow(x.N, 1.0/dim));

    if(((J-2) & (J-1)) != 0)
        throw std::invalid_argument("fmgv: J-1 is not a power of 2");

    LPV<real> out = LPV<real>(x);
    if(dim == 1){
        fmgv1D(rhs, lambda, smoother, nsmooth, out);
    }else if(dim == 2){
        fmgv2D(rhs, lambda, smoother, nsmooth, out);
    }else{
        fmgv3D(rhs, lambda, smoother, nsmooth, out);
    }
    return out;
}

template <typename real>
LPV<real> residual(const int dim, const LPV<real> &rhs, const double lambda, const LPV<real> &x){
    const int J = (int) std::round(std::pow(x.N, 1.0/dim));

    if(((J-2) & (J-1)) != 0)
        throw std::invalid_argument("residual: J-1 is not a power of 2");

    LPV<real> out = 0.0*x;
    if(dim == 1){
        residual1D(rhs, lambda, x, out);
    }else if(dim == 2){
        residual2D(rhs, lambda, x, out);
    }else{
        residual3D(rhs, lambda, x, out);
    }
    return out;
}
