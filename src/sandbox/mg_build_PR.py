import numpy as np

import scipy.sparse as sp
from scipy.sparse.linalg import spsolve

def assemble_matrix(K, dim):
    e = np.ones((K,))
    L = sp.spdiags([-e,2*e,-e],[-1,0,1], K, K, format="csr")
    I = sp.eye(L.shape[0])
    if   dim == 1: A = L
    elif dim == 2: A = sp.kron(I,L) + sp.kron(L,I)
    elif dim == 3: A = sp.kron(sp.kron(I,I),L) + sp.kron(L,sp.kron(I,I)) + sp.kron(sp.kron(I,L),I)

    return -A

def inject1D(J,r):
    d = 1
    N = J**d
    Jc = int(np.round((J+1)/2))
    Nc = Jc**d

    rc = np.zeros((Nc,))
    for j in range(1,Jc-1):
        rc[j] = r[2*j]

    return rc

def interpolate1D(Jc,rc):
    d = 1
    Nc = Jc**d
    Jf = 2*Jc-1
    Nf = Jf**d

    rf = np.zeros((Nf,))
    for j in range(0,Jc-1):
        rf[2*j] = rc[j]
        rf[2*j+1] = (rc[j] + rc[j+1])/2

    rf[2*(Jc-1)] = rc[Jc-1]

    return rf

def I2(i,j,K): return j*K+i

def inject2D(J,r):
    d = 2
    N = J**d
    Jc = int(np.round((J+1)/2))
    Nc = Jc**d

    rc = np.zeros((Nc,))
    for i in range(1,Jc-1):
        for j in range(1,Jc-1):
            rc[I2(i,j,Jc)] = r[I2(2*i,2*j,J)]

    return rc


def restrict2D(J,r):
    d = 2
    N = J**d
    Jc = int(np.round((J+1)/2))
    Nc = Jc**d

    rc = np.zeros((Nc,))
    for i in range(1,Jc-1):
        for j in range(1,Jc-1):
            rc[I2(i,j,Jc)] =1/16*(4*r[I2(2*i,2*j,J)] + 2*(r[I2(2*i+1,2*j,J)] + r[I2(2*i,2*j+1,J)] + r[I2(2*i-1,2*j,J)] + r[I2(2*i,2*j-1,J)]) + r[I2(2*i+1,2*j+1,J)] + r[I2(2*i-1,2*j+1,J)] + r[I2(2*i+1,2*j-1,J)] + r[I2(2*i-1,2*j-1,J)])

    return rc

def interpolate2D(Jc,rc):
    d = 2
    Nc = Jc**d
    Jf = 2*Jc-1
    Nf = Jf**d

    rf = np.zeros((Nf,))

    # the 0,Jc loops could be 1,Jc-1 loops, but let us make sure we do not screw up on the C++ side
    for i in range(0,Jc):
        for j in range(1, Jc-1):
            rf[I2(2*i,2*j,Jf)] = rc[I2(i,j,Jc)];

    for i in range(0,Jc-1):
        for j in range(0, Jc):
            rf[I2(2*i+1,2*j,Jf)] = (rc[I2(i,j,Jc)] + rc[I2(i+1,j,Jc)])/2;

    for i in range(0,Jc):
        for j in range(0, Jc-1):
            rf[I2(2*i,2*j+1,Jf)] = (rc[I2(i,j,Jc)] + rc[I2(i,j+1,Jc)])/2;

    for i in range(0,Jc-1):
        for j in range(0, Jc-1):
            rf[I2(2*i+1,2*j+1,Jf)] = (rc[I2(i,j,Jc)] + rc[I2(i+1,j,Jc)] + rc[I2(i,j+1,Jc)] + rc[I2(i+1,j+1,Jc)])/4;

    return rf.flatten()

def e(N,j):
    e = np.zeros((N,))
    e[j] = 1
    return e

def I3(i,j,k,K): return (k*K+j)*K+i

def inject3D(J,r):
    d = 3
    N = J**d
    Jc = int(np.round((J+1)/2))
    Nc = Jc**d

    rc = np.zeros((Nc,))
    for i in range(1,Jc-1):
        for j in range(1,Jc-1):
            for k in range(1,Jc-1):
                rc[I3(i,j,k,Jc)] = r[I3(2*i,2*j,2*k,J)]

    return rc


def interpolate3D(Jc,rc):
    d = 3
    Nc = Jc**d
    Jf = 2*Jc-1
    Nf = Jf**d

    rf = np.zeros((Nf,))

    # the 0,Jc loops could be 1,Jc-1 loops, but let us make sure we do not screw up on the C++ side
    for i in range(0,Jc):
        for j in range(0, Jc):
            for k in range(0, Jc):
                rf[I3(2*i,2*j,2*k,Jf)] = rc[I3(i,j,k,Jc)];

    for i in range(0,Jc-1):
        for j in range(0, Jc):
            for k in range(0, Jc):
                rf[I3(2*i+1,2*j,2*k,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i+1,j,k,Jc)])/2;

    for i in range(0,Jc):
        for j in range(0, Jc-1):
            for k in range(0, Jc):
                rf[I3(2*i,2*j+1,2*k,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i,j+1,k,Jc)])/2;

    for i in range(0,Jc):
        for j in range(0, Jc):
            for k in range(0, Jc-1):
                rf[I3(2*i,2*j,2*k+1,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i,j,k+1,Jc)])/2;

    for i in range(0,Jc-1):
        for j in range(0, Jc-1):
            for k in range(0, Jc):
                rf[I3(2*i+1,2*j+1,2*k,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i+1,j,k,Jc)] + rc[I3(i,j+1,k,Jc)] + rc[I3(i+1,j+1,k,Jc)])/4;

    for i in range(0,Jc-1):
        for j in range(0, Jc):
            for k in range(0, Jc-1):
                rf[I3(2*i+1,2*j,2*k+1,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i+1,j,k,Jc)] + rc[I3(i,j,k+1,Jc)] + rc[I3(i+1,j,k+1,Jc)])/4;

    for i in range(0,Jc):
        for j in range(0, Jc-1):
            for k in range(0, Jc-1):
                rf[I3(2*i,2*j+1,2*k+1,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i,j,k+1,Jc)] + rc[I3(i,j+1,k,Jc)] + rc[I3(i,j+1,k+1,Jc)])/4;

    for i in range(0,Jc-1):
        for j in range(0, Jc-1):
            for k in range(0, Jc-1):
                rf[I3(2*i+1,2*j+1,2*k+1,Jf)] = (rc[I3(i,j,k,Jc)] + rc[I3(i+1,j,k,Jc)] + rc[I3(i,j+1,k,Jc)] + rc[I3(i,j,k+1,Jc)] + rc[I3(i+1,j+1,k,Jc)] + rc[I3(i+1,j,k+1,Jc)] + rc[I3(i,j+1,k+1,Jc)] + rc[I3(i+1,j+1,k+1,Jc)])/8;

    return rf.flatten()

def nonborder_indices2D(J):
    out = []
    for i in range(J):
        for j in range(J):
            if i not in [0,J-1] and j not in [0,J-1]:
                out.append(I2(i,j,J))

    return np.sort(np.array(out))

def nonborder_indices3D(J):
    out = []
    for i in range(J):
        for j in range(J):
            for k in range(J):
                if i not in [0,J-1] and j not in [0,J-1] and k not in [0,J-1]:
                    out.append(I3(i,j,k,J))

    return np.sort(np.array(out))

d = 3
J = 7 # 2**3-1
Jc = 3 # 2**2-1
l = 1
N = J**d

A = assemble_matrix(J,d).toarray()
Ac = assemble_matrix(Jc,d).toarray()

B = np.eye(N) - l*A

if d == 1:
    II = np.vstack([inject1D(J+2,e(J+2,i)) for i in range(J+2)]).T[1:-1,1:-1]
    I = np.vstack([interpolate1D(J-2,e(J-2,i)) for i in range(J-2)])[1:-1,1:-1]
    print(abs(I@A@II.T-Ac/2).max(), abs(I@II.T-np.eye(Jc**d)).max())
elif d == 2:
    # this works
    III = np.vstack([restrict2D(J+2,e((J+2)**2,i)) for i in range((J+2)**2)]).T
    II = np.vstack([inject2D(J+2,e((J+2)**2,i)) for i in range((J+2)**2)]).T
    # this now is fixed and also works and I = III*4
    I = np.vstack([interpolate2D(J-2,e((J-2)**2,i)) for i in range((J-2)**2)])
    idxp = nonborder_indices2D(J+2)
    idxm = nonborder_indices2D(J-2)
    mask = np.ix_(idxm,idxp)
    I = I[mask]
    II = II[mask]
    III = III[mask]

    print(abs(I@A@II.T-Ac/2).max(), abs(I@II.T-np.eye(Jc**d)).max())
elif d == 3:
    II = np.vstack([inject3D(J+2,e((J+2)**3,i)) for i in range((J+2)**3)]).T
    I = np.vstack([interpolate3D(J-2,e((J-2)**3,i)) for i in range((J-2)**3)])
    idxp = nonborder_indices3D(J+2)
    idxm = nonborder_indices3D(J-2)
    mask = np.ix_(idxm,idxp)
    I = I[mask]
    II = II[mask]
    print(abs(I@A@II.T-Ac/2).max(), abs(I@II.T-np.eye(Jc**d)).max())
